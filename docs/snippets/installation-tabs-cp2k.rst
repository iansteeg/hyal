.. tabs::

   .. group-tab:: Ubuntu (apt)

      On Linux-based systems, this is easiest done with Anaconda_.

      .. code-block:: bash

         conda install cp2k -c conda-forge




   .. group-tab:: Mac OSX (brew)

      On OSX systems, this is easiest done with Homebrew_.

      .. code-block:: bash

         brew install cp2k


   .. group-tab:: Windows

      .. image:: https://static.wikia.nocookie.net/malcolminthemiddle/images/1/1b/DeweyWilkerson.jpg/revision/latest?cb=20190728093932
         :width: 400
         :alt: I have no idea


.. _Anaconda:
   https://docs.anaconda.com/free/anaconda/install/index.html

.. _Homebrew:
   https://brew.sh/
