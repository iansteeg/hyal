(installation-label)=
# <i class="fa fa-power-off fa-fw"></i> Installation

The most straightforward way to install the package is via [pip](https://pip.pypa.io/en/stable/):

```pip
python3 -m pip install git+https://gitlab.com/hylleraasplatform/hyal.git
```

Development installation can be done by cloning the repository and installing it with pip:

```bash
git clone https://gitlab.com/hylleraasplatform/hyal.git
cd hyal
python3 -m pip install -e . config-settings editable_mode=strict
```

## Dependencies

HyAL requires the Python package [Hylleraas Software Platform](https://hylleraas.readthedocs.io/en/latest/) (HSP) to be installed. If no HSP installation is found, HyAL will attempt to install it automatically. If this fails, you may install it manually by:

```bash
python3 -m pip install git+https://gitlab.com/hylleraas/hylleraas.git
```

### Optional dependencies

#### DeepMD-kit

HyAL can use the [DeepMD-kit](https://docs.deepmodeling.com/projects/deepmd/en/master/index.html) package to perform machine learning based force field optimization. If you wish to use this feature, you must install DeepMD-kit manually. This is easiest done via [Anaconda](https://docs.anaconda.com/free/anaconda/install/index.html) (note carefully that this does **not** have to be the same environment as HyAL is installed in):

```bash
conda create -p  ./deepmd python=3.9
conda activate ./deepmd/
conda install deepmd-kit -c conda-forge
```

#### CP2K

HyAL can use the [CP2K](https://www.cp2k.org/) package to perform quantum chemical calculations. If you wish to use this feature, you must install CP2K manually.

```{eval-rst}
.. include:: snippets/installation-tabs-cp2k.rst
```

CP2K most likely needs an extra folder containing data about the basis sets and pseudopotentials. This folder can be downloaded from the [CP2K website](https://www.cp2k.org/extras:cp2k-data:download). The folder should be placed in the same folder as the CP2K executable, and the path to the folder should be added to the `CP2K_DATA_DIR` environment variable. This can be done by adding the following line to your `.bashrc` file:

```bash
git clone https://github.com/cp2k/cp2k.git
mv cp2k/data cp2k_data_dir
rm -rf cp2k
export CP2K_DATA_DIR=$(realpath cp2k_data_dir)
```

#### LAMMPS

HyAL can use Lammps to perform molecular dynamics simulations. If you wish to use this feature, you must install Lammps (with DeepMD-kit support) manually.

```bash
wget https://github.com/lammps/lammps/archive/stable_23Jun2022_update2.tar.gz
rm -rf lammps-stable_23Jun2022_update2
tar xf stable_23Jun2022_update2.tar.gz
cd lammps-stable_23Jun2022_update2/
chmod -R ugo+rwx .
export LAMMPS_ROOT=$PWD
cd ../

#DeePMD-kit
cd deepmd-kit
export deepmd_root=$PWD/install
export deepmd_source=$PWD/source

cd $deepmd_source
# Version extraction was problematic, so I set manually
find . -type f | xargs -l sed -i  's#$(./lmp_version.sh)#20220623#g'

rm -rf build $deepmd_root
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$deepmd_root -DUSE_ROCM_TOOLKIT=TRUE -DUSE_TF_PYTHON_LIBS=TRUE -DLAMMPS_SOURCE_ROOT=$LAMMPS_ROOT -DLAMMPS_VERSION_NUMBER=20220623 -DCMAKE_BUILD_TYPE=Release -DHIP_HIPCC_FLAGS="--amdgpu-target=gfx90a" -DDP_VARIANT=rocm ..

make -j 32
make install

cd $deepmd_root
PATH=$deepmd_root/bin:$PATH
PATH=$deepmd_root/lib:$PATH
export PATH

cd $LAMMPS_ROOT
rm -rf build
mkdir build
cd build
CXX=CC cmake -D PKG_PLUGIN=ON -D PKG_KSPACE=ON -D LAMMPS_INSTALL_RPATH=ON -DBUILD_SHARED_LIBS=yes -D CMAKE_INSTALL_PREFIX=${deepmd_root} -DCMAKE_INSTALL_LIBDIR=lib -DCMAKE_INSTALL_FULL_LIBDIR=${deepmd_root}/lib   -DDOWNLOAD_PLUMED=NO  -D BUILD_MPI=no   ../cmake
make -j16
make install
cp lmp lmp_serial

CXX=CC cmake -D PKG_PLUGIN=ON -D PKG_KSPACE=ON -D LAMMPS_INSTALL_RPATH=ON -DBUILD_SHARED_LIBS=yes -D CMAKE_INSTALL_PREFIX=${deepmd_root} -DCMAKE_INSTALL_LIBDIR=lib -DCMAKE_INSTALL_FULL_LIBDIR=${deepmd_root}/lib   -DDOWNLOAD_PLUMED=NO  -D BUILD_MPI=yes   ../cmake
make -j16
make install
cp lmp lmp_mpi
```
