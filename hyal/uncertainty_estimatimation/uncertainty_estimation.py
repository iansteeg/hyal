import itertools
import json
import random
from copy import deepcopy
from itertools import combinations
from pathlib import Path
from typing import Dict, List, Union

import numpy as np
import pandas as pd
from hyobj import DataSet
from scipy.optimize import minimize


class UncertaintyEstimation:
    """Uncertainty Estimation class.

    This class is used to estimate the uncertainty model predictions
    using datasets, and is purely a dataset processing class.
    """

    def __init__(self):
        pass

    def get_mean_dataset(
        self,
        dataset: Union[DataSet, pd.DataFrame],
        groupby_list: list[str] = ['geometry_id'],
        method_id: str = 'mean',
    ):
        """Get the mean dataset.

        Arguments
        ---------
        dataset : Union[DataSet, pd.DataFrame]
            The dataset to be aggregated to the mean.
        groupby_list : list[str]
            The list of columns to do the mean over.
        method_id : str
            The method_id to be used for the mean dataset.

        Returns
        -------
        Union[DataSet, pd.DataFrame]
            The mean dataset.

        """
        if isinstance(dataset, DataSet):
            pd_out = dataset.data.groupby(groupby_list).agg(self.mean_agg)
        else:
            pd_out = dataset.groupby(groupby_list).agg(self.mean_agg)

        # Make sure it has the groupby_list column
        pd_out = pd_out.reset_index()

        pd_out['method_id'] = [method_id] * len(pd_out)
        if isinstance(dataset, DataSet):
            return DataSet(pd_out, units=dataset.units)
        else:
            return pd_out

    def mean_min_max_agg(self, x):
        """Calculate the mean, min, and max of a dataset."""
        # Convert input to np.ndarray if it is not already one.
        if not isinstance(x, np.ndarray):
            x = np.array(x)

        # Handle different data types:
        if np.issubdtype(x.dtype, np.number):
            # If x is a numeric numpy array,
            # it is straightforward to calculate the mean.
            return [np.mean(x), np.min(x), np.max(x)]
        try:
            stacked_arrays = np.stack(x)
            mean_array = np.mean(stacked_arrays)
            min_array = np.min(stacked_arrays)
            max_array = np.max(stacked_arrays)
            return [mean_array, min_array, max_array]
        except Exception:
            # If elements within x are lists or arrays,
            # treat each one as a unique string.
            x_str = pd.Series([str(e) for e in x])
            unique_elements = x_str.drop_duplicates()

            if len(unique_elements) == 1:
                # Return the single unique element
                # as it originally was in x.
                return x[0]
            else:
                # Return NaN if there is not exactly one unique element.
                return np.nan

    def mean_agg(self, x):
        """Calculate the mean of a dataset.

        Arguments
        ---------
        x : array_like
            The dataset to be aggregated.
            Can be a list, numpy array, or pandas series.

        Returns
        -------
        np.ndarray, element, or float
            The mean of the dataset, the single unique element, or NaN.

        """
        # Convert input to np.ndarray if it is not already one.
        if not isinstance(x, np.ndarray):
            x = np.array(x)

        # Handle different data types:
        if np.issubdtype(x.dtype, np.number):
            # If x is a numeric numpy array,
            # it is straightforward to calculate the mean.
            return np.mean(x, axis=0)
        try:
            stacked_arrays = np.stack(x)
            mean_array = np.mean(stacked_arrays, axis=0)
            return mean_array
        except Exception:
            # If elements within x are lists or arrays,
            # treat each one as a unique string.
            x_str = pd.Series([str(e) for e in x])
            unique_elements = x_str.drop_duplicates()

            if len(unique_elements) == 1:
                # Return the single unique element
                # as it originally was in x.
                return x[0]
            else:
                # Return NaN if there is not exactly one unique element.
                return np.nan

    def error_agg(self, x):
        """Calculate the error of a dataset."""
        # Convert input to np.ndarray if it is not already one.
        if not isinstance(x, np.ndarray):
            x = np.array(x)

        # Handle different data types:
        if np.issubdtype(x.dtype, np.number):
            # If x is a numeric numpy array,
            # it is straightforward to calculate the mean.
            return np.mean(x**2, axis=0) ** 0.5
        try:
            stacked_arrays = np.stack(x)
            mean_array = (
                np.mean(stacked_arrays**2, axis=0) ** 0.5
            )  # Calculate mean across the first axis
            return mean_array
        except Exception:
            # If elements within x are lists or arrays,
            # treat each one as a unique string.
            x_str = pd.Series([str(e) for e in x])
            unique_elements = x_str.drop_duplicates()

            if len(unique_elements) == 1:
                # Return the single unique element
                # as it originally was in x.
                return x[0]
            else:
                # Return NaN if there is not exactly one unique element.
                return np.nan

    def std_agg(self, x):
        """Calculate the standard deviation of a dataset."""
        # Convert input to np.ndarray if it is not already one.
        if not isinstance(x, np.ndarray):
            x = np.array(x)

        # Handle different data types:
        if np.issubdtype(x.dtype, np.number):
            # If x is a numeric numpy array,
            # it is straightforward to calculate the mean.
            return (np.sum(x**2, axis=0) / (len(x) - 1)) ** 0.5
        try:
            stacked_arrays = np.stack(x)
            mean_array = (
                np.sum(stacked_arrays**2, axis=0) / (len(x) - 1)
            ) ** 0.5  # Calculate mean across the first axis
            return mean_array
        except Exception:
            # If elements within x are lists or arrays,
            # treat each one as a unique string.
            x_str = pd.Series([str(e) for e in x])
            unique_elements = x_str.drop_duplicates()

            if len(unique_elements) == 1:
                # Return the single unique element
                # as it originally was in x.
                return x[0]
            else:
                # Return NaN if there is not exactly one unique element.
                return np.nan

    def std_agg_from_values(self, agg):
        """Calculate the standard deviation of a dataset."""
        # Handle different data types:
        try:
            if np.issubdtype(agg.dtype, np.number):
                return np.std(agg, axis=0, ddof=1)
            elif np.issubdtype(agg.dtype, np.object_):
                try:
                    return np.std(np.stack(agg), axis=0, ddof=1)
                except ValueError:
                    return np.nan
            else:
                return np.nan
        except TypeError:
            return np.nan

    def make_deviation_report(
        self,
        dataset: DataSet,
        chosen_columns: List[str] = ['energy', 'forces', 'virial'],
        reference_method_id: str = 'reference',
        sub_dir: str = 'deviation_reports',
        linear_correction: Dict = {},
        save_all: bool = False,
        overwrite: bool = False,
        n_models_calibration: List[int] = [],
        error_metrics: Dict = {},
        robust: bool = True,
        use_hash: bool = True,
    ):
        """Make a deviation report.

        Arguments
        ---------
        dataset : DataSet
            The dataset containing model predictions
            and reference data, identified by the method_id column.

        chosen_columns : List[str]
            The columns to be used for the deviation report.

        reference_method_id : str
            The method_id of the reference dataset.

        sub_dir : str
            The sub directory to save the deviation reports.

        save_all: bool
            If True, all reports will be saved. If False, only the
            deviation report will be saved.

        Returns
        -------
        Dict
            The output files.

        """
        # Get the hash of the dataset, signifying the unique calculation
        if use_hash:
            calculation_hash = dataset.hash + '_'
        else:
            calculation_hash = ''

        # Determine which categories the different models are in
        model_categories = dataset.data['method_id'].unique()
        model_categories = model_categories[
            model_categories != reference_method_id
        ]

        # Set up error metrics
        if not error_metrics:
            error_metrics = {
                'forces': self.atomcomponent_metric,
                'virial': self.trace_metric,
            }

        assert len(model_categories) > 0, 'No models in dataset.'

        # Make sure the sub_dir exists
        Path(sub_dir).mkdir(parents=True, exist_ok=True)

        # Make sure the reference method is in the dataset
        has_reference = reference_method_id in dataset.data['method_id'].values
        # if not, we just estimate deviations between models and mean

        # Make output files dictionary, which will be returned
        output_files = dict(
            dev_models_vs_mean=f'{sub_dir}/{calculation_hash}'
            + 'dev_models_mean.pkl'
        )

        if has_reference:
            output_files.update(
                dict(
                    dev_models_vs_ref=f'{sub_dir}/{calculation_hash}'
                    + 'dev_models_vs_ref.pkl',
                    dev_mean_vs_ref=f'{sub_dir}/{calculation_hash}'
                    + 'dev_mean_vs_ref.pkl',
                )
            )
        # Make sure that full_dataset is a output file
        if save_all:
            output_files.update(
                dict(
                    full_dataset=f'{sub_dir}/{calculation_hash}'
                    + 'full_dataset.pkl'
                )
            )

        # Check that if  all the large files exist, if not, compute
        if not self.all_files_exists(output_files):
            # Get mean model predictions
            mean_df = self.get_mean_dataset(
                dataset.data[dataset.data['method_id'] != reference_method_id],
                groupby_list=['geometry_id'],
                method_id='mean',
            )

            # Add mean to the dataset, which will
            # be used for deviation calculations
            full_dataset = DataSet(
                pd.concat([mean_df, dataset.data]), units=dataset.units
            )
            full_dataset.data = full_dataset.data.reset_index(drop=True)

            if save_all:
                full_dataset.write_data(output_files['full_dataset'])

        # Get deviations between each model and reference
        if not has_reference:
            pass
        elif Path(output_files['dev_models_vs_ref']).exists():
            dev_models_vs_ref = DataSet(output_files['dev_models_vs_ref'])
        else:
            dev_models_vs_ref = self.get_deviations_dataset(
                full_dataset,
                between_col_category='method_id',
                chosen_columns=chosen_columns,
                categories1=[reference_method_id],
                categories2=model_categories,
            )
            dev_models_vs_ref.write_data(output_files['dev_models_vs_ref'])

        # Get deviations between each model and mean
        if Path(output_files['dev_models_vs_mean']).exists():
            dev_models_vs_mean = DataSet(output_files['dev_models_vs_mean'])
        else:
            dev_models_vs_mean = self.get_deviations_dataset(
                full_dataset,
                between_col_category='method_id',
                chosen_columns=chosen_columns,
                categories1=['mean'],
                categories2=model_categories,
            )
            dev_models_vs_mean.write_data(output_files['dev_models_vs_mean'])

        # Get deviations between mean and reference
        if not has_reference:
            pass
        elif Path(output_files['dev_mean_vs_ref']).exists():
            dev_mean_vs_ref = DataSet(output_files['dev_mean_vs_ref'])
        else:
            dev_mean_vs_ref = self.get_deviations_dataset(
                full_dataset,
                between_col_category='method_id',
                chosen_columns=chosen_columns,
                categories1=['mean'],
                categories2=[reference_method_id],
            )
            dev_mean_vs_ref.write_data(output_files['dev_mean_vs_ref'])

        # Compute the absoloute error for the models and the mean
        extra_outputs = dict(
            error_models_vs_ref=f'{sub_dir}/{calculation_hash}'
            + 'error_models_vs_ref.pkl',
            error_mean_vs_ref=f'{sub_dir}/{calculation_hash}'
            + 'error_mean_vs_ref.pkl',
        )
        if has_reference and not self.all_files_exists(extra_outputs):
            error_models_vs_ref = dev_models_vs_ref.data.groupby(
                'geometry_id'
            ).agg(self.error_agg)

            error_mean_vs_ref = dev_mean_vs_ref.data.groupby(
                'geometry_id'
            ).agg(self.error_agg)
            error_models_vs_ref = error_models_vs_ref.reset_index()
            error_mean_vs_ref = error_mean_vs_ref.reset_index()
            error_mean_vs_ref = DataSet(error_mean_vs_ref)
            error_models_vs_ref = DataSet(error_models_vs_ref)
            error_models_vs_ref.write_data(
                extra_outputs['error_models_vs_ref']
            )
            error_mean_vs_ref.write_data(extra_outputs['error_mean_vs_ref'])
        elif has_reference:
            error_models_vs_ref = DataSet(extra_outputs['error_models_vs_ref'])
            error_mean_vs_ref = DataSet(extra_outputs['error_mean_vs_ref'])
        if has_reference:
            output_files.update(extra_outputs)

        #
        outputs = {
            'uncalibrated_error_estimate'
            + '_models_vs_mean': f'{sub_dir}/{calculation_hash}'
            + 'uncalibrated_error'
            + '_estimate_models_vs_mean.pkl'
        }
        if not Path(list(outputs.values())[0]).exists():
            std_models_vs_mean = dev_models_vs_mean.data.groupby(
                'geometry_id'
            ).agg(self.std_agg)

            std_models_vs_mean = std_models_vs_mean.reset_index()
            std_models_vs_mean = DataSet(std_models_vs_mean)
            std_models_vs_mean.write_data(
                f'{sub_dir}/{calculation_hash}uncalibrated_error'
                + '_estimate_models_vs_mean.pkl'
            )
        else:
            std_models_vs_mean = DataSet(list(outputs.values())[0])

        output_files.update(outputs)

        # Compute uncertainty corrections if not provided or not saved
        if not linear_correction and has_reference:
            correction_file = (
                f'{sub_dir}/{calculation_hash}correction_factors.json'
            )
            if Path(correction_file).exists() and not overwrite:
                linear_correction = json.load(open(correction_file))
            else:
                # we compute the correction factors
                linear_correction['mean_vs_ref'] = (
                    self.get_correction_from_residuals(
                        dev_mean_vs_ref,
                        std_models_vs_mean,
                        chosen_columns=chosen_columns,
                    )
                )
                linear_correction['models_vs_ref'] = (
                    self.get_correction_from_residuals(
                        dev_models_vs_ref,
                        std_models_vs_mean,
                        chosen_columns=chosen_columns,
                    )
                )
                with open(correction_file, 'w') as f:
                    json.dump(linear_correction, f, indent=4)

            output_files.update({'correction_factors': correction_file})
        if len(n_models_calibration) > 0:
            output = (
                f'{sub_dir}/{calculation_hash}ue_corrections_per_model.json'
            )

            if not Path(output).exists():
                ue_corrections_per_model = {}
                for n_models in n_models_calibration:
                    ue_corrections_per_model[str(n_models)] = (
                        self.ue_per_num_models(
                            dataset,
                            dev_models_vs_ref,
                            n_models,
                            chosen_columns,
                            robust=robust,
                        )
                    )

                if ue_corrections_per_model:
                    # Save the calibrated uncertainty estimates
                    ue_corrections_per_model_file = (
                        f'{sub_dir}/{calculation_hash}'
                        + 'ue_corrections_per_model.json'
                    )
                    with open(ue_corrections_per_model_file, 'w') as f:
                        json.dump(ue_corrections_per_model, f, indent=4)
                    output_files.update(
                        {
                            'ue_corrections_per_model':
                            ue_corrections_per_model_file  # fmt: skip
                        }
                    )
            else:
                ue_corrections_per_model = json.load(open(output))
            output_files.update({'ue_corrections_per_model': output})

        outputs = {
            'calibrated_error_estimate_mean_vs_ref': f'{sub_dir}'
            + f'/{calculation_hash}calibrated_error'
            + '_estimate_mean_vs_ref.pkl',
            'calibrated_error_estimate_models_vs_ref': f'{sub_dir}'
            + f'/{calculation_hash}calibrated_error'
            + '_estimate_models_vs_ref.pkl',
        }

        if linear_correction:
            est_uncertainty_mean_vs_ref = self.get_calibrated_uncertainty(
                std_models_vs_mean, linear_correction['mean_vs_ref']
            )
            est_uncertainty_models_vs_ref = self.get_calibrated_uncertainty(
                std_models_vs_mean, linear_correction['models_vs_ref']
            )

            # Save the calibrated uncertainty estimates
            est_uncertainty_mean_vs_ref.write_data(
                outputs['calibrated_error_estimate_mean_vs_ref']
            )
            est_uncertainty_models_vs_ref.write_data(
                outputs['calibrated_error_estimate_models_vs_ref']
            )
            output_files.update(outputs)

            residual_files = {
                'residuals_mean_vs_ref': f'{sub_dir}/{calculation_hash}'
                + 'residuals_mean_vs_ref.pkl',
                'residuals_models_vs_ref': f'{sub_dir}/{calculation_hash}'
                + 'residuals_models_vs_ref.pkl',
                'residuals_models_vs_mean': f'{sub_dir}/{calculation_hash}'
                + 'residuals_models_vs_mean.pkl',
            }
            if (
                save_all
                and not self.all_files_exists(residual_files)
                and has_reference
            ):
                # Get residuals
                residuals_mean_vs_ref = self.get_residuals(
                    est_uncertainty_mean_vs_ref,
                    dev_mean_vs_ref,
                    chosen_columns=chosen_columns,
                )
                residuals_mean_vs_ref.write_data(
                    residual_files['residuals_mean_vs_ref']
                )
                residuals_models_vs_ref = self.get_residuals(
                    est_uncertainty_models_vs_ref,
                    dev_models_vs_ref,
                    chosen_columns=chosen_columns,
                )
                residuals_models_vs_mean = self.get_residuals(
                    std_models_vs_mean,
                    dev_models_vs_mean,
                    chosen_columns=chosen_columns,
                )

                residuals_models_vs_ref.write_data(
                    residual_files['residuals_models_vs_ref']
                )
                residuals_models_vs_mean.write_data(
                    residual_files['residuals_models_vs_mean']
                )
            if save_all:
                output_files.update(residual_files)

        ##########################################################
        # Compute the calibrated error estimates and real errors #
        ##########################################################
        reports = []
        if has_reference:
            reports.append(
                [
                    error_models_vs_ref,
                    'errors_models_vs_ref_geometry_id_dataset_id',
                    'errors_models_vs_ref_dataset_id',
                ]
            )
        reports.append(
            [
                est_uncertainty_models_vs_ref,
                'calibrated_error_estimate_models'
                + '_vs_ref_geometry_id_dataset_id',
                'calibrated_error_estimate_models_vs_ref_dataset_id',
            ]
        )

        for dataset_report in reports:
            data = dataset_report[0]
            name_geometry_id_dataset_id = dataset_report[1]
            name_dataset_id = dataset_report[2]
            # Check if exists
            report_names = [
                f'{sub_dir}/{calculation_hash}'
                + f'{name_geometry_id_dataset_id}.pkl',
                f'{sub_dir}/{calculation_hash}{name_dataset_id}.pkl',
            ]
            output_files[name_geometry_id_dataset_id] = report_names[0]
            output_files[name_dataset_id] = report_names[1]

            if all(
                [Path(report_name).exists() for report_name in report_names]
            ):
                continue
            else:
                error_with_metric = self.apply_error_metrics(
                    data, error_metrics
                )
                error_with_metric.data.reset_index(inplace=True, drop=True)
                error_stats = self.error_stats(
                    error_with_metric,
                    groupby_list=['dataset_id', 'geometry_id'],
                )
                if not isinstance(error_stats.data, DataSet):
                    # save as pkl
                    DataSet(error_stats.data).write_data(report_names[0])
                else:
                    error_stats.write_data(report_names[0])

                # Now do by dataset_id and save as md
                error_stats = self.error_stats(
                    error_with_metric, groupby_list=['dataset_id']
                )
                try:

                    error_stats.data.to_markdown(
                        report_names[1].replace('.pkl', '.md'), index=False
                    )
                except ModuleNotFoundError:
                    error_stats.data.to_csv(
                        report_names[1].replace('.pkl', '.md'),
                        sep='\t',
                        index=False,
                    )
        output_files.update(outputs)

        return output_files

    def generate_unique_combinations(self, elements, m, seed):
        """Generate unique combinations of elements."""
        random.seed(seed)
        random.shuffle(elements)
        combination_generator = combinations(elements, m)
        selected_combinations = set()
        max_trials = 10000000
        trials = 0
        while True:
            trials += 1
            if trials > max_trials:
                raise ValueError('Could not generate unique combinations.')
            new_combination = next(combination_generator, None)
            if new_combination is None:
                break
            if new_combination not in selected_combinations:
                selected_combinations.add(new_combination)
                yield new_combination

    def get_deviations_dataset(
        self,
        dataset,
        groupby: str = 'geometry_id',
        between_col_category: str = 'method_id',
        chosen_columns: list = None,
        categories1: list = None,
        categories2: list = None,
    ):
        """Get deviations between categories in a dataset.

        Arguments
        ---------
        dataset : DataSet
            The dataset to be used for deviation calculations.
            This dataset should contain the columns: method_id, geometry_id,
            and the chosen_columns.

        groupby : str
            indexing to take the mean over.

        between_col_category : str
            The column to take the mean over, for example, 'method_id'.

        chosen_columns : list
            The columns which will be used for the deviation calculations.

        categories1 : list
            The categories to be used for the deviation calculations.
            If None, all unique categories will be used.

        categories2 : list
            The categories to be used for the deviation calculations.
            If None, all unique categories will be used.

        Returns
        -------
        DataSet
            The dataset containing deviations between categories.
            Columns: geometry_id, dataset_id,
            category1, category2, property, deviation

        """
        if isinstance(dataset, DataSet):
            sub_dataset = dataset.data.copy()
        else:
            sub_dataset = dataset.copy()

        # Filter by chosen_columns if provided
        if chosen_columns is not None:
            sub_columns = (
                chosen_columns
                + [groupby, between_col_category]
                + ['dataset_id']
            )
            sub_columns = [
                col for col in sub_columns if col in sub_dataset.columns
            ]
            sub_dataset = sub_dataset[sub_columns]

        # Prepare the DataFrame to capture deviations
        deviation_records = []

        for geom_id, group in sub_dataset.groupby(groupby):
            unique_categories = group[between_col_category].unique()
            cat1 = (
                categories1 if categories1 is not None else unique_categories
            )
            cat2 = (
                categories2 if categories2 is not None else unique_categories
            )

            # Generate all possible category pairs,
            # filtering out pairs with the same category
            category_pairs = [(x, y) for x in cat1 for y in cat2 if x != y]

            # Iterate over each category pair
            for cat_i, cat_j in category_pairs:
                # Check if both categories are present
                # in the group's unique categories
                if cat_i in unique_categories and cat_j in unique_categories:

                    # Filter dataset for those categories
                    data_i = group[group[between_col_category] == cat_i].iloc[
                        0
                    ]  # Get the first row of each category group
                    data_j = group[group[between_col_category] == cat_j].iloc[
                        0
                    ]

                    # Prepare a record dictionary to store deviations
                    cat1, cat2 = sorted([cat_i, cat_j])
                    record = {
                        groupby: geom_id,
                        'category1': cat1,
                        'category2': cat2,
                    }

                    # Iterate over the columns to calculate deviations
                    for column in (
                        chosen_columns
                        if chosen_columns is not None
                        else group.columns.drop(
                            [groupby, between_col_category]
                        )
                    ):
                        # Compute deviations
                        try:
                            deviation = data_i[column] - data_j[column]
                        except TypeError:
                            deviation = np.array(data_i[column]) - np.array(
                                data_j[column]
                            )
                        except ValueError:
                            continue
                        deviation_column_name = f'{column}'
                        record[deviation_column_name] = deviation

                    # Append the record dictionary to the list
                    deviation_records.append(record)

        # Convert the records into a pandas DataFrame
        deviation_df = pd.DataFrame(deviation_records)

        # add back dataset_id if it was present, as indexed by geometry_id
        if 'dataset_id' in dataset.data.columns:
            unique_dataset = dataset.data[
                ['geometry_id', 'dataset_id']
            ].drop_duplicates(subset='geometry_id', keep='first')

            # Merge while ensuring 'geometry_id' is unique
            deviation_df = deviation_df.merge(
                unique_dataset, on='geometry_id', how='left'
            )
            column_order = ['geometry_id', 'dataset_id'] + [
                col
                for col in deviation_df.columns
                if col not in ['geometry_id', 'dataset_id']
            ]
            deviation_df = deviation_df[column_order]

        # Handle DataSet return type
        if isinstance(dataset, DataSet):
            return DataSet(deviation_df, units=dataset.units)

        return deviation_df

    def get_mean_error(
        self,
        dataset,
        groupby_list: list[str] = ['dataset_id'],
        chosen_columns: list[str] = ['energy', 'forces', 'virial'],
    ):
        """Calculate mean error over method_id."""
        dataset_shaped = deepcopy(dataset)
        if isinstance(dataset, pd.DataFrame):
            dataset_shaped = DataSet(dataset)

        dataset_shaped.standardize_columns(shaped=True)
        chosen_columns = [
            column
            for column in chosen_columns
            if column in dataset_shaped.data.columns
        ]

        # Make sure chosen_columns is not empty
        if len(chosen_columns) == 0:
            raise ValueError('No chosen columns in dataset.')

        # Make sure it is a deviations dataset
        if 'category1' not in dataset_shaped.data.columns:
            raise ValueError('category1 not in dataset.')
        if 'category2' not in dataset_shaped.data.columns:
            raise ValueError('category2 not in dataset.')
        if 'dataset_id' not in dataset_shaped.data.columns:
            # If dataset_id is not present, we add a nan column
            # this will fail for inhomogeneous datasets
            dataset_shaped.data['dataset_id'] = ['nan'] * len(
                dataset_shaped.data
            )

        # Get mean error for each dataset_id
        mean_error_records = []
        for dataset_id, group in dataset_shaped.data.groupby(groupby_list):
            # Get mean error for each category pair
            for category_pair, group_cat in group.groupby(
                ['category1', 'category2']
            ):
                category1, category2 = category_pair
                # Get mean error for each column
                for column in chosen_columns:
                    # Check shape of column and calculate mean error
                    if len(group_cat[column].shape) == 1:
                        scalars = np.array(group_cat[column].values.tolist())
                        errors = np.abs(scalars)
                    else:
                        tensors = np.array((group_cat[column].values.tolist()))
                        errors = np.linalg.norm(tensors, axis=-1)

                    mean_error = np.mean(errors)
                    max_error = np.max(errors)
                    min_error = np.min(errors)

                    # make 6 decimals, scientific notation
                    mean_error = f'{mean_error:.6e}'
                    max_error = f'{max_error:.6e}'
                    min_error = f'{min_error:.6e}'
                    if dataset.has_units:
                        column_units = dataset.data.attrs['column_units'][
                            column
                        ][0]
                    else:
                        column_units = 'None'
                    dict_entry = {}
                    # add groupby_list
                    for groupby in groupby_list:
                        dict_entry[groupby] = group[groupby].unique()[0]
                    dict_entry.update(
                        {
                            'category1': category1,
                            'category2': category2,
                            'property': column,
                            'units': column_units,
                            'mean_error': mean_error,
                            'max_error': max_error,
                            'min_error': min_error,
                        }
                    )
                    mean_error_records.append(dict_entry)
        mean_error_df = pd.DataFrame(mean_error_records)
        mean_error_df = mean_error_df[mean_error_df['mean_error'].notna()]
        mean_error_df = mean_error_df[mean_error_df['mean_error'] != 'nan']

        if isinstance(dataset, DataSet):
            if dataset.has_units:
                return DataSet(mean_error_df, units=dataset.units)
            else:
                return DataSet(mean_error_df)
        else:
            return mean_error_df

    def get_residuals(
        self,
        dataset_uncertainty: DataSet,
        dataset_errors: DataSet,
        chosen_columns: List[str] = ['energy', 'forces'],
    ):
        """Determine the residuals from the uncalibrated uncertainty.

        Arguments
        ---------
        dataset_uncertainty : DataSet
            The dataset containing the uncertainty estimates.

        dataset_errors: DataSet
            The dataset containing the errors.

        chosen_columns : List[str]
            The columns to be used for the deviation report.

        Returns
        -------
        DataSet
            A dataset containing the residuals, which are the errors
            divided by the uncertainty estimates for each geometry id.
            This dataset is shaped same as dataset_errors.

        """
        dataset_residuals = deepcopy(dataset_errors)
        matched_uncertainty = self.get_matched_dataset(
            dataset_errors, dataset_uncertainty
        )

        for column in chosen_columns:
            residuals = (
                dataset_errors.data[column].values
                / matched_uncertainty.data[column].values
            )
            dataset_residuals.data[column] = residuals

        for column in dataset_residuals.data.columns:
            if column not in chosen_columns + ['geometry_id', 'dataset_id']:
                dataset_residuals.data = dataset_residuals.data.drop(
                    column, axis=1
                )

        return dataset_residuals

    def get_matched_dataset(
        self,
        large_dataset: DataSet,
        small_dataset: DataSet,
        column: str = 'geometry_id',
    ):
        """Repeat the small dataset to match the large dataset.

        Arguments
        ---------
        large_dataset : DataSet
            The dataset to be matched.

        small_dataset : DataSet
            The dataset to be repeated.

        column : str
            The column to be matched.

        Returns
        -------
        DataSet
            The matched dataset.

        """
        small_dataset.data.index = small_dataset.data[column]
        tmp_dataset = deepcopy(large_dataset)
        # Iterate through each column in the small dataset
        for col in small_dataset.data.columns:
            if col != column:  # no need to map geometry_id to itself
                # create a new column in large dataset by
                # mapping values from the small dataset
                tmp_dataset.data[col] = large_dataset.data[column].map(
                    small_dataset.data[col]
                )

        matched_dataset = DataSet(tmp_dataset.data)
        return matched_dataset

    def get_correction_from_residuals(
        self,
        dataset_errors: DataSet,
        dataset_uncertainty: DataSet,
        chosen_columns: List[str] = ['energy', 'forces', 'virial'],
        robust: bool = True,
    ):
        """Determine the correction factor from the residuals.

        Arguments
        ---------
        dataset_errors : DataSet
            The dataset containing the residuals.

        dataset_uncertainty : DataSet
            The dataset containing the uncertainty estimates.

        chosen_columns : List[str]
            The columns to be used for the deviation report.

        Returns
        -------
        Dict
            The correction factor for each chosen column.

        """
        correction_factors = {}

        # Repeat the uncertainty dataset to match the errors dataset
        matched_uncertainty = self.get_matched_dataset(
            dataset_errors, dataset_uncertainty
        )

        for column in chosen_columns:
            errors = self.flatten(dataset_errors.data[column])
            sigma = self.flatten(matched_uncertainty.data[column])
            correction_factors[column] = self.get_corrections_from_arrays(
                sigma, errors, robust=robust
            )

        return correction_factors

    def get_corrections_from_arrays(self, sigma, residuals, robust=True):
        """Determine the correction factor from the residuals."""
        # if robust is true, we want to exclude outliers, we do this by looking
        # at the residuals, and removing the ones that are more than
        # 5 standard deviations away from the mean
        if robust:
            exclude_idx = np.abs(residuals - np.mean(residuals)) > 5 * np.std(
                residuals
            )
            residuals = residuals[~exclude_idx]
            sigma = sigma[~exclude_idx]

        def fitness(*args):
            sig = self.uncertainty_model(sigma, args)
            sum_tmp = np.sum(
                np.log(2 * np.pi) + np.log(sig**2) + (residuals) ** 2 / sig**2
            )
            return 0.5 * sum_tmp / len(residuals)

        if robust:
            bounds = [(0.0, None), (0.0, None)]
        else:
            bounds = [(0.0, None), (0.0, None)]
        res = minimize(
            lambda x: fitness(*x),
            [1.0, 0.5],
            method='Nelder-Mead',
            bounds=bounds,
            options={'maxiter': 10000},
        )
        correction_factors = res.x.tolist()
        return correction_factors

    def get_calibrated_uncertainty(
        self, dataset_uncertainty: DataSet, correction_factors: dict
    ):
        """Calibrate the uncertainty estimates using the correction factors.

        Arguments
        ---------
        dataset_uncertainty : DataSet
            The dataset containing the uncertainty estimates.

        correction_factors : dict
            The correction factor for each chosen column.

        Returns
        -------
        DataSet
            The calibrated uncertainty estimates.

        """
        calibrated_uncertainty = deepcopy(dataset_uncertainty)
        for column in correction_factors.keys():
            calibrated_uncertainty.data[column] = self.uncertainty_model(
                dataset_uncertainty.data[column].values,
                correction_factors[column],
            )
        return calibrated_uncertainty

    def uncertainty_model(self, x, parameters):
        """Model for the correction of  uncertainty.

        Arguments
        ---------
        x : np.ndarray
            The x values.

        parameters : Tuple
            The parameters for the model.

        Returns
        -------
        np.ndarray
            The model values.

        """
        a, c = parameters
        return a * x**c

    def uncertainty_vs_rmse(self, uncertainty, rmse, bins=20):
        """Calculate the uncertainty vs rmse.

        Arguments
        ---------
        uncertainty : np.ndarray
            The uncertainty estimates.

        rmse : np.ndarray
            The root mean square error.

        bins : int
            The number of bins to use for the uncertainty.

        Returns
        -------
        Tuple
            The bin centers and the mean rmse for each bin.

        """
        # Check if bins is an integer
        if isinstance(bins, int):
            bins = np.linspace(min(uncertainty), max(uncertainty), num=bins)
        elif isinstance(bins, list):
            bins = np.array(bins)
        elif isinstance(bins, np.ndarray):
            pass
        else:
            raise ValueError('bins should be an integer, list or numpy array.')

        binned_rmse = np.digitize(uncertainty, bins)
        rmse_bins_means = [
            np.mean(rmse[binned_rmse == i]) for i in range(1, len(bins))
        ]
        # make sure they are numpy arrays
        bins = np.array(bins)
        rmse_bins_means = np.array(rmse_bins_means)

        return 0.5 * (bins[:-1] + bins[1:]), rmse_bins_means

    def block_average_uncertainty_vs_rmse(
        self, uncertainty, rmse, bins, nblocks
    ):
        """Calculate the block average uncertainty vs rmse.

        Arguments
        ---------
        uncertainty : np.ndarray
            The uncertainty estimates.

        rmse : np.ndarray
            The root mean square error.

        bins : int or np.ndarray
            The number of bins or the bin edges to use for the uncertainty.

        nblocks : int
            The number of blocks to use for the uncertainty.

        Returns
        -------
        Tuple
            The bin centers, the mean rmse for each bin, and the block errors.

        """
        # Call the original function on the full
        # dataset to get bin centers and bin means
        bin_centers, rmse_bin_means = self.uncertainty_vs_rmse(
            uncertainty, rmse, bins
        )

        # Get bin edges from bin centers
        bin_edges = np.zeros(len(bin_centers) + 1)
        bin_size = bin_centers[1] - bin_centers[0]
        bin_edges = bin_centers - bin_size / 2
        bin_edges = np.append(bin_edges, bin_centers[-1] + bin_size / 2)
        block_errors = []

        # Determine the size of each block
        block_size = len(uncertainty) // nblocks

        histograms = [
            self.uncertainty_vs_rmse(
                uncertainty[i * block_size: (i + 1) * block_size],  # fmt: skip
                rmse[i * block_size: (i + 1) * block_size],  # fmt: skip
                bins
            )
            for i in range(nblocks)
        ]

        # Calculate the block errors
        for bin_tmp in range(len(bin_centers)):
            block_errors.append(
                np.std(
                    [histogram[1][bin_tmp] for histogram in histograms], ddof=1
                )
            )

        # Make sure they are numpy arrays
        bin_centers = np.array(bin_centers)
        rmse_bin_means = np.array(rmse_bin_means)
        block_errors = np.array(block_errors)

        return bin_centers, rmse_bin_means, block_errors

    def all_files_exists(self, files):
        """Determine whether files exists.

        Arguments
        ---------
        files : Dict or list.
            files to be verified if exist.

        Returns
        -------
        bool
            Wheteher all the files in files exist.

        """
        if isinstance(files, Dict):
            tmp_files = files.values()
        elif isinstance(files, List):
            tmp_files = deepcopy(files)
        else:
            raise ValueError('files need to be a list or dictionary!')

        exists = [Path(f).exists() for f in tmp_files]

        return sum(exists) == len(exists)

    def flatten(self, data):
        """Flatten a list of lists."""
        try:
            return np.array(data.values.tolist()).flatten()
        except ValueError:
            list_rep = data.values.flatten().tolist()
            return np.array(list(itertools.chain(*list_rep)))

    def ue_per_num_models(
        self,
        predictions,
        error_models_vs_ref,
        n_models,
        chosen_columns: List[str] = ['energy', 'forces', 'virial'],
        reference_method_id: str = 'reference',
        robust: bool = True,
    ):
        """Calculate the uncertainty estimate per number of models.

        Arguments
        ---------
        dataset : DataSet
            The dataset containing the uncertainty estimates.

        chosen_columns : List[str]
            The columns to be used for the deviation report.

        Returns
        -------
        Dict
            The uncertainty estimates per number of models.

        """
        predictions_tmp = deepcopy(predictions)

        # remove reference
        predictions_tmp.data = predictions_tmp.data[
            predictions_tmp.data['method_id'] != reference_method_id
        ]
        model_categories = predictions_tmp.data['method_id'].unique()

        # Create random  model combinations used for calibration
        total_num_models = len(model_categories)
        model_categories_tmp = model_categories.copy()
        unique_combinations_generator = self.generate_unique_combinations(
            model_categories_tmp, n_models, n_models
        )

        random_model_combinations = [
            next(unique_combinations_generator)
            for i in range(total_num_models // n_models)
        ]
        # loop random_model_combinations
        df_errors = pd.DataFrame()
        df_sigma = pd.DataFrame()
        for i, model_combinations in enumerate(random_model_combinations):
            # Compute for all geometries
            model_combinations_and_ref = list(model_combinations) + [
                reference_method_id
            ]

            df_sigma_tmp = (
                predictions_tmp.data[
                    predictions_tmp.data['method_id'].isin(
                        model_combinations_and_ref
                    )
                ]
                .groupby('geometry_id')
                .agg(self.std_agg_from_values)
            )
            df_sigma_tmp.reset_index(inplace=True)

            df_errors_tmp = error_models_vs_ref.data[
                error_models_vs_ref.data['category1'].isin(
                    model_combinations_and_ref
                )
            ]

            df_errors_tmp = df_errors_tmp[
                df_errors_tmp['category2'].isin(
                    list(model_combinations_and_ref)
                )
            ]
            # reset index
            df_errors_tmp.reset_index(inplace=True, drop=True)

            # use the matched dataset
            matched_df = self.get_matched_dataset(
                DataSet(df_errors_tmp), DataSet(df_sigma_tmp)
            )
            df_sigma = pd.concat([df_sigma, matched_df.data], axis=0)
            df_errors = pd.concat([df_errors, df_errors_tmp], axis=0)

        # Estimate correction factors
        correction_factors = {}
        for column in chosen_columns:
            residuals = self.flatten(df_errors[column])
            sigma = self.flatten(df_sigma[column])
            correction_factors[column] = self.get_corrections_from_arrays(
                sigma, residuals
            )

        return correction_factors

    def error_stats(self, error, groupby_list):
        """Calculate the mean, min and max error for an error dataset.

        Arguments
        ---------
        error : DataSet
            The error dataset, can contain floats and lists of floats.

        groupby_list : list
            The columns to group by, when calculating the error stats.

        Returns
        -------
        DataSet
            The error stats dataset.

        """
        error_stats = error.data.groupby(groupby_list).agg(
            self.mean_min_max_agg
        )
        error_stats = error_stats.reset_index()
        value_vars = list(error.data.columns)
        # try to remove groupby_list
        for spurious in groupby_list + [
            'category1',
            'category2',
            'geometry_id',
        ]:
            try:
                value_vars.remove(spurious)
            except ValueError:
                pass

        error_stats = error_stats.melt(
            id_vars=groupby_list,
            value_vars=value_vars,
            var_name='property',
            value_name='value',
        )

        def split_values(row):
            if isinstance(row['value'], list) and len(row['value']) == 3:
                return pd.Series(
                    {
                        'mean_error': row['value'][0],
                        'min_error': row['value'][1],
                        'max_error': row['value'][2],
                    }
                )
            return pd.Series(
                {'mean_error': None, 'min_error': None, 'max_error': None}
            )

        # Apply the function along the rows (axis=1)
        error_expanded = error_stats.apply(split_values, axis=1)

        # Join the expanded error data back to the original
        # DataFrame minus the 'value' column
        error_stats = error_stats.drop('value', axis=1).join(error_expanded)

        return DataSet(error_stats)

    def apply_error_metrics(self, data, error_metric):
        """Apply metrics transformations to columns.

        Parameters
        ----------
        data : pd.DataFrame or DataSet
            The input DataFrame with error columns.

        error_metric : dict
            A dictionary mapping column names in `data` to
            functions that should be applied
            to each column. These functions must be applicable to a pd.Series.

        Returns
        -------
        pd.DataFrame or DataSet
            The DataFrame with the error metrics applied.

        """
        if isinstance(data, DataSet):
            data_tmp = data.data.copy()
        else:
            data_tmp = data.copy()

        # Apply each function to the specified column
        for column, func in error_metric.items():
            if column in data_tmp.columns:
                # Apply the function to the column
                data_tmp[column] = data_tmp[column].apply(func)

        if isinstance(data, DataSet):
            return DataSet(data_tmp, units=data.units)
        else:
            return data_tmp

    def trace_metric(self, data):
        """Trace metric for error analysis.

        Parameters
        ----------
        data : np.ndarray
            The input data.

        Returns
        -------
        np.ndarray
            The trace of the data.

        """
        return np.trace(np.reshape(data, (-1, 3, 3)))

    def atomcomponent_metric(self, data):
        """Atom component metric for error analysis.

        Parameters
        ----------
        data : np.ndarray
            The input data.

        Returns
        -------
        np.ndarray
            The atom component of the data.

        """
        return np.sqrt(np.sum(np.reshape(data, (-1, 3)) ** 2, axis=-1))
