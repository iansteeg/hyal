import copy
import glob
import inspect
import itertools
import json
from copy import deepcopy
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
import pandas as pd
from hyif import Generic, HylleraasMLInterface
from hyif.utils import unique_filename
from hyobj import DataSet, Molecule, PeriodicSystem, Units
from hyset import (ComputeSettings, File, create_compute_settings,
                   run_concurrent)
from hytools.exception_handler import exception_handler
from hytools.logger import get_logger
from vesin import NeighborList

from .conref import ConRef
from .uncertainty_estimatimation import UncertaintyEstimation


class ActiveLearning:
    """Hylleraas Active Learning class."""

    def __init__(
        self,
        *args,
        ml_engines: Optional[List[Callable]] = None,
        calculator: Optional[Callable] = None,
        sampler: Optional[Callable] = None,
        variant: Optional[Any] = 'ConRef',
        conf_ref_compute_settings: Optional[ComputeSettings] = None,
        python_name: Optional[str] = 'python',
        units: Optional[Units] = Units('metal'),
        local_ml_engines: Optional[List[Callable]] = None,
        log_file: Optional[str] = 'hyal.log',
        logging_level: Optional[str] = 'INFO',
        **kwargs,
    ) -> None:
        # initialize logger
        config_file = kwargs.pop(
            'config_file', Path(__file__).parent / 'logger_config.yml'
        )
        self.logger = get_logger(
            logger_name='hyal',
            log_file=log_file,
            logging_level=logging_level,
            config_file=config_file,
        )

        self.logger.debug('Initializing Hylleraas Active Learning')

        self.calculator: Callable = calculator  # type: ignore
        self.sampler: Callable = sampler  # type: ignore
        self.ml_engines: Union[List[Callable]] = ml_engines  # type: ignore
        if local_ml_engines is None:
            self.local_ml_engines = ml_engines
        else:
            self.local_ml_engines = local_ml_engines
        self.units: Units = units
        # Initialize config generator
        if conf_ref_compute_settings is not None:
            self.conf_ref_compute_settings = conf_ref_compute_settings
        else:
            self.conf_ref_compute_settings = create_compute_settings()

        self.python_name = python_name
        self.conf_ref_interface = Generic(
            {'program': self.python_name}, self.conf_ref_compute_settings
        )

        self.variant = variant
        self.config_generator_interface = kwargs.pop(
            'config_generator_interface', None
        )
        if variant is not None and isinstance(variant, str):
            if variant == 'ConRef':
                with open(inspect.getfile(ConRef), 'r') as fp:
                    prog = fp.read()
                self.config_generator_interface = ConRef()
            elif isinstance(variant, str):
                prog = variant
                if self.config_generator_interface is None:
                    raise Exception(
                        'config_generator_interface must be provided'
                    )
            else:
                raise Exception('variant must be a string or a callable')

            def setup_config_generator(
                models,
                system: Union[dict, str],
                options: Union[dict, str],
                **kwargs,
            ):
                if isinstance(system, str):
                    system = json.loads(Path(system).read_text())
                if isinstance(options, str):
                    options = json.loads(Path(options).read_text())

                if isinstance(system, dict):
                    for key in system.keys():
                        if isinstance(system[key], np.ndarray):
                            system[key] = system[key].tolist()
                data_files = []
                system_str = json.dumps(system, indent=4)
                options_str = json.dumps(options, indent=4)
                models_str = models
                hash_str = unique_filename(
                    system_str.split('\n')
                    + options_str.split('\n')
                    + models_str
                )
                name_samples = hash_str + '.input'
                name_options = hash_str + '.options'
                name_output = hash_str + '.output'
                name_stdout = hash_str + '.out'

                files_to_write = [
                    File(name=name_samples, content=system_str),
                    File(name=name_options, content=options_str),
                ]
                # fmt: off
                for model in models:
                    data_files.append(
                        File(
                            name=model,
                            handler=self.conf_ref_compute_settings
                            .run_settings.file_handler,
                        )
                    )
                run_opt = kwargs.pop('run_opt', {})
                run_opt['data_files'] = data_files
                run_opt['files_to_write'] = files_to_write

                output_file = File(
                    name=name_output,
                    handler=self.conf_ref_compute_settings
                    .run_settings.file_handler,
                )
                stdout_file = File(
                    name=name_output.replace('.output', '.out'),
                    handler=self.conf_ref_compute_settings
                    .run_settings.file_handler,
                )

                run_opt['stdout_file'] = stdout_file
                run_opt['stderr_file'] = stdout_file
                run_opt['output_file'] = output_file
                files_to_parse = kwargs.pop('files_to_parse', [])
                kwargs['output_file'] = name_stdout

                files_to_parse.append(
                    File(
                        name=name_samples,
                        handler=self.conf_ref_compute_settings
                        .run_settings.file_handler,
                    )
                )
                files_to_parse.append(
                    File(
                        name=name_output,
                        handler=self.conf_ref_compute_settings
                        .run_settings.file_handler,
                    )
                )
                files_to_parse.append(
                    File(
                        name=name_output.replace('.output', '.xyz'),
                        handler=self.conf_ref_compute_settings
                        .run_settings.file_handler,
                    )
                )
                # fmt: on
                run_opt['files_to_parse'] = files_to_parse

                if self.conf_ref_compute_settings.arch_type == 'remote':
                    model_names = [str(Path(model).name) for model in models]
                else:
                    model_names = [str(model) for model in models]

                run_opt['args'] = [
                    '--models',
                    ' '.join(model_names),
                    '--input_geometries',
                    name_samples,
                    '--options',
                    name_options,
                    '--output',
                    name_output,
                ]
                return prog, run_opt, kwargs

            def arun_config_generator(
                models,
                system: Union[dict, str],
                options: Union[dict, str],
                **kwargs,
            ):
                prog, run_opt, kwargs = setup_config_generator(
                    models, system, options, **kwargs
                )
                return self.conf_ref_interface.arun(
                    prog, run_opt=run_opt, **kwargs
                )

            def run_config_generator(
                models,
                system: Union[dict, str],
                options: Union[dict, str],
                **kwargs,
            ):
                prog, run_opt, kwargs = setup_config_generator(
                    models, system, options, **kwargs
                )
                return self.conf_ref_interface.run(
                    prog, run_opt=run_opt, **kwargs
                )

            self.run_config_generator = run_config_generator
            self.arun_config_generator = arun_config_generator

        else:
            self.run_config_generator = None  # type: ignore
            self.arun_config_generator = None  # type: ignore

    @exception_handler(ValueError)
    def generate_dataset(
        self, data: Union[str, Path, DataSet], **kwargs
    ) -> DataSet:
        """Generate dataset."""
        self.logger.debug('Generating dataset')
        if isinstance(data, DataSet):
            return data
        elif isinstance(data, (str, Path)):
            if Path(data).is_file():
                return DataSet(data)
            return self.dataset_from_dir(Path(data), **kwargs)
        else:
            raise ValueError('data should be a DataSet, path or a directory')

    @exception_handler(KeyError)
    def remove_nonconverged(self, dataset: DataSet) -> DataSet:
        """Remove nonconverged geometries."""
        self.logger.debug('Removing nonconverged geometries')
        dataset_out = deepcopy(dataset)
        initial_len = len(dataset_out)
        if 'is_converged' in dataset_out.data.columns:
            is_coverged = dataset_out.data['is_converged']
            is_coverged[is_coverged != 1] = False
            dataset_out.data = dataset_out.data[is_coverged]
            dataset_out.data = dataset_out.data.reset_index(drop=True)

        self.logger.debug(
            f'Removed {initial_len - len(dataset_out)} nonconverged geometries'
        )
        return dataset_out

    @exception_handler(ValueError)
    @exception_handler(AttributeError)
    @exception_handler(KeyError)
    def dataset_from_dir(
        self, path: Path, calculator=None, **kwargs
    ) -> Union[Path, DataSet]:
        """Get initial set from dir."""
        self.logger.debug('Getting dataset from directory')

        files: List[str] = [f.name for f in sorted(path.iterdir())]
        if all(['type_map.raw' in files, 'type.raw' in files]):
            return path

        parse_input = kwargs.pop('parse_input', True)

        if calculator is None:
            calculator = self.calculator

        input_suffix = calculator.__dict__.get('input_file_suffix', '.inp')
        output_suffix = calculator.__dict__.get('output_file_suffix', '.out')

        if input_suffix is not None:
            not_calculated: list = []
            num_input = 0
            num_output = 0
            ffiles: List[Path] = sorted(path.iterdir())
            for f in ffiles:
                if f.suffix == input_suffix:
                    num_input += 1
                    if not f.with_suffix(output_suffix) in ffiles:
                        not_calculated.append(f.name)
                elif f.suffix == output_suffix:
                    num_output += 1
            if num_output + num_input <= 0:
                self.logger.warning(
                    'could not find either input'
                    + f'{input_suffix} nor output files {output_suffix}'
                )
                return None

            if len(not_calculated) > 0:
                w = 'the following input files'
                w += ' dont have a corresponding output:\n'
                w += '\n'.join(not_calculated)
                self.logger.warning(w)

        if not hasattr(calculator, 'OutputParser'):
            raise AttributeError(
                'calculator should contain an OutputParser'
                + 'please contact '
                + f'{calculator.author}'  # type: ignore
            )  # type: ignore
        parser = calculator.OutputParser  # type: ignore
        match = '*' + output_suffix
        try:
            ds_out = DataSet(
                path,
                match=match,
                parser=parser,
                parse=True,
                units=calculator.units,  # type: ignore
            )
        except IndexError:
            ds_out = DataSet({}, units=calculator.units)

        if input_suffix is not None and parse_input:
            if not hasattr(calculator, 'InputParser'):
                raise AttributeError(
                    'calculator should contain an InputParser '
                    + 'please contact '
                    + f'{calculator.author}'  # type: ignore
                )  # type: ignore

            parser = calculator.InputParser  # type: ignore
            match = '*' + input_suffix
            ds_inp = DataSet(
                path,
                match=match,
                parser=parser,
                parse=True,
                units=calculator.units,  # type: ignore
            )

            # Check that input and output files match
            if (
                'input_file' in ds_inp.data.columns
                and 'output_file' in ds_out.data.columns
            ):
                hashes_inp = [
                    Path(f).stem.replace(input_suffix, '')
                    for f in ds_inp.data['input_file']
                ]
                hashes_out = [
                    Path(f).stem.replace(output_suffix, '')
                    for f in ds_out.data['output_file']
                ]
                idx_to_drop_inp = []
                idx_to_drop_out = []
                for i, hash_inp in enumerate(hashes_inp):
                    if hash_inp not in hashes_out:
                        idx_to_drop_inp.append(i)
                        continue
                for i, hash_out in enumerate(hashes_out):
                    if hash_out not in hashes_inp:
                        idx_to_drop_out.append(i)
                        continue
                ds_inp.data = ds_inp.data.drop(idx_to_drop_inp)
                ds_inp.data = ds_inp.data.reset_index(drop=True)
                ds_out.data = ds_out.data.drop(idx_to_drop_out)
                ds_out.data = ds_out.data.reset_index(drop=True)

            else:
                self.logger.warning(
                    'No input_file and output_file key found in dataset. '
                    'Could not match input and output files, use interface at '
                    'own risk or add input_file and output_file to interface'
                )

            df = pd.concat([ds_inp.data, ds_out.data], axis=1)
            # remove duplicate columns
            df = df.loc[:, ~df.columns.duplicated()]
            ds = DataSet(df, units=calculator.units)
        else:
            ds = ds_out

        # Convert units
        ds.change_units(self.units)
        # Remove columns with NaN
        # Todo: Make dataset more robust to NaN
        ds.data = ds.data.dropna(how='all')

        forces_list = ['gradient', 'gradients', 'force', 'forces']
        energy_list = ['energy', 'en']

        idx_to_drop = []
        for i in range(len(ds)):
            for key in energy_list + forces_list:
                if key in ds.data.iloc[i].keys():
                    values = ds.data.iloc[i][key]
                    has_nan = np.any(np.isnan(np.array(values)))
                    if has_nan:
                        # warnings.warn(
                        #     f'could not find {key} for {ds.data.iloc[i]},\n'
                        #     + 'removing it from dataset.'  # type: ignore
                        # )
                        idx_to_drop.append(i)
                        continue

        ds.data = ds.data.drop(idx_to_drop)
        ds.data.reset_index(inplace=True, drop=True)

        return ds

    @exception_handler(Exception)
    def create_folder(
        self,
        dirname: Union[str, Path],
        prefix: Optional[Union[str, Path]] = None,
    ) -> Path:
        """Create folder."""
        try:
            if prefix is not None:
                prefix = Path(prefix)
                path = Path(prefix / dirname)
            else:
                path = Path(dirname)
        except Exception:
            raise Exception(f'could not create path from path {dirname}')
        else:
            path.mkdir(mode=0o777, parents=True, exist_ok=True)

        return path

    @exception_handler(ValueError)
    @exception_handler(FileNotFoundError)
    @exception_handler(KeyError)
    def run(
        self,
        systems: Dict,
        options: Union[Dict, List[Dict]] = {},
        initial_set: Optional[Any] = None,
        maxiter: Optional[int] = None,
        seed: Optional[int] = 0,
        append=False,
        avoid_redo=False,
        restart=True,
        sub_dir: Optional[Union[str, Path]] = None,
        geometry_pool: Optional[Dict] = {},
        **kwargs,
    ):
        """Perform an active learning model optimization."""
        self.logger.info('Running active learning')
        # Load options
        # active learning options: #
        (al_opts_its, sampling_opts_its, confref_opts_its, mlp_opts_its) = (
            self.parse_options(options, systems, maxiter=maxiter)
        )
        rand_num_gen = np.random.Generator(np.random.MT19937(seed=seed))

        if sub_dir is None:
            sub_dir_hyal = './'
        else:
            sub_dir_hyal = str(self.create_folder(sub_dir))
            if not sub_dir_hyal.endswith('/'):
                sub_dir_hyal += '/'

        # Dictionary object containing all training set objects
        # to be read by the ML engine
        data_sets = {}
        if initial_set is None:
            ret = self.make_initial_set(
                systems,
                num_initial=al_opts_its[0]['num_initial'],
                dev=al_opts_its[0]['initial_displacement'],
                generator=rand_num_gen,
                sub_dir=sub_dir_hyal,
            )
            data_sets.update(ret)
        if isinstance(initial_set, dict):
            data_sets.update(self.correct_units(initial_set))
        else:
            # Load initial set if either a list or a filename is provided
            if isinstance(initial_set, list):
                for i, fn in enumerate(initial_set):
                    data_sets['system_' + str(i + 1)] = self.generate_dataset(
                        fn
                    )
            else:
                data_sets['system_' + str(1)] = self.generate_dataset(
                    initial_set
                )

        # Make sure datasets have correct naming:
        for i, (name, ds) in enumerate(data_sets.items()):
            ds.standardize_columns()

        # Apply corrections to data_sets from initial set
        calculator_corrections = al_opts_its[-1].get(
            'calculator_corrections', False
        )
        if calculator_corrections:
            for system_name, ds in data_sets.items():
                data_sets[system_name] = self.correct_dataset(
                    ds, calculator_corrections
                )

        # Active learning loop
        for iter_num, (
            al_opts,
            sampling_opts,
            confref_opts,
            mlp_opts,
        ) in enumerate(
            zip(al_opts_its, sampling_opts_its, confref_opts_its, mlp_opts_its)
        ):
            self.logger.info(f'Active learning loop iteration {iter_num}')
            if (
                not restart
                or not Path(f'{sub_dir_hyal}/iter_{iter_num}').exists()
            ):
                self.create_folder(f'{sub_dir_hyal}/iter_{iter_num}')
                with open(
                    f'{sub_dir_hyal}/iter_{iter_num}/hyal_settings_iter.json',
                    'w',
                ) as f:
                    json.dump(al_opts, f, indent=4)

            if iter_num > 0:
                with open(
                    f'{sub_dir_hyal}/iter_{iter_num-1}'
                    + '/hyal_settings_iter.json',
                    'r',
                ) as f:
                    state = json.load(f)['final_random_state']
                rand_num_gen = self.generator_from_state(state)

            # Create filters
            calculator_filters = {}
            for filter_name, filter_properties in al_opts[
                'calculator_filters'
            ].items():  # type: ignore
                calculator_filters[filter_name] = self.set_filter(
                    filter_name, filter_properties
                )
            ml_filters = {}
            for filter_name, filter_properties in al_opts[
                'ml_filters'
            ].items():  # type: ignore
                ml_filters[filter_name] = self.set_filter(
                    filter_name, filter_properties
                )
            if iter_num > 0:
                self.logger.debug('Adding previous data sets to current data')
                # find all calculator subdirectories from previous iteration
                # and add to data_sets dictionary
                for calc_sub_dir in glob.glob(
                    f'{sub_dir_hyal}/iter_{iter_num-1}/calculator/*/'
                ):
                    system_name = calc_sub_dir.split('/')[-2]
                    new_set = self.generate_dataset(calc_sub_dir)

                    if new_set:
                        new_set.standardize_columns()
                        # Apply corrections to new_set
                        calculator_corrections = al_opts.get(
                            'calculator_corrections', False
                        )
                        if calculator_corrections:
                            new_set = self.correct_dataset(
                                new_set, calculator_corrections
                            )

                        if system_name in data_sets.keys():
                            data_sets[system_name] = (
                                data_sets[system_name] + new_set
                            )
                        else:
                            data_sets[system_name] = new_set

            # Apply filters and remove nonconvered from data_sets
            names_to_remove = []
            for system_name, ds in data_sets.items():
                ds_out = self.remove_nonconverged(ds)
                ds_out = self.apply_filters(ds_out, calculator_filters)
                if ds_out is None:
                    names_to_remove.append(system_name)
                elif len(ds_out) == 0:
                    names_to_remove.append(system_name)
                else:
                    data_sets[system_name] = ds_out
            for system_name in names_to_remove:
                del data_sets[system_name]

            # Check that there are systems left
            if len(data_sets) == 0:
                raise ValueError(
                    'No systems left after filtering. Check'
                    + ' molecular geometries or try another filter.'
                )

            self.create_folder(f'{sub_dir_hyal}/iter_{iter_num}/mlp')

            # Check if loop finished, this is neccessary
            # if people change systems etc.
            if Path(f'{sub_dir_hyal}/iter_{iter_num}/FINISHED').exists():
                self.logger.debug(
                    f'Iteration {iter_num} was finished, restoring PRNG state'
                )
                # set state of random number generator
                with open(
                    f'{sub_dir_hyal}/iter_{iter_num}/hyal_settings_iter.json',
                    'r',
                ) as f:
                    hyal_results = json.load(f)
                    state = hyal_results['final_random_state']
                    models = hyal_results['final_models']
                rand_num_gen = self.generator_from_state(state)
                continue

            ###########################################
            # Train machine learning potential models #
            ###########################################
            test_split = al_opts.get('test_split', 0.0)
            bootstrap_split = al_opts.get('bootstrap_split', 0.0)

            if test_split > 0.0:
                # Hold out a test set that is not used during training
                datasets_train, datasets_test = self.split_datasets(
                    data_sets, test_split, rng=rand_num_gen, safe=True
                )
            else:
                datasets_train = data_sets
                datasets_test = {}

            datasets_train_bootstrap = []
            datasets_val_bootstrap = []
            if al_opts.get('bootstrap_split', 0) > 0.0:
                # Make a bootstrap split of the training set
                for ml_index, ml_engine in enumerate(self.ml_engines):
                    train, val = self.split_datasets(
                        datasets_train,
                        bootstrap_split / (1 - test_split),
                        rng=rand_num_gen,
                        safe=True,
                        minimal_size=1,
                    )
                    datasets_train_bootstrap.append(train)
                    datasets_val_bootstrap.append(val)
            else:
                datasets_train_bootstrap = [datasets_train] * len(
                    self.ml_engines
                )
                datasets_val_bootstrap = [] * len(self.ml_engines)

            run_opts = []
            data_dirs = []
            data_dirs_val = []
            self.logger.debug('Preparing data for training')
            for ml_index, ml_engine in enumerate(self.ml_engines):
                if bootstrap_split > 0.0:
                    sub_dir_data = (
                        f'{sub_dir_hyal}/iter_{iter_num}/mlp/model_{ml_index}/'
                    )
                else:
                    sub_dir_data = f'{sub_dir_hyal}/iter_{iter_num}/mlp/'
                trainingset = datasets_train_bootstrap[ml_index]
                data_dirs.append(
                    self.prep_for_training(
                        trainingset,
                        ml_engine,
                        sub_dir=sub_dir_data,  # type: ignore
                    )
                )
                if (
                    bootstrap_split > 0.0
                    and len(datasets_val_bootstrap[ml_index]) > 0
                ):
                    validationset = datasets_val_bootstrap[ml_index]
                    data_dirs_val.append(
                        self.prep_for_training(
                            validationset,
                            ml_engine,
                            sub_dir=sub_dir_data,  # type: ignore
                            name='validation',
                        )
                    )
                run_opts.append(
                    dict(
                        sub_dir=f'{sub_dir_hyal}/iter_{iter_num}'
                        + f'/mlp/model_{ml_index}'
                    )
                )

            self.logger.debug(f'Size of training set: {len(data_dirs)}')
            self.logger.debug(f'Size of validation set: {len(data_dirs_val)}')

            if al_opts.get('force_same_validation', False):
                self.logger.warning(
                    'Setting validation set to be the same as training set'
                )
                data_dirs_val = copy.deepcopy(data_dirs)

            # Make a json serializable seeds list
            seeds = [
                int(integer)
                for integer in rand_num_gen.integers(
                    0, 10000, size=len(self.ml_engines)
                )
            ]
            program_opts_train = []
            for i, ml_engine in enumerate(self.ml_engines):
                program_opts_train.append(dict(seed=seeds[i]))
                program_opts_train[-1].update(mlp_opts[i])

                if data_dirs_val:
                    program_opts_train[-1].update(  # type: ignore
                        {'validation_set': data_dirs_val[i]}  # type: ignore
                    )  # type: ignore

            self.logger.debug('Training machine learning potential models')
            train = self.train_parallel(
                data_dirs,
                self.ml_engines,
                program_opts=program_opts_train,
                run_opt=run_opts,
            )

            #########################
            # Sample new geometries #
            #########################
            self.logger.debug('Start sampling new geometries')
            models = []
            type_maps = []
            for j in range(len(train)):
                if 'is_converged' in train[j].keys():
                    if not all(train[j]['is_converged']):
                        raise ValueError(
                            f'Problems with convergence in model {j}'
                        )

                if 'model_compressed' in train[j].keys():
                    models.append(train[j]['model_compressed'])
                else:
                    models.append(train[j]['model'])

                try:
                    type_map = self.ml_engines[j].get_type_map(  # type: ignore
                        train[j]
                    )
                except AttributeError:
                    atoms = sum(
                        self.dict_of_dataset_to_dataset(
                            data_sets
                        ).data.atoms.values.tolist(),  # type: ignore
                        [],
                    )
                    type_map = sorted(set(np.unique(atoms)))
                type_maps.append(type_map)

            # Verify that all models have the same type map
            if not all([type_map == type_maps[0] for type_map in type_maps]):
                raise ValueError(
                    'All models must have the same type map. '
                    + 'Please check that the type map is the same for all '
                    + 'ML engines.'
                )
            else:
                type_map = type_maps[0]
                systems = self.set_type_map_systems(systems, type_map)

            ###########################
            # Recalibrate uncertainty #
            ###########################
            self.logger.debug('Recalibrating uncertainty')
            if al_opts['recalibrate_uncertainty']:
                if datasets_test:
                    datasets_nodict = self.dict_of_dataset_to_dataset(
                        datasets_test
                    )
                else:
                    datasets_nodict = self.dict_of_dataset_to_dataset(
                        data_sets
                    )
                datasets_nodict.change_units(self.units)
                datasets_nodict.data['method_id'] = ['reference'] * len(
                    datasets_nodict
                )
                possible_models_numbers = np.arange(2, len(models) + 1)
                ue_cal_report = self.make_uncertainty_report(
                    dataset=datasets_nodict,
                    models=models,
                    ml_engines=self.local_ml_engines,
                    run_opt=dict(
                        sub_dir=f'{sub_dir_hyal}/iter_{iter_num}'
                        + '/uncertainty_recalibration'
                    ),
                    save_all=True,
                    robust=True,
                    linear_correction={},
                    n_models_calibration=possible_models_numbers,
                )

                # recalibration using all models
                with open(ue_cal_report['correction_factors'], 'r') as f:
                    ue_calibrations = json.load(f)

                # recalibration n models, used for conf ref
                ue_calibrations_conf_ref = {}
                if al_opts.get('recalibrate_uncertainty_per_nmodels', False):
                    with open(
                        ue_cal_report['ue_corrections_per_model'], 'r'
                    ) as f:
                        ue_calibrations_conf_ref = json.load(f)
                else:
                    for n in possible_models_numbers:
                        ue_calibrations_conf_ref[f'{n}'] = ue_calibrations[
                            'models_vs_ref'
                        ]
            else:
                # Use default calibration
                ue_calibrations = {}
                ue_calibrations['models_vs_ref'] = {
                    'forces': [1, 0],
                    'energy': [1, 0],
                    'virial': [1, 0],
                }
                ue_calibrations['mean_vs_ref'] = {
                    'forces': [1, 0],
                    'energy': [1, 0],
                    'virial': [1, 0],
                }
                ue_calibrations_conf_ref = {}
                for n in np.arange(2, len(models) + 1):
                    ue_calibrations_conf_ref[f'{n}'] = deepcopy(
                        ue_calibrations
                    )

            #######################################
            # Set up sampling of different models #
            #######################################
            self.logger.debug('Setting up sampling of different models')
            prog_opts_sampler: List[dict] = []
            run_opts_sampler = []
            system_names_sampler = []

            model_idx = 0
            for j, system_name in enumerate(list(systems.keys())):
                sub_dir = (
                    f'{sub_dir_hyal}/iter_{iter_num}'
                    + f'/sampler/{system_name}'
                )
                try:
                    sampling_opts_system = sampling_opts[system_name]
                except KeyError:
                    sampling_opts_system = copy.deepcopy(sampling_opts)

                # Add options if supplied
                if 'options' in sampling_opts_system.keys():
                    if isinstance(sampling_opts_system['options'], list):
                        options = deepcopy(sampling_opts_system['options'])
                    elif isinstance(sampling_opts_system['options'], dict):
                        options = [deepcopy(sampling_opts_system['options'])]
                else:
                    options = [{}]

                # Add model to options
                for option in options:
                    # Allow for sampling multiple samples on same system
                    if 'num_repeated_sampling' in option.keys():
                        num_repeated_sampling = option.pop(
                            'num_repeated_sampling'
                        )
                        if isinstance(num_repeated_sampling, int):
                            pass
                        elif isinstance(num_repeated_sampling, str) and (
                            num_repeated_sampling == 'models'
                        ):
                            num_repeated_sampling = len(models)
                        else:
                            raise ValueError(
                                "num_repeated_sampling must be 'models'"
                                + ' or an integer'
                            )
                    else:
                        num_repeated_sampling = 1
                    for i in range(num_repeated_sampling):
                        prog_opts_sampler.append(deepcopy(option))
                        run_opts_sampler.append(dict(sub_dir=sub_dir))
                        model_idx = model_idx % len(models)

                        prog_opts_sampler[-1]['model'] = models[model_idx]
                        if (
                            self.ml_engines[model_idx].__class__.__name__
                            == 'NequIP'
                            and self.sampler.__class__.__name__ == 'Lammps'
                        ):
                            prog_opts_sampler[-1]['mlp_pair_style'] = (
                                prog_opts_sampler[-1].get(
                                    'mlp_pair_style', 'allegro'
                                )
                            )
                        if 'template' in sampling_opts_system.keys():
                            prog_opts_sampler[-1]['template'] = (
                                sampling_opts_system['template']
                            )
                        if 'cli_args' in sampling_opts_system.keys():
                            prog_opts_sampler[-1]['cli_args'] = (
                                sampling_opts_system['cli_args']
                            )
                        system_names_sampler.append(system_name)
                        model_idx += 1

            # Remove systems that have zero steps, and use geometry directly
            self.logger.debug('Removing systems with zero steps')
            samples = {}
            idx_to_remove = []
            for i, system_name in enumerate(system_names_sampler):
                # Check wheter to skip sampling
                random_sampling = {}
                if self.sampler is None:
                    random_sampling = {'num_samples': 1, 'source': 'systems'}
                random_sampling = prog_opts_sampler[i].get(
                    'random_sampling', random_sampling
                )

                if random_sampling:
                    num_random_samples = int(
                        random_sampling['num_samples']  # type: ignore
                    )
                    if random_sampling['source'] == 'systems':
                        dict_tmp = {}
                        dict_tmp['atoms'] = np.array(
                            systems[system_name].atoms
                        )
                        dict_tmp['coordinates'] = np.array(
                            systems[system_name].coordinates
                        ).reshape(1, -1)
                        if isinstance(systems[system_name], PeriodicSystem):
                            dict_tmp['box'] = np.array(
                                systems[system_name].box
                            ).reshape(-1, 9)
                        samples[system_name] = DataSet(
                            pd.DataFrame([dict_tmp]), units=self.units
                        )
                    elif random_sampling['source'] == 'geometry_pool':
                        if system_name in geometry_pool.keys():
                            replace = (
                                len(geometry_pool[system_name])
                                < num_random_samples
                            )
                            samples[system_name] = DataSet(
                                geometry_pool[system_name].data.sample(
                                    n=random_sampling['num_samples'],
                                    random_state=rand_num_gen,
                                    replace=replace,
                                ),
                                units=geometry_pool[system_name].units,
                            )
                        else:
                            raise ValueError(
                                'No geometry pool found for system '
                                + f'{system_name}.'
                            )
                    elif random_sampling['source'] == 'training_set':
                        replace = (
                            len(data_sets[system_name]) < num_random_samples
                        )
                        if system_name in data_sets.keys():
                            samples[system_name] = DataSet(
                                data_sets[system_name].data.sample(
                                    n=random_sampling['num_samples'],
                                    random_state=rand_num_gen,
                                    replace=replace,
                                )
                            )
                        else:
                            raise ValueError(
                                'No training set found for system '
                                + f'{system_name}.'
                            )
                    else:
                        raise ValueError(
                            'Unknown source for random sampling: '
                            + f'{random_sampling["source"]}.'
                        )
                    idx_to_remove.append(i)

            # remove elements belonging to idx_to_remove
            for i in sorted(idx_to_remove, reverse=True):
                del prog_opts_sampler[i]
                del run_opts_sampler[i]
                del system_names_sampler[i]

            # systems_sampler = {
            #     system_name: systems[system_name]
            #     for system_name in system_names_sampler
            # }
            systems_sampler = [
                systems[system_name] for system_name in system_names_sampler
            ]

            # Check if already done, if avoid_redo and al
            if (
                avoid_redo
                and Path(
                    f'{sub_dir_hyal}/iter_{iter_num}' + '/sampler/FINISHED'
                ).exists()
            ):
                self.logger.debug('Sampler already run, skipping')
            else:
                self.logger.debug('Running sampler')
                self.sampler_parallel(
                    systems_sampler, prog_opts_sampler, run_opts_sampler
                )
                Path(f'{sub_dir_hyal}/iter_{iter_num}/sampler/').mkdir(
                    parents=True, exist_ok=True
                )
                with open(
                    f'{sub_dir_hyal}/iter_{iter_num}/sampler/FINISHED', 'w'
                ) as f:
                    f.write('')

            for i, system_name in enumerate(system_names_sampler):
                samples[system_name] = self.generate_dataset(
                    f'{sub_dir_hyal}/iter_{iter_num}/sampler/{system_name}',
                    calculator=self.sampler,
                    parse_input=False,
                )
                # Check that atoms are present in all samples
                for system_name, ds in samples.items():
                    if 'types' in samples[system_name].data.columns:
                        # use type_map to map types to atom
                        # names and remove types
                        samples[system_name].data['atoms'] = (
                            samples[system_name]
                            .data['types']
                            .apply(
                                lambda indexes: [
                                    systems[system_name].properties[
                                        'type_map'
                                    ][
                                        i - 1
                                    ]  # -1 to convert to 0-indexing
                                    for i in indexes
                                ]
                            )
                        )
                        samples[system_name].data.drop(
                            'types', axis=1, inplace=True
                        )

                    # Check if periodic system
                    if (
                        not isinstance(systems[system_name], PeriodicSystem)
                        and 'box' in samples[system_name].data.columns
                    ):
                        # Remove box from samples
                        samples[system_name].data.drop(
                            'box', axis=1, inplace=True
                        )

                    samples[system_name].change_units(self.units)

            # Pick out a random subset of the samples
            self.logger.debug('Picking out a random subset of the samples')
            samples_subset = {}
            for system_name in list(samples.keys()):
                max_num_samples = al_opts['max_num_samples'][system_name]
                if len(samples[system_name]) > max_num_samples:
                    samples_subset[system_name] = DataSet(
                        samples[system_name].data.sample(
                            n=max_num_samples, random_state=rand_num_gen
                        )
                    )
                else:
                    samples_subset[system_name] = DataSet(
                        samples[system_name].data.sample(
                            n=max_num_samples,
                            random_state=rand_num_gen,
                            replace=True,
                        )
                    )

            samples_error_statistics = {}
            for system_name in list(samples.keys()):
                max_num_samples_error_statistics = al_opts[
                    'max_num_samples_error_statistics'
                ][system_name]
                if (
                    len(samples[system_name])
                    > max_num_samples_error_statistics
                ):
                    samples_error_statistics[system_name] = DataSet(
                        samples[system_name].data.sample(
                            n=max_num_samples_error_statistics,
                            random_state=rand_num_gen,
                        )
                    )
                else:
                    samples_error_statistics[system_name] = samples[
                        system_name
                    ]

            ##############################################################
            # Set up and run conf ref for a subset of sampled geometries #
            ##############################################################
            self.logger.debug('Setting up ConfRef')
            run_opts_conf = []
            prog_opts_conf = []
            all_samples = []
            all_models = []
            index_table = []
            for system_name, system_samples in samples_subset.items():
                model_combs = self.random_model_combs(
                    models,
                    num_possible_models_comb=al_opts['num_models_confref'][
                        system_name
                    ],
                    rng=rand_num_gen,
                    num_combinations=len(system_samples.data),
                )

                for i, model_comb in enumerate(model_combs):
                    config = system_samples.data.iloc[i].to_dict()
                    run_opts_conf.append(
                        dict(
                            sub_dir=f'{sub_dir_hyal}/iter_{iter_num}'
                            + f'/conf_ref/{system_name}'
                        )
                    )
                    if 'system_name' in confref_opts.keys():
                        confref_opts_system = copy.deepcopy(
                            confref_opts[system_name]
                        )
                        if al_opts['recalibrate_uncertainty']:
                            # add uncertainty calibration
                            confref_opts_system['uncertainty_calibration'] = (
                                ue_calibrations_conf_ref[f'{len(model_comb)}']
                            )
                    else:
                        confref_opts_system = copy.deepcopy(confref_opts)
                        if al_opts['recalibrate_uncertainty']:
                            # add uncertainty calibration
                            confref_opts_system['uncertainty_calibration'] = (
                                ue_calibrations_conf_ref[f'{len(model_comb)}']
                            )

                    prog_opts_conf.append(confref_opts_system)
                    all_samples.append(config)
                    all_models.append(model_comb)
                    index_table.append(system_name)

            # Run conf ref
            if (
                avoid_redo
                and Path(
                    f'{sub_dir_hyal}/iter_{iter_num}' + '/conf_ref/FINISHED'
                ).exists()
            ):
                self.logger.debug('ConfRef already run, skipping')
            else:
                self.logger.debug('Running ConfRef')
                self.conf_ref_parallel(
                    all_samples, all_models, prog_opts_conf, run_opts_conf
                )
                with open(
                    f'{sub_dir_hyal}/iter_{iter_num}/conf_ref/FINISHED', 'w'
                ) as f:
                    f.write('')

            # Use parser to parse results
            self.logger.debug('Parsing ConfRef results')
            confs = {}
            for system_name in list(samples.keys()):
                confs[system_name] = self.generate_dataset(
                    f'{sub_dir_hyal}/iter_{iter_num}/conf_ref/{system_name}',
                    calculator=self.config_generator_interface,
                    parse_input=False,
                )
                confs[system_name].change_units(self.units)

            # Check if any confs is empty
            for system_name in list(confs.keys()):
                if len(confs[system_name]) == 0:
                    del confs[system_name]

            # Check if any confs is empty
            if len(confs) == 0:
                raise ValueError('No configurations left after conf ref.')

            ###################################################
            # Filter out geometries based on error estimation #
            ###################################################
            self.logger.debug(
                'Filtering out geometries based on error estimation'
            )
            # Apply filters on ml predictions
            outliers = {}
            for system_name, ds in confs.items():
                ds_out = self.remove_nonconverged(ds)
                ds_out = self.apply_filters(ds, ml_filters)

                if ds_out is not None:
                    outliers[system_name] = ds_out
                    if (
                        len(outliers[system_name])
                        > al_opts['max_num_calcs'][system_name]
                    ):
                        outliers[system_name].data = outliers[
                            system_name
                        ].data.sample(  # type: ignore
                            n=al_opts['max_num_calcs'][system_name],
                            random_state=rand_num_gen,
                        )

            # Check if any outliers is empty
            if len(outliers) == 0:
                self.logger.debug(
                    'No outliers found, exiting active learning loop.'
                )
                return models

            ####################################
            # Calculate reference for outliers #
            ####################################
            self.logger.debug('Calculating reference for outliers')
            calc_systems = []
            run_opts_calc = []
            for system_name, ds in outliers.items():
                for idx, outlier in enumerate(ds.systems):
                    run_opts_calc.append(
                        dict(
                            sub_dir=f'{sub_dir_hyal}/iter_{iter_num}'
                            + f'/calculator/{system_name}'
                        )
                    )
                    calc_systems.append(outlier)

            if (
                avoid_redo
                and Path(
                    f'{sub_dir_hyal}/iter_{iter_num}' + '/calculator/FINISHED'
                ).exists()
            ):
                self.logger.debug('Calculator already run, skipping')
            else:
                self.logger.debug('Running calculator')
                self.calculator_parallel(calc_systems, run_opts=run_opts_calc)
                with open(
                    f'{sub_dir_hyal}/iter_{iter_num}/calculator/FINISHED', 'w'
                ) as f:
                    f.write('')

            ########################################
            # Estimate error on sampled geometries #
            ########################################
            self.logger.debug('Estimating error on sampled geometries')
            if al_opts['error_estimation']:
                samples_no_dict = self.dict_of_dataset_to_dataset(
                    samples_error_statistics
                )
                samples_no_dict.change_units(self.units)
                self.make_uncertainty_report(
                    dataset=samples_no_dict,
                    models=models,
                    run_opt=dict(
                        sub_dir=f'{sub_dir_hyal}/iter_{iter_num}'
                        + '/error_estimation'
                    ),
                    save_all=True,
                    linear_correction=ue_calibrations,
                    has_reference=False,
                    ml_engines=self.local_ml_engines,
                )
                if datasets_test:
                    datasets_nodict = self.dict_of_dataset_to_dataset(
                        datasets_test
                    )
                    # make sure reference is in the dataset
                    datasets_nodict.change_units(self.units)
                    datasets_nodict.data['method_id'] = ['reference'] * len(
                        datasets_nodict
                    )
                    self.make_uncertainty_report(
                        dataset=datasets_nodict,
                        models=models,
                        run_opt=dict(
                            sub_dir=f'{sub_dir_hyal}/iter_{iter_num}'
                            + '/test_error'
                        ),
                        save_all=True,
                        linear_correction=ue_calibrations,
                        has_reference=True,
                        ml_engines=self.local_ml_engines,
                    )

            self.logger.debug('Active learning loop iteration finished')
            with open(f'{sub_dir_hyal}/iter_{iter_num}/FINISHED', 'a') as f:
                f.write('Iteration finished')
            with open(
                f'{sub_dir_hyal}/iter_{iter_num}' + '/hyal_settings_iter.json',
                'w',
            ) as f:
                al_opts['final_random_state'] = self.get_rng_state(
                    rand_num_gen
                )
                al_opts['final_models'] = models
                json.dump(al_opts, f, indent=4)

        return models

    def prep_for_training(
        self,
        data: dict,
        ml_engine: HylleraasMLInterface,
        sub_dir='./',
        name='',
    ) -> Any:
        """Prepare data for training."""
        sub_dir = self.create_folder(
            f'{sub_dir}/' + f'dataset_{ml_engine.__class__.__name__}/'
        )
        data_dirs = []
        for system_name, ds in data.items():
            if name:
                folder_name = f'{sub_dir}/{name}_{system_name}'
            else:
                folder_name = f'{sub_dir}/{system_name}'
            try:
                data = ml_engine.get_data(ds)
            except AttributeError:
                return self.dict_of_dataset_to_dataset(data)
            try:
                data.dump_data(  # type: ignore
                    data_dir=folder_name, overwrite=True
                )
            except AttributeError:
                raise AttributeError(
                    'ML engine does not have dump_data method'
                )

            data_dirs.append(str(Path(folder_name).absolute()))
        return data_dirs

    @run_concurrent
    async def train_parallel(
        self, data_dirs: List, ml_engines: List, program_opts, *args, **kwargs
    ) -> dict:
        """Train model cofunction."""
        index = kwargs.pop('index', 0)
        result_train = await ml_engines[index].atrain(
            data_dirs, *args, **program_opts[index], **kwargs
        )
        return result_train

    def merge_dicts(self, d1: dict, d2: dict) -> dict:
        """Merge dictionaries."""
        d: dict = {}
        if d1 == {}:
            return d2
        for k, v1 in d1.items():
            v2 = d2[k]
            if isinstance(v1, list):
                v1.append(v2)
            elif isinstance(v1, np.ndarray):
                np.append(v1, np.array(v2))
            elif isinstance(v1, dict):
                v1.update(v2)
            else:
                v1 = [v1, v2]

            d[k] = v1
        return d

    @run_concurrent
    async def calculator_parallel(
        self,
        system: Union[Molecule, PeriodicSystem],
        program_opts: List = [],
        run_opts: List = [],
        **kwargs,
    ):
        """Use calculator in parallel."""
        index = kwargs.pop('index', 0)
        calc_kwargs = {}

        if run_opts:
            calc_kwargs['run_opt'] = run_opts[index]

        if program_opts:
            calc_kwargs['program_opt'] = program_opts[index]

        return await self.calculator.arun(  # type: ignore
            system, **calc_kwargs
        )

    @run_concurrent
    async def sampler_parallel(
        self,
        system: Union[Molecule, PeriodicSystem],
        program_opts,
        run_opts,
        *args,
        **kwargs,
    ):
        """Use sampler in parallel."""
        index = kwargs.pop('index', 0)

        # if run_sampler:
        run_opt = run_opts[index]
        program_opt = program_opts[index]
        kwargs_extra = {}
        if 'template' in program_opt.keys():
            kwargs_extra['template'] = program_opt.pop('template')
        if 'cli_args' in program_opt.keys():
            kwargs_extra['cli_args'] = program_opt.pop('cli_args')

        all_samples = await self.sampler.arun(  # type: ignore
            system,
            program_opt=program_opt,
            run_opt=run_opt,
            units=self.units,
            **kwargs_extra,
        )

        return all_samples

    @run_concurrent
    async def conf_ref_parallel(
        self,
        samples,  # List(Union[dict, Molecule, PeriodicSystem]
        models,  # : Union[str, Path]
        program_opts,
        run_opts,
        *args,
        **kwargs,
    ):
        """Generate configurations in parallel."""
        index = kwargs.pop('index', 0)
        models = models[index]
        program_opt = program_opts[index]
        run_opt = run_opts[index]
        confs = await self.arun_config_generator(
            models, samples, options=program_opt, run_opt=run_opt
        )
        return confs

    @run_concurrent
    async def infer_parallel(self, program_opts, run_opts, *args, **kwargs):
        """Use infer for ml engines in parallel."""
        index = kwargs.pop('index', 0)
        ml_engine = program_opts.pop('ml_engine')
        results = await ml_engine.ainfer(
            **program_opts, run_opt=run_opts[index]
        )
        return results

    def infer_parallel_old(
        self, program_opts: list[dict], run_opts: list[dict], **kwargs
    ):
        """Use infer for ml engines in parallel."""
        results = []
        ml_engines = [
            program_opt.pop('ml_engine') for program_opt in program_opts
        ]
        results = [
            ml_engine.infer(**program_opt, run_opt=run_opt)
            for ml_engine, program_opt, run_opt in zip(
                ml_engines, program_opts, run_opts
            )
        ]
        return results

    @exception_handler(KeyError)
    def make_initial_set(
        self, systems: dict, num_initial=1, dev=0.1, **kwargs
    ):
        """Make an initial training set from systems."""
        self.logger.debug('Making initial training set')
        # Create an initial set from the systems provided
        # to avoid recalculating the same initial set

        sub_dir = kwargs.pop('sub_dir', './')

        generator = kwargs.pop('generator', np.random.default_rng())
        system_sub_dirs = {}
        for system_name in systems.keys():
            system_sub_dirs[system_name] = (
                f'{sub_dir}/initial_set/{system_name}'
            )

        run_opts_calc = []
        for system_name in systems.keys():
            run_opts_calc += [
                dict(sub_dir=system_sub_dirs[system_name])
            ] * num_initial

        systems_tmp = []
        index = 0
        for system_name, system in systems.items():
            systems_tmp += [system]
            index += 1
            if num_initial > 1:
                for j in range(num_initial - 1):
                    systems_tmp += [deepcopy(system)]
                    coordinates = system.coordinates
                    coordinates += (
                        generator.normal(0, dev, size=(len(system.atoms), 3))
                        / 0.529177249
                    )
                    systems_tmp[index].coordinates = coordinates
                    index += 1
        self.calculator_parallel(systems_tmp, run_opts=run_opts_calc)
        data_sets = {}
        for key in system_sub_dirs.keys():
            data_sets[key] = self.generate_dataset(system_sub_dirs[key])
        return data_sets

    @exception_handler(KeyError)
    def apply_filters(
        self, dataset: DataSet, filters: dict, deviations: dict = {}
    ) -> DataSet:
        """Apply filters to dataset."""
        self.logger.debug('Applying filters to dataset')
        # columns_before = dataset.data.columns
        dataset_out = deepcopy(dataset)
        keep_values = np.ones(len(dataset), dtype=bool)
        self.logger.debug(f'Dataset size before filtering: {len(dataset)}')
        for filter in filters.values():
            keep_values *= filter(dataset, deviations=deviations)
        if sum(keep_values) == 0:
            self.logger.warning('Empty dataset after filtering')
            return None
        dataset_out.data = deepcopy(dataset_out.data[keep_values]).reset_index(
            drop=True
        )
        self.logger.debug(f'Dataset size after filtering: {len(dataset_out)}')
        # Remove columns that were not present in the input dataset
        # dataset_out.data = dataset_out.data[columns_before]

        return dataset_out

    def set_type_map_system(
        self, system: Union[Molecule, PeriodicSystem], type_map: List[str]
    ):
        """Set type map."""
        system.properties['type_map'] = type_map
        return system

    def set_type_map_systems(self, systems: Dict, type_map: List[str]):
        """Set type map."""
        for key in systems.keys():
            systems[key] = self.set_type_map_system(systems[key], type_map)
        return systems

    @exception_handler(ValueError)
    @exception_handler(KeyError)
    def parse_options(
        self, options: Any, systems: dict = {}, maxiter=None
    ) -> List[List[Dict]]:
        """Parse active learning options."""
        self.logger.debug('Parsing active learning options')
        if isinstance(options, list):
            # Already set up the options for all iterations
            pass
        elif isinstance(options, Dict):
            if maxiter is None:
                raise ValueError('maxiter must be set if options is a dict')
            # Set up the options for all iterations
            options = [options for i in range(maxiter)]
        else:
            raise ValueError(
                'options should be a dictionary or a list of dictionaries'
            )
        options = [deepcopy(option) for option in options]
        al_opts = []
        sampling_opts = []
        confref_opts = []
        mlp_opts = []

        for option in options:
            al_opt = {
                'num_samples': 10,
                'max_iter': 10,
                'seed': 0,
                'num_initial': 1,
                'initial_displacement': 0.1,
                'recalibrate_uncertainty': False,
                'recalibrate_uncertainty_per_nmodels': False,
                'error_estimation': False,
                'force_same_validation': False,
                'calculator_filters': {},
                'ml_filters': {
                    'max_force': {
                        'key': 'forces',
                        'upper': True,
                        'operation': 'max',
                        'limit': 1.0,
                        'shape': [-1, 3],
                    }
                },
            }
            al_opt.update(option.pop('al_opts', {}))

            al_opt.setdefault('num_models_confref', [len(self.ml_engines)])
            al_opt.setdefault('max_num_samples', 10)
            al_opt.setdefault('max_num_samples_error_statistics', 100)
            al_opt.setdefault('max_num_calcs', 10)

            al_opt_parsed = copy.deepcopy(al_opt)
            if not isinstance(al_opt_parsed['num_models_confref'], dict):
                al_opt_parsed['num_models_confref'] = {
                    key: al_opt_parsed['num_models_confref']
                    for key in systems.keys()
                }

            if not isinstance(al_opt_parsed['max_num_samples'], dict):
                al_opt_parsed['max_num_samples'] = {
                    key: al_opt_parsed['max_num_samples']
                    for key in systems.keys()
                }

            if not isinstance(
                al_opt_parsed['max_num_samples_error_statistics'], dict
            ):
                al_opt_parsed['max_num_samples_error_statistics'] = {
                    key: al_opt_parsed['max_num_samples_error_statistics']
                    for key in systems.keys()
                }

            if not isinstance(al_opt_parsed['max_num_calcs'], dict):
                al_opt_parsed['max_num_calcs'] = {
                    key: al_opt_parsed['max_num_calcs']
                    for key in systems.keys()
                }

            # options specifying sampling MD algorithm used for sampling
            sampling_opt = option.pop('sampling_opts', {})
            sampling_opts.append(sampling_opt)

            # options specifying model configurational refinement
            confref_opt = option.pop('confref_opts', {})
            confref_opts.append(confref_opt)

            # options specifying MLP training
            mlp_opt = option.pop('mlp_opts', {})
            if not isinstance(mlp_opt, list):
                mlp_opt = [mlp_opt] * len(self.ml_engines)
            assert len(mlp_opt) == len(self.ml_engines)

            mlp_opts.append(mlp_opt)
            al_opts.append(al_opt_parsed)
        return [al_opts, sampling_opts, confref_opts, mlp_opts]

    def set_filter(
        self, filter_name: str, filter_properties: Union[dict, Callable]
    ):
        """Set filter."""
        if isinstance(filter_properties, dict):
            key_val = filter_properties['key']
            upper_val = filter_properties.get('upper', True)
            limit_val = filter_properties['limit']
            reshape_val = filter_properties.get('shape', [-1])
            operation_val = filter_properties.get('operation', 'max')

            def filter_callable(
                dataset,
                filter_key=key_val,
                upper=upper_val,
                limit=limit_val,
                operation=operation_val,
                reshape=reshape_val,
                deviations={},
            ):
                return self.dataset_filter(
                    dataset,
                    filter_key,
                    upper,
                    limit,
                    operation,
                    reshape,
                    deviations,
                )

        else:
            filter_callable = copy.deepcopy(filter_properties)
        return filter_callable

    def get_rng_state(self, generator):
        """Get random number generator state."""

        def serialize(state_piece):
            if isinstance(state_piece, dict):
                return {
                    key: serialize(val) for key, val in state_piece.items()
                }
            elif isinstance(state_piece, np.ndarray):
                return state_piece.tolist()
            elif isinstance(state_piece, list):
                return [serialize(item) for item in state_piece]
            else:
                return state_piece

        # Extract the generator's current state
        state = generator.bit_generator.state
        # Serialize the state using the recursive helper function
        serialized_state = serialize(state)
        return serialized_state

    def generator_from_state(
        self, serialized_state=None, bit_generator_type=np.random.MT19937
    ):
        """Create generator from state."""
        if serialized_state is not None:
            deserialized_state = {
                k: np.array(v, dtype='uint32') if isinstance(v, list) else v
                for k, v in serialized_state.items()
            }

            # Instantiate the MT19937 BitGenerator with the restored state
            bit_generator = bit_generator_type()
            bit_generator.state = (
                deserialized_state  # Restore the internal state
            )
            restored_generator = np.random.Generator(bit_generator)
        else:
            # If no state is provided, create a new generator instance
            # (this generator is 'seeded' by default with entropy from the OS)
            restored_generator = np.random.default_rng()

        return restored_generator

    def random_model_combs(
        self,
        models,
        rng,  #: np.random.Generator = np.random.default_rng(),
        num_possible_models_comb: List[int] = [2, 3],
        num_combinations: int = 5,
    ) -> List[List[str]]:
        """Generate random model combinations."""
        self.logger.debug('Generating random model combinations')
        seen = set()
        result: List[List[str]] = []

        while len(result) < num_combinations:
            # Randomly choose the size of the next combination
            size = rng.choice(num_possible_models_comb)

            combination = tuple(
                sorted(rng.choice(models, size, replace=False))
            )

            # Add the combination to the result if it's new
            if combination not in seen:
                seen.add(combination)
                result.append(list(combination))
                if len(seen) == self.count_possible_combinations(
                    models, num_possible_models_comb
                ):
                    seen = set()
                    continue
        # turn into list of lists
        result = [list(comb) for comb in result]  # type: ignore
        return result  # type: ignore

    def count_possible_combinations(
        self, models: List[str], num_possible_models_comb: List[int]
    ) -> int:
        """Count the total number model combinations."""
        total_combinations = sum(
            len(list(itertools.combinations(models, size)))
            for size in num_possible_models_comb
        )
        return total_combinations

    @exception_handler(ValueError)
    def correct_units(self, data_sets: dict[str, DataSet]):
        """Change units of all datasets to hyal units."""
        self.logger.debug('Changing units of datasets to hyal units')
        for dataset_name, dataset in data_sets.items():
            # Verify that it has units
            if not dataset.has_units:
                raise ValueError(
                    f'{dataset_name} does not have units, \
                                  while active learning requires \
                                 them to be defined.'
                )
            else:
                # Change to units of hyal
                data_sets[dataset_name].change_units(self.units)
        return data_sets

    @exception_handler(KeyError)
    def dataset_filter(
        self,
        dataset: DataSet,
        filter_key: str,
        upper: bool,
        limit: float,
        operation: str = 'max',
        reshape: list = [],
        deviations: dict = {},
    ) -> list[bool]:
        """Filter dataset."""
        if filter_key == 'distances':
            if operation != 'min':
                raise ValueError(
                    'Operation must be min when filtering on distances.'
                )
            dataset = self.compute_distances(
                dataset, limit, force_recompute=False, minimum_distance=1.0e6
            )

        if not deviations:
            dataset_keys = list(dataset.data.keys())
            if filter_key not in dataset_keys:
                raise ValueError(
                    f'Key {filter_key} not in dataset. Available '
                    f'keys are {dataset_keys}.'
                )
            values = dataset.data[filter_key]
            values = values.apply(np.reshape, newshape=reshape)
            for i in range(len(values[0].shape) - 1):
                values = values.apply(np.linalg.norm, axis=-1)
            if operation == 'max':
                values = values.apply(np.max, axis=-1)
            elif operation == 'min':
                values = values.apply(np.min, axis=-1)
            elif operation == 'mean':
                values = values.apply(np.mean, axis=-1)
            else:
                raise KeyError(f'Operation {operation} not supported. ')
        else:
            values = np.array(deviations[filter_key])
        keep_vals = np.atleast_1d((values < limit))
        if not upper:
            keep_vals = np.invert(keep_vals)

        return keep_vals

    @exception_handler(ValueError)
    def make_uncertainty_report(
        self,
        dataset: DataSet,
        chosen_columns: list = ['forces', 'energy', 'virial'],
        ml_engines: list = [],
        models=[],
        run_opt={},
        reference_method_id: str = 'reference',
        save_all=False,
        linear_correction={},
        has_reference=True,
        **kwargs,
    ):
        """Make uncertainty report."""
        self.logger.debug('Making uncertainty report')
        if 'sub_dir' in run_opt.keys():
            sub_dir = run_opt['sub_dir']
        else:
            sub_dir = './'

        sub_dir = self.create_folder(sub_dir)
        if 'method_id' in dataset.data.columns:
            # Check that it is unique
            if len(dataset.data['method_id'].unique()) > 1:
                raise ValueError(
                    'method_id is not unique for reference dataset'
                )
            else:
                ref_method_id = dataset.data['method_id'].unique()[0]
        else:
            ref_method_id = reference_method_id
        dataset.data['method_id'] = [ref_method_id] * len(dataset.data)

        # Check if contains geometry_id, if not add 0-n
        if 'geometry_id' not in dataset.data.columns:
            dataset.data['geometry_id'] = np.arange(len(dataset.data))

        # Make sure the columns have standard naming
        dataset.standardize_columns()

        dataset.change_units(self.units)
        # Get predictions on reference dataset
        # Check the hylleraas interface, and check if is unique
        ml_interfaces = [
            ml_engine.__class__.__name__ for ml_engine in ml_engines
        ]
        if len(set(ml_interfaces)) == 1:
            predictions_dataset = DataSet(
                ml_engines[0].infer_external(
                    models,
                    dataset,
                    program=self.python_name,
                    cleanup=False,
                    full_output=True,
                    run_opt=dict(sub_dir=sub_dir),
                ),
                units=self.units,
            )
        else:
            raise ValueError('Not implemented yet.')

        # Create uncertainty_estimation class
        ue = UncertaintyEstimation()
        if has_reference:
            full_dataset = DataSet(
                pd.concat([dataset.data, predictions_dataset.data], axis=0),
                units=dataset.units,
            )
        else:
            full_dataset = DataSet(
                predictions_dataset.data, units=dataset.units
            )

        return ue.make_deviation_report(
            full_dataset,
            sub_dir=sub_dir,
            chosen_columns=chosen_columns,
            save_all=save_all,
            linear_correction=linear_correction,
            **kwargs,
        )

    def dict_of_dataset_to_dataset(self, dict_rep: dict) -> DataSet:
        """Convert dictionarof dataset to a single dataset."""
        dataset_out = pd.DataFrame()
        for key, value in dict_rep.items():
            df = value.data
            df['dataset_id'] = [key] * len(df)
            dataset_out = pd.concat([dataset_out, df], axis=0)

        dataset_out = dataset_out.reset_index(drop=True)
        if 'geometry_id' not in dataset_out.columns:
            dataset_out['geometry_id'] = np.arange(len(dataset_out))
        # make dataset_id a category
        dataset_out['dataset_id'] = dataset_out['dataset_id'].astype(
            'category'
        )
        return DataSet(dataset_out)

    def dataset_to_dict_of_dataset(self, dataset: DataSet) -> dict:
        """Convert a dataset to a dictionary of datasets."""
        dict_rep = {}  # type: dict
        if dataset is None:
            return dict_rep
        for key in dataset.data['dataset_id'].unique():
            dict_rep[key] = DataSet(
                dataset.data[dataset.data['dataset_id'] == key],
                units=dataset.units,
            )
        # Remove the dataset_id column
        for key, value in dict_rep.items():
            dict_rep[key].data = dict_rep[key].data.drop(
                columns=['dataset_id']
            )
        return dict_rep

    @exception_handler(ValueError)
    def split_dataset(
        self,
        dataset: DataSet,
        split_ratio,
        rng: np.random.Generator,
        safe=False,
    ) -> Tuple[DataSet, DataSet]:
        """Split a dataset into two datasets."""
        if len(dataset) == 0:
            raise ValueError('Dataset is empty')

        num_train = int(np.round(len(dataset.data) * (1 - split_ratio)))

        # If safe and num_train is 0, set it to 1 to avoid empty training set
        if safe and num_train == 0:
            num_train = 1
        num_test = len(dataset) - num_train

        if num_train > 0:
            train_set = dataset.data.sample(n=num_train, random_state=rng)
        if num_test > 0:
            if num_train == 0:
                test_set = dataset.data
            else:
                test_set = dataset.data.drop(train_set.index)

        if num_train == 0:
            train_set = None  # DataSet(pd.DataFrame())
        else:
            train_set.reset_index(drop=True, inplace=True)
            train_set = DataSet(train_set, units=dataset.units)

        if num_test == 0:
            test_set = None  # DataSet(pd.DataFrame())
        else:
            test_set.reset_index(drop=True, inplace=True)
            test_set = DataSet(test_set, units=dataset.units)

        return train_set, test_set

    def split_datasets(
        self,
        datasets: dict,
        split_ratio: float = 0.8,
        rng=np.random.Generator(np.random.MT19937()),
        safe=False,
        minimal_size=0,
    ) -> Tuple[dict, dict]:
        """Split a dictionary of datasets into two dictionaries."""
        datasets_nodict = self.dict_of_dataset_to_dataset(datasets)

        train, test = self.split_dataset(
            datasets_nodict, split_ratio, rng=rng, safe=safe
        )
        if test is None and minimal_size > 0:
            # make sure it has minimal size
            # then take out a random sample of the training set
            if len(train) > minimal_size:
                test = train.data.sample(n=minimal_size, random_state=rng)
                train.data = train.data.drop(test.index)
                test = DataSet(test, units=train.units)
                train.data = train.data.sample(
                    frac=1, random_state=rng
                ).reset_index(drop=True)

        train = self.dataset_to_dict_of_dataset(train)

        if test is not None:
            test = self.dataset_to_dict_of_dataset(test)
        else:
            test = {}
        return train, test

    @exception_handler(ValueError)
    @exception_handler(KeyError)
    def correct_dataset(self, dataset: DataSet, correction_dict):
        """Correct dataset."""
        # Make a copy
        dataset = deepcopy(dataset)
        for key, value in correction_dict.items():
            # Verify that the correction is applied to a column in the dataset
            if key in dataset.data.columns:
                # if a dictionary, assume denoting atoms, and
                if isinstance(value, dict):
                    for atom, correction in value.items():
                        # add correction times number of atoms matching atom
                        dataset.data[key] += (
                            dataset.data['atoms'].apply(
                                lambda atoms: atoms.count(atom)
                            )
                            * correction
                        )
                else:
                    dataset.data[key] += value
            else:
                raise ValueError(
                    f'Key {key} not in dataset. Available'
                    f'keys are {dataset.data.columns}.'
                )

    def compute_distances(
        self,
        dataset: DataSet,
        cutoff: float = 5.0,
        force_recompute=True,
        minimum_distance=None,
    ) -> DataSet:
        """Compute distances."""
        calculator = NeighborList(cutoff=cutoff, full_list=True)
        dataset = deepcopy(dataset)
        dataset.standardize_columns()
        # verify that distances are in the dataset
        if 'distances' not in dataset.data.columns or force_recompute:
            dataset.data['distances'] = [None] * len(dataset.data)

        # Find indices where distances are None or NaN
        missing_distances = dataset.data['distances'].isna()

        if missing_distances.any():
            for index, row in dataset.data.iterrows():
                if missing_distances[index]:
                    kwargs = {}
                    if 'box' in row.keys():
                        # check if 9 elements or 3x3
                        if len(row['box']) == 9 or len(row['box']) == 3:
                            kwargs['box'] = np.array(row['box']).reshape(3, 3)
                            kwargs['periodic'] = True
                        else:
                            kwargs['periodic'] = False

                    d = calculator.compute(
                        points=np.array(row['coordinates']).reshape(-1, 3),
                        **kwargs,
                        quantities='d',
                    )
                    d = d[0]
                    # if no distances, add minimum_distance
                    if len(d) == 0 and minimum_distance is not None:
                        d = np.array([minimum_distance])
                    dataset.data.at[index, 'distances'] = d
        return dataset
