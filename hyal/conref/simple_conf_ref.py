import copy
import inspect
import json
import pickle
from collections import namedtuple
from pathlib import Path
from typing import Any, Callable, Dict, List, NamedTuple, Optional, Union

import hyif
import numpy as np
import pandas as pd
from hyobj import DataSet, Molecule, PeriodicSystem, Units
from hyset import create_compute_settings
from scipy.optimize import minimize


class ConfRefOutput:
    """ConfRef input class."""

    @classmethod
    def parse(cls, inpath):
        """Parse conf_ref input."""
        # read json file
        results = {}
        try:
            with open(inpath, 'r') as f:
                results.update(json.load(f))

            # Remove spurious dimension for single entry
            for key, value in results.items():
                if isinstance(value, list):
                    if len(value) == 1:
                        results[key] = value[0]
        except FileNotFoundError:
            pass

        # Check if stdout file exists, make sure is relative path is conserved
        stdout_file = Path('.'.join(str(inpath).split('.')[:-1]) + '.out')
        try:
            with open(stdout_file, 'r') as f:
                lines = f.readlines()
        except FileNotFoundError:
            lines = []

        if any('GEOMETRY OPTIMIZATION COMPLETED' in line for line in lines):
            results['is_converged'] = True
        else:
            results['is_converged'] = False

        for i, line in enumerate(lines):
            if line.strip() == 'minimizer_nit':
                results['numb_iter'] = int(lines[i + 1])

        # Check if xyz file exists
        xyz_file = Path('.'.join(str(inpath).split('.')[:-1]) + '.xyz')
        if xyz_file.exists():
            results['trajectory_file'] = xyz_file
        return results


class ConfRefInput:
    """ConfRef input class."""

    @classmethod
    def parse(cls, inpath):
        """Parse conf_ref input."""
        # read json file
        with open(inpath, 'r') as f:
            result = json.load(f)

        # Remove spurious dimension for single entry
        for key, value in result.items():
            if isinstance(value, list):
                if len(value) == 1:
                    result[key] = value[0]

        stem = Path(inpath).stem
        options = f'{stem}.options'
        try:
            with open(options, 'r') as f:
                result.update(json.load(f))
        except FileNotFoundError:
            pass

        return result


class ConRef:
    """Configurational refinement class."""

    def __init__(
        self,
        models: Optional[List[str]] = [],
        ml_engines: Optional[List[Callable[..., Any]]] = None,
        **kwargs: Any,
    ):
        # input and output suffix for parser
        self.input_file_suffix = '.input'
        self.output_file_suffix = '.output'

        self.InputParser = ConfRefInput  # type: ignore
        self.OutputParser = ConfRefOutput  # type: ignore
        self.units = kwargs.get('units', Units('metal'))

        if ml_engines is None:
            ml_engines = []
            for model in models:
                if str(model).endswith('.pb'):
                    myenv_deepmd = create_compute_settings()
                    mydeepmd = hyif.DeepMD(
                        {'check_version': False}, compute_settings=myenv_deepmd
                    )
                    ml_engines.append(mydeepmd)
                elif str(model).endswith('.pth'):
                    myenv_nequip = create_compute_settings()
                    mynequip = hyif.NequIP(
                        {'input': {}, 'check_version': False},
                        compute_settings=myenv_nequip,
                    )
                    ml_engines.append(mynequip)

                else:
                    raise ValueError(
                        'ML model are incorrectly specified, must be .pb file.'
                    )
        self.models_signature = ' '.join(models)

        # Check if model should be preloaded
        if kwargs.get('run_external', True):
            for i, (model, ml_engine) in enumerate(zip(models, ml_engines)):
                # infer the function to preload model
                try:
                    # check if model has a preload_model function
                    sig = inspect.signature(
                        ml_engine.preload_model  # type: ignore
                    )
                    parameters = sig.parameters

                    # verify if device is needed
                    kwargs = {}
                    if 'device' in parameters:
                        kwargs['device'] = 'cuda'
                    elif 'kwargs' in parameters:
                        kwargs['device'] = 'cuda'

                    model = ml_engine.preload_model(  # type: ignore
                        model, **kwargs
                    )
                    models[i] = model

                # pass if no preload_model function
                except AttributeError:
                    pass

        self.models_infere_fn = []
        for model, ml_engine in zip(models, ml_engines):

            def infer_fn(data, model=model, ml_engine=ml_engine):
                return ml_engine.infer(data=data, model=model, **kwargs)

            self.models_infere_fn.append(infer_fn)

    def gen_configs(
        self,
        input_geometries: Any,
        options_in: Union[dict, str] = {},
        **kwargs,
    ) -> DataSet:
        """Generate adversial geometries.

        Arguments
        ---------
        input_geometries (Union[dict, str]): The input geometries.
        options (Union[dict, str]):
        The options to be read. It can be either a dictionary or a file path.
        kwargs (Any): Additional keyword arguments.

        Returns
        -------
        DataSet: The generated adversial geometries.

        """
        # Read input geometries
        configs_in = self.parse_input(input_geometries)

        # Check for PBC, if not, add a large box
        if 'box' not in configs_in.data.keys():
            configs_in.data['box'] = [
                [50.0, 0, 0, 0, 50.0, 0, 0, 0, 50.0]
            ] * len(configs_in.data['coordinates'])
            pbc = False
        else:
            pbc = True

        # Set options
        if isinstance(options_in, dict):
            parsed_options_filename = 'parsed.options'
        else:
            parsed_options_filename = (
                '.'.join(options_in.split('.')[:-1]) + '_parsed.options'
            )

        options_in = self.set_options(options_in, pbc=pbc)
        with open(parsed_options_filename, 'w') as fp:
            json.dump(options_in, fp, indent=4)

        options = namedtuple('options', options_in.keys())(
            *options_in.values()
        )

        # Open xyz file for writing geometries
        if kwargs.get('xyz_file', False):
            xyz_file = str(kwargs.get('xyz_file'))
            fp_xyz = open(xyz_file, 'w')
        else:
            fp_xyz = open('conf_ref.xyz', 'w')

        # Prepare results
        # configs_out = copy.deepcopy(configs_in)
        configs_out = pd.DataFrame()

        # Add empty columns for the new configurations

        # Loop over configurations
        for conf_idx in range(len(configs_in)):
            config_dict = {}
            config_dict['coordinates'] = configs_in.data['coordinates'][
                conf_idx
            ]
            config_dict['box'] = configs_in.data['box'][conf_idx]
            config_dict['atoms'] = configs_in.data['atoms'][conf_idx]
            # Create a mapping from x to variables
            mapping_x_to_dict = self.create_mapping(
                config_dict, options.opt_vars  # type: ignore
            )

            config_const = copy.deepcopy(
                self.toggle_scaled_coordinates(config_dict)
            )

            # Create a function that can be used to prepare the gradient
            def prep_grad(
                x,
                config_const=config_const,
                mapping_x_to_dict=mapping_x_to_dict,
                options=options,
                **kwargs,
            ):
                return self.prep_grad(
                    x, config_const, mapping_x_to_dict, options, **kwargs
                )

            # Create a function that can be used to calculate the objective
            # function and gradient
            def objective_and_gradient(x):
                return self.objective_and_gradient(
                    x, prep_grad, mapping_x_to_dict, fp_xyz, options
                )

            def obj_grad(x):
                return objective_and_gradient(x)[0:2]

            # Set initial guess using mapping
            x0 = np.zeros(len(mapping_x_to_dict))
            for x_index, mapping in enumerate(mapping_x_to_dict):
                x0[x_index] = config_const[mapping[0]][mapping[1]]
                x0[x_index] = self.rescale_to_x(
                    x0[x_index], mapping[-2], mapping[-1]
                )
            # config_dict = self.toggle_scaled_coordinates(config_dict)

            bounds = []
            for x_index, mapping in enumerate(mapping_x_to_dict):
                key = mapping[0]
                if key == 'box':
                    if mapping[1] in [3, 6, 7]:
                        bounds.append((None, None))
                    else:
                        bounds.append(
                            (
                                self.rescale_to_x(0, mapping[-2], mapping[-1]),
                                None,
                            )
                        )
                else:
                    bounds.append((None, None))

            # Do geometry optimization
            try:

                minimize_result = minimize(
                    obj_grad,
                    x0,
                    jac=True,
                    bounds=bounds,
                    options={
                        'maxiter': options.num_iter,  # type: ignore
                        'ftol': 5e-9,
                        'gtol': 1e-6,
                        'disp': True,
                    },
                    method=options.minimizer,  # type: ignore
                )

                x = minimize_result['x']
                print('-' * 80)
                print('GEOMETRY OPTIMIZATION COMPLETED')
                print('-' * 80)
                print('minimizer_success:\n', minimize_result['success'])
                print('-' * 80)
                print('minimizer_status:\n', minimize_result['status'])
                print('-' * 80)
                print('minimizer_message:\n', minimize_result['message'])
                print('-' * 80)
                print('minimizer_nit:\n', minimize_result['nit'])
            except Exception:
                raise ValueError('Geometry optimization failed.')

            # Extract results from geometry optimization
            for x_index, mapping in enumerate(mapping_x_to_dict):
                config_dict[mapping[0]][mapping[1]] = x[x_index]
                config_dict[mapping[0]][mapping[1]] = self.rescale_from_x(
                    x[x_index], mapping[-2], mapping[-1]
                )
            config_dict = self.toggle_scaled_coordinates(config_dict)

            # Write final geometry to xyz file
            fitness_values, grad, data_to_write = objective_and_gradient(x)

            self.write_xyz_file(
                fp_xyz, data_to_write, options.xyz_settings  # type: ignore
            )

            tmp_dict = {}
            for key, value in data_to_write.items():
                if isinstance(value, np.ndarray):
                    tmp_dict[key] = value.flatten()
                else:
                    tmp_dict[key] = value

            configs_out = pd.concat(
                [configs_out, pd.DataFrame([tmp_dict])], axis=0
            )

        configs_out = DataSet(configs_out, units=self.units)
        configs_out.standardize_columns()

        fp_xyz.close()
        return configs_out

    def parse_input(self, input):
        """Parse input molecules and turn into a dataset."""
        coord_keys = [
            'coord',
            'coordinates',
            'coordinateses',
            'trajectory',
            'trajectories',
        ]
        box_keys = ['box', 'boxes', 'cell', 'cells']
        atoms_keys = ['atoms', 'atom', 'atomss', 'name', 'species']

        if isinstance(input, dict):
            # Find matching key in molecules.keys()
            try:
                coord_key = [k for k in coord_keys if k in input.keys()][0]
                atoms_key = [k for k in atoms_keys if k in input.keys()][0]
            except IndexError:
                raise ValueError(
                    'Could not find coordinates or atoms in molecules dict.'
                )

            atoms = np.array(input[atoms_key])
            coord = np.array(input[coord_key])

            # Determine traj_len and natoms
            if len(atoms.shape) == 2:
                traj_len = atoms.shape[0]
                natoms = atoms.shape[1]
            elif len(atoms.shape) == 1:
                natoms = atoms.shape[0]
                traj_len = 1
            else:
                raise ValueError(
                    'Could not determine number of atoms or trajectory length.'
                )

            traj_len = int(coord.size // natoms // 3)

            coord = coord.reshape((traj_len, natoms * 3))
            if atoms.size == natoms:
                atoms = np.array([atoms] * traj_len)
            atoms = atoms.reshape((traj_len, natoms))

            cdict = {'coordinates': coord, 'atoms': atoms}

            # Check for PBC box
            try:
                box_key = [k for k in box_keys if k in input.keys()][0]
                box = np.array(input[box_key])
                box = box.reshape((traj_len, 9))
                cdict['box'] = box
            except IndexError:
                pass

            return DataSet(cdict, dimension='multi')

        elif isinstance(input, DataSet):
            # Key translation
            coord_key = [k for k in coord_keys if k in input.data.keys()][0]
            atoms_key = [k for k in atoms_keys if k in input.data.keys()][0]

            input_pd = input.data()
            if coord_key != 'coordinates':
                input_pd['coordinates'] = input_pd.pop(coord_key)
            if atoms_key != 'atoms':
                input_pd['atoms'] = input_pd.pop(atoms_key)

            # Check if box is in input for PBC
            try:
                box_key = [k for k in box_keys if k in input.data.keys()][0]
                if box_key != 'box':
                    input_pd['box'] = input_pd.pop(box_key)
            except IndexError:
                pass

            return DataSet(input_pd, dimension='multi')
        elif isinstance(input, Molecule):
            # convert to dataset
            return DataSet(
                {'coordinates': input.coord, 'atoms': input.atoms},
                dimension='multi',
            )
        elif isinstance(input, PeriodicSystem):
            return DataSet(
                {
                    'coordinates': input.coord,
                    'atoms': input.atoms,
                    'box': input.box,
                },
                dimension='multi',
            )

        elif isinstance(input, str):
            # load as json
            with open(input, 'r') as fp:
                input = json.load(fp)

            return DataSet(input, dimension='multi')
        else:
            # raise not implemented error
            # TODO: implement this
            raise NotImplementedError(
                'Input type not implemented. Please use a dict or a DataSet.'
            )

    def create_mapping(self, config: dict, opt_vars: dict) -> List[list]:
        """Create a mapping from x to variables.

        Arguments
        ----------
        config (Union[dict, DataSet]): The configuration data.
        It can be either a dictionary or a DataSet object.
        opt_vars (dict): The optimization variables.

        Returns
        -------
        List[list]: The mapping from x to variables.

        """
        if isinstance(config, dict):
            config_dict = config
        else:
            raise ValueError('config must be a dict')
        # Check if local_optimization is set
        if 'local_optimization' in opt_vars.keys():
            coord_mapping = self.create_local_coord_mapping(
                **opt_vars['local_optimization'], config_dict0=config_dict
            )
            opt_vars['scaled_coordinates']['mapping'] = coord_mapping

        # Create a mapping from x to variables
        mapping_x_to_dict = []
        config_dict = self.toggle_scaled_coordinates(config_dict)

        # Get box lengths, to ensure reasonable scale close to 1
        max_val = {}
        if 'box' in config_dict.keys():
            box_lengths = np.linalg.norm(
                np.array(config_dict['box']).reshape(3, 3), axis=1
            )
        max_box = {}
        max_box[0] = box_lengths[0]
        max_box[4] = box_lengths[1]
        max_box[8] = box_lengths[2]
        max_box[3] = np.max([box_lengths[0] / 4.0, config_dict['box'][3]])
        max_box[6] = np.max([box_lengths[1] / 4.0, config_dict['box'][6]])
        max_box[7] = np.max([box_lengths[2] / 4.0, config_dict['box'][7]])
        min_box = {}
        min_box[0] = 0.0
        min_box[4] = 0.0
        min_box[8] = 0.0
        min_box[3] = 0.0
        min_box[6] = 0.0
        min_box[7] = 0.0

        # Find maximum distance in scaled coordinates
        min_scaled_coordinates = [
            np.min(config_dict['scaled_coordinates'].reshape(-1, 3)[:, i])
            for i in range(3)
        ]
        max_scaled_coordinates = [
            np.max(config_dict['scaled_coordinates'].reshape(-1, 3)[:, i])
            for i in range(3)
        ]

        for key in [
            key for key in opt_vars.keys() if key != 'local_optimization'
        ]:
            max_val = np.max(config_dict[key])
            min_val = np.min(config_dict[key])

            # Special case for degenarte values, such as in a box vector
            if 'mapping' in opt_vars[key].keys():
                for idx in opt_vars[key]['mapping']:
                    if key == 'box':
                        mapping_x_to_dict.append(
                            [key, idx, min_box[idx], max_box[idx]]
                        )
                    elif key == 'scaled_coordinates':
                        mapping_x_to_dict.append(
                            [
                                key,
                                idx,
                                min_scaled_coordinates[idx % 3],
                                max_scaled_coordinates[idx % 3],
                            ]
                        )
                    else:
                        mapping_x_to_dict.append([key, idx, min_val, max_val])
            else:
                for idx, value in enumerate(config_dict[key]):
                    mapping_x_to_dict.append([key, idx, min_val, max_val])
        config_dict = self.toggle_scaled_coordinates(config_dict)
        return mapping_x_to_dict

    def prep_grad(
        self,
        x: List[float],
        config_const: dict,
        mapping_x_to_dict: List[list],
        options: NamedTuple,
        **kwargs,
    ) -> dict:
        """Prepare gradient computation.

        Arguments
        ----------
        x (List[float]): The coordinates of the atoms.
        config_const (dict): The constant configuration data.
        mapping_x_to_dict (List[list]): The mapping from x to variables.
        options (NamedTuple): The options.

        Returns
        -------
        dict: The computed results.

        """
        # First element corresponding unmodified configuration, update with x
        data_dict0 = copy.deepcopy(config_const)
        for x_index, mapping in enumerate(mapping_x_to_dict):
            data_dict0[mapping[0]][mapping[1]] = self.rescale_from_x(
                x[x_index], mapping[-2], mapping[-1]
            )

        # Prepare the data set with 1+ Nxm configurations,
        # needed for numerical gradient
        if options.derivative == 'forward':  # type: ignore
            data_dict = {
                'scaled_coordinates': np.array(
                    [data_dict0['scaled_coordinates']] * (1 + len(x)),
                    dtype=np.float64,
                ),
                'box': np.array(
                    [data_dict0['box']] * (1 + len(x)), dtype=np.float64
                ),
                'atoms': np.atleast_2d([data_dict0['atoms']] * (1 + len(x))),
            }
        elif options.derivative == 'central':  # type: ignore
            data_dict = {
                'scaled_coordinates': np.array(
                    [data_dict0['scaled_coordinates']] * (1 + 2 * len(x)),
                    dtype=np.float64,
                ),
                'box': np.array(
                    [data_dict0['box']] * (1 + 2 * len(x)), dtype=np.float64
                ),
                'atoms': np.atleast_2d(
                    [data_dict0['atoms']] * (1 + 2 * len(x))
                ),
            }
        elif options.derivative == 'noise-robust':  # type: ignore
            data_dict = {
                'scaled_coordinates': np.array(
                    [data_dict0['scaled_coordinates']] * (1 + 4 * len(x)),
                    dtype=np.float64,
                ),
                'box': np.array(
                    [data_dict0['box']] * (1 + 4 * len(x)), dtype=np.float64
                ),
                'atoms': np.atleast_2d(
                    [data_dict0['atoms']] * (1 + 4 * len(x))
                ),
            }

        # Perturb structure for numerical gradients
        box_lengths = np.linalg.norm(data_dict['box'][0].reshape(3, 3), axis=1)
        for x_index, mapping in enumerate(mapping_x_to_dict):

            # set finite difference step
            d = options.opt_vars[mapping[0]]['epsilon']  # type: ignore
            if mapping[0] == 'scaled_coordinates':
                d = d / box_lengths[mapping[1] % 3]
            if mapping[0] == 'box':
                d = d * box_lengths[int(mapping[1] // 3)]

            # Apply finite difference step
            if options.derivative == 'forward':  # type: ignore
                data_dict[mapping[0]][x_index + 1][mapping[1]] += d
            elif options.derivative == 'central':  # type: ignore
                data_dict[mapping[0]][x_index * 2 + 1][mapping[1]] += d
                data_dict[mapping[0]][x_index * 2 + 2][mapping[1]] -= d
            elif options.derivative == 'noise-robust':  # type: ignore
                data_dict[mapping[0]][x_index * 4 + 1][mapping[1]] += d
                data_dict[mapping[0]][x_index * 4 + 2][mapping[1]] -= d
                data_dict[mapping[0]][x_index * 4 + 3][mapping[1]] += 2 * d
                data_dict[mapping[0]][x_index * 4 + 4][mapping[1]] -= 2 * d
            else:
                raise ValueError('Derivative method not recognized.')

        # Toggle back to ordinary coordinates compatible ml interfaces
        data_dict = self.toggle_scaled_coordinates(data_dict)
        # Do unitless for now, to avoid computational costs related to change
        # units.
        tmp_dataset = DataSet(data_dict)  # , units=self.units)

        results_tmp = []
        for infer in self.models_infere_fn:
            # Calculate energy, force, virial for each configuration
            results_tmp.append(infer(data=tmp_dataset))
        results = dict()
        for key, value in data_dict.items():
            results[key] = np.array(value)
        for key in results_tmp[0].keys():
            results[key] = np.array(
                [results_tmp[i][key] for i in range(len(results_tmp))]
            )
        return results

    def write_xyz_file(self, fp_xyz, data_dict, xyz_settings):
        """Write xyz file.

        Arguments
        ---------
        fp_xyz (file): The file object to write the xyz file to.
        data_dict (dict): A dictionary containing the data to be
        written to the xyz file.
        xyz_settings (dict): A dictionary containing the
        settings for the xyz file.

        Returns
        -------
        file: The file object after writing the xyz file.

        """
        for key in xyz_settings['atomic_properties'].keys():
            shape = [-1] + [
                int(xyz_settings['atomic_properties'][key].split(':')[-1])
            ]
            data_dict[key] = data_dict[key].reshape(shape)

        # data_dict = dataset.data.to_dict(orient='list')
        for key in data_dict.keys():
            data_dict[key] = np.array(data_dict[key])

        fp_xyz.write(str(len(data_dict['coordinates'])) + '\n')
        line = ''

        # Write system properties
        for key, value in xyz_settings['system_properties'].items():
            if '{}' in value:
                numbers = ' '.join(
                    [str(number) for number in np.atleast_1d(data_dict[key])]
                )
                line += value.format(numbers) + ' '
            else:
                line += value + ' '

        # Specify properties of atoms
        line += 'Properties='
        for key, value in xyz_settings['atomic_properties'].items():
            if key in data_dict.keys():
                line += value + ':'
        line = line[:-1]
        fp_xyz.write(line + '\n')

        # Write atomic properties
        for i in range(len(data_dict['atoms'])):
            line = ''
            for key, value in xyz_settings['atomic_properties'].items():
                for j in range(len(data_dict[key][i])):
                    line += str(data_dict[key][i][j]) + ' '
            line = line[:-1]
            fp_xyz.write(line + '\n')
        fp_xyz.flush()
        return fp_xyz

    def objective_and_gradient(
        self,
        x: List[float],
        prep_grad: Callable[..., Any],
        mapping_x_to_dict: List[list],
        fp_xyz: Any,
        options: NamedTuple,
    ):
        """Determine objective function and gradient.

        Arugments
        ---------
        x: coordinates of the atoms

        Returns
        -------
        objective: objective function
        gradient: gradient of the objective function

        """
        res = prep_grad(x)
        ret_vars = dict()
        natoms = len(res['atoms'][0])  # noqa: F841
        for key, value in options.fitness.items():  # type: ignore
            if 'replace_string' in value['type']:
                # replace by name of variable
                replace_string = 'res["{}"]'.format(value['key'])  # noqa: W605
                if 'calibration' in value.keys():
                    a = value['calibration'][0]  # noqa: F841
                    b = value['calibration'][1]  # noqa: F841

                else:
                    a = 1.0  # noqa: F841
                    b = 1.0  # noqa: F841

                ret_vars[key] = eval(
                    value['type'].replace('replace_string', replace_string)
                )  # noqa: W605
            else:
                raise NotImplementedError(
                    'Only replace_string is implemented.'
                )

        # Loop over ret_vars and calculate fitness
        if options.derivative == 'forward':  # type: ignore
            fitness_values = np.zeros(len(x) + 1)
        elif options.derivative == 'central':  # type: ignore
            fitness_values = np.zeros(2 * len(x) + 1)
        elif options.derivative == 'noise-robust':  # type: ignore
            fitness_values = np.zeros(4 * len(x) + 1)
        for key, value in ret_vars.copy().items():
            prefactor = options.fitness[key]['prefactor']  # type: ignore
            if isinstance(prefactor, str):
                prefactor = eval(prefactor)
            target = options.fitness[key]['target']  # type: ignore
            if 'norm_function' in options.fitness[key].keys():  # type: ignore
                replace_string = 'ret_vars["{}"]'.format(key)  # noqa: W605
                norm_function = options.fitness[key][  # type: ignore
                    'norm_function'
                ]
                reduced_fitness_value = prefactor * eval(
                    norm_function.replace('replace_string', replace_string)
                )
            else:
                reduced_fitness_value = prefactor * (value - target) ** 2
            while len(reduced_fitness_value.shape) > 1:  # type: ignore
                reduced_fitness_value = np.sum(reduced_fitness_value, axis=-1)
            ret_vars['fitness_' + key] = [reduced_fitness_value[0]]
            fitness_values += reduced_fitness_value

        ret_vars['fitness'] = [fitness_values[0]]
        grad = np.zeros(len(x))

        res = self.toggle_scaled_coordinates(res)
        for x_index, mapping in enumerate(mapping_x_to_dict):
            if options.derivative == 'forward':  # type: ignore
                d_fitness = fitness_values[1 + x_index] - fitness_values[0]
                d_x = self.rescale_to_x(
                    res[mapping[0]][1 + x_index][mapping[1]],
                    mapping[-2],
                    mapping[-1],
                ) - self.rescale_to_x(
                    res[mapping[0]][0][mapping[1]], mapping[-2], mapping[-1]
                )
            elif options.derivative == 'central':  # type: ignore
                d_fitness = (
                    fitness_values[1 + 2 * x_index]
                    - fitness_values[2 + 2 * x_index]
                )
                d_x = self.rescale_to_x(
                    res[mapping[0]][1 + 2 * x_index][mapping[1]],
                    mapping[-2],
                    mapping[-1],
                ) - self.rescale_to_x(
                    res[mapping[0]][2 + 2 * x_index][mapping[1]],
                    mapping[-2],
                    mapping[-1],
                )

            elif options.derivative == 'noise-robust':  # type: ignore
                d_fitness = (
                    2 * fitness_values[1 + 4 * x_index]
                    - 2 * fitness_values[2 + 4 * x_index]
                    + fitness_values[3 + 4 * x_index]
                    - fitness_values[4 + 4 * x_index]
                )
                d_x = 2 * (
                    self.rescale_to_x(
                        res[mapping[0]][3 + 4 * x_index][mapping[1]],
                        mapping[-2],
                        mapping[-1],
                    )
                    - self.rescale_to_x(
                        res[mapping[0]][4 + 4 * x_index][mapping[1]],
                        mapping[-2],
                        mapping[-1],
                    )
                )
            else:
                raise ValueError('Derivative method not recognized.')
            grad[x_index] = d_fitness / d_x
        res = self.toggle_scaled_coordinates(res)
        if fp_xyz is not None:
            # Write geometry to xyz file
            data_to_write = dict()
            for key, value in res.items():
                if (
                    key
                    in options.xyz_settings[  # type: ignore
                        'atomic_properties'
                    ].keys()
                    or key
                    in options.xyz_settings[  # type: ignore
                        'system_properties'
                    ].keys()
                ):
                    data_to_write[key] = value[0]
            for key, value in ret_vars.items():
                data_to_write[key] = value[0]

            self.write_xyz_file(
                fp_xyz, data_to_write, options.xyz_settings  # type: ignore
            )
        return [fitness_values[0], grad, data_to_write]

    def set_options(
        self, options_in: Union[dict, str], pbc: bool = True
    ) -> dict:
        """Set options from a file or a dictionary.

        Arguments
        ---------
        options (Union[dict,str]): The options to be read.
        It can be either a dictionary or a file path.
        input_geometries (dict): The input geometries.

        Returns
        -------
        dict: The read options as a dictionary.

        Raises
        ------
        ValueError: If options is neither a dictionary nor a string.

        """
        # Read in options

        if isinstance(options_in, str):
            with open(options_in, 'r') as fp:
                options = json.load(fp)
        elif isinstance(options_in, dict):
            options = options_in
            pass
        else:
            raise ValueError('options must be a dict or a str.')

        options.setdefault('num_iter', 100)
        options.setdefault('derivative', 'forward')
        assert options['derivative'] in ['forward', 'central', 'noise-robust']

        options.setdefault('minimizer', 'L-BFGS-B')

        # Check for depricated options
        if 'f_pref' in options.keys():
            # Cast an error
            raise ValueError('f_pref is a depricated option.')
        if 'e_pref' in options.keys():
            # Cast an error
            raise ValueError('e_pref is a depricated option.')

        # Set default options for opt_vars
        if 'opt_vars' not in options.keys() or (
            len(options['opt_vars']) == 1
            and 'local_optimization' in options['opt_vars'].keys()
        ):
            # Create a default opt_vars
            opt_vars = options.get('opt_vars', dict())
            if options['derivative'] == 'central':
                opt_vars['coordinates'] = {'shape': [-1, 3], 'epsilon': 1e-7}
                opt_vars['box'] = {
                    'shape': [-1, 9],
                    'epsilon': 5e-7,
                    'mapping': [0, 4, 8, 3, 6, 7],
                }
            elif options['derivative'] == 'forward':
                opt_vars['coordinates'] = {'shape': [-1, 3], 'epsilon': 1e-7}
                opt_vars['box'] = {
                    'shape': [-1, 9],
                    'epsilon': 5e-7,
                    'mapping': [0, 4, 8, 3, 6, 7],
                }
            elif options['derivative'] == 'noise-robust':
                opt_vars['coordinates'] = {'shape': [-1, 3], 'epsilon': 1e-7}
                opt_vars['box'] = {
                    'shape': [-1, 9],
                    'epsilon': 5e-7,
                    'mapping': [0, 4, 8, 3, 6, 7],
                }

            if 'default_opt_vars_params' in options.keys():
                for key in list(opt_vars.keys()):
                    if key in options['default_opt_vars_params'].keys():
                        opt_vars[key].update(
                            options['default_opt_vars_params'][key]
                        )
                    else:
                        opt_vars.pop(key)
            options['opt_vars'] = opt_vars

        if not pbc and 'box' in options['opt_vars'].keys():
            options['opt_vars'].pop('box')

        if 'coordinates' in options['opt_vars'].keys():
            # turn into scaled coordinates, which are optimized
            options['opt_vars']['scaled_coordinates'] = options['opt_vars'][
                'coordinates'
            ]
            options['opt_vars'].pop('coordinates')

        if 'fitness' not in options.keys():
            # look for default fitness parameters
            if 'default_fitness_params' not in options.keys():
                options['default_fitness_params'] = {
                    'energy_per_atoms_dev': {
                        'target': 0.001,
                        'prefactor': 0.0001,
                    },
                    'forces_dev': {
                        'target': 0.1,
                        'prefactor': '1.0 / (3 * natoms)',
                    },
                    'forces_mag': {
                        'target': 10.0,
                        'prefactor': '1E-2/ (3 * natoms)',
                    },
                    'mean_forces': {'target': 0.0},
                    'mean_energy': {'target': 0},
                    'virial_dev': {'target': 0, 'prefactor': 0},
                    'virial_mean': {'target': 0},
                }

            fitness = dict()
            if (
                'energy_per_atoms_dev'
                in options['default_fitness_params'].keys()
            ):
                fitness['energy_per_atoms_dev'] = {
                    'key': 'energy',
                    'target': 0.001,
                    'shape': [1],
                    'type': '(a*np.std(replace_string,'
                    + 'axis=0,ddof=1)**b)/natoms',
                    'prefactor': 0.0001,
                    'calibration': [1, 1],
                    'norm_function': '((replace_string-target)/target)**2',
                }
                fitness['energy_per_atoms_dev'].update(
                    options['default_fitness_params']['energy_per_atoms_dev']
                )

            if 'forces_dev' in options['default_fitness_params'].keys():
                fitness['forces_dev'] = {
                    'key': 'forces',
                    'target': 0.1,
                    'shape': [-1, 1],
                    'type': """np.sum((a*np.std(
                replace_string.reshape(replace_string.shape[0],
                replace_string.shape[1],-1,3)
                ,ddof=1,axis=0)**b)**2,axis=-1)**0.5""",
                    'calibration': [1, 1],
                    'prefactor': '1.0 / (3 * natoms)',
                }
                fitness['forces_dev'].update(
                    options['default_fitness_params']['forces_dev']
                )
            if 'forces_mag' in options['default_fitness_params'].keys():
                fitness['forces_mag'] = {
                    'key': 'forces',
                    'target': 5.0,
                    'shape': [-1, 1],
                    'type': 'np.linalg.norm(np.mean('
                    + 'replace_string.reshape(replace_string.shape[0],'
                    + 'replace_string.shape[1],-1,3)'
                    + ',axis=0),axis=-1)',
                    'prefactor': '1E-2 / (3 * natoms)',
                    'norm_function': '(np.maximum(0,'
                    + 'replace_string-target)/target)**2',
                }
                fitness['forces_mag'].update(
                    options['default_fitness_params']['forces_mag']
                )
            if 'mean_forces' in options['default_fitness_params'].keys():
                fitness['mean_forces'] = {
                    'key': 'forces',
                    'target': 0.0,
                    'shape': [-1, 3],
                    'type': 'np.mean(replace_string.reshape('
                    + 'replace_string.shape[0]'
                    + ',replace_string.shape[1],-1,3),axis=0)',
                    'prefactor': 0,
                }
                fitness['mean_forces'].update(
                    options['default_fitness_params']['mean_forces']
                )
            if 'mean_energy' in options['default_fitness_params'].keys():
                fitness['mean_energy'] = {
                    'key': 'energy',
                    'target': 0,
                    'shape': [1],
                    'type': 'np.mean(replace_string,axis=0)',
                    'prefactor': 0,
                }
                fitness['mean_energy'].update(
                    options['default_fitness_params']['mean_energy']
                )
            if 'virial_dev' in options['default_fitness_params'].keys():
                fitness['virial_dev'] = {
                    'key': 'virial',
                    'target': 0.001,
                    'shape': [9],
                    'type': '(a*np.std(replace_string,'
                    + 'axis=0,ddof=1)**b)/natoms',
                    'prefactor': 0,
                    'calibration': [1, 1],
                    'norm_function': '((replace_string-target)/target)**2/9',
                }
                fitness['virial_dev'].update(
                    options['default_fitness_params']['virial_dev']
                )
            if 'virial_mean' in options['default_fitness_params'].keys():
                fitness['virial_mean'] = {
                    'key': 'virial',
                    'target': 0,
                    'shape': [9],
                    'type': 'np.mean(replace_string,axis=0)',
                    'prefactor': 0,
                }
                fitness['virial_mean'].update(
                    options['default_fitness_params']['virial_mean']
                )

            options['fitness'] = fitness

        # Add uncertainty calibration for each fitness
        uncertainty_calibration = options.get('uncertainty_calibration', {})
        for key, value in options['fitness'].items():
            if (
                value['key'] in uncertainty_calibration.keys()
                and 'calibration' in value.keys()
            ):
                value['calibration'] = uncertainty_calibration[value['key']]

        options['pbc'] = pbc

        # Setup how files should be written
        xyz_settings: Dict[str, dict] = dict()
        xyz_settings['system_properties'] = dict()
        xyz_settings['atomic_properties'] = dict()

        if pbc:
            xyz_settings['system_properties']['box'] = 'Lattice="{}"'
            xyz_settings['system_properties']['pbc'] = 'pbc="T T T"'
        else:
            xyz_settings['system_properties']['pbc'] = 'pbc="F F F"'

        xyz_settings['atomic_properties']['atoms'] = 'species:S:1'
        xyz_settings['atomic_properties']['coordinates'] = 'pos:R:3'
        for key, value in options['fitness'].items():
            if value['shape'][0] == -1:
                xyz_settings['atomic_properties'][key] = (
                    key + ':R:' + str(value['shape'][-1])
                )
            else:
                xyz_settings['system_properties'][key] = key + '="{}"'
        for key, value in options['fitness'].items():
            # Print a system property for each fitness, using fitness_key
            new_key = 'fitness_' + key
            xyz_settings['system_properties'][new_key] = new_key + '="{}"'
        xyz_settings['system_properties']['fitness'] = 'fitness="{}"'

        for key, value in options['opt_vars'].items():
            if key == 'local_optimization':
                continue
            if key == 'scaled_coordinates':
                continue
            if key == 'box':
                continue
            if (
                value['shape'][0] != -1
                and key not in xyz_settings['system_properties'].keys()
            ):
                xyz_settings['system_properties'][key] = key + '="{}"'
            elif key not in xyz_settings['atomic_properties'].keys():
                xyz_settings['atomic_properties'][key] = (
                    key + ':R:' + str(value['shape'][-1])
                )
        options['xyz_settings'] = xyz_settings

        return options

    def create_local_coord_mapping(self, indices, config_dict0, cutoff=0.0):
        """Select atoms within a cutoff radius of central atoms.

        Arguments
        ---------
        indices (List[int]): The indices of central.
        cutoff (float): The cutoff radius.
        config_dict0 (dict): The configuration data.

        Returns
        -------
        List[list]: The mapping from local coordinates to global coordinates.

        """
        neighbors = []
        coordinates = np.array(config_dict0['coordinates']).reshape((-1, 3))
        potential_neighbors = [
            index for index in range(len(coordinates)) if index not in indices
        ]
        box = np.array(config_dict0['box']).reshape((3, 3))
        for index_central in indices:
            for index in potential_neighbors:
                distance_vector = (
                    coordinates[index] - coordinates[index_central]
                )
                distance_vector = np.matmul(
                    distance_vector, np.linalg.inv(box)
                )
                distance_vector -= np.round(distance_vector)
                distance_vector = np.matmul(distance_vector, box)
                distance = np.linalg.norm(distance_vector)
                if distance < cutoff:
                    neighbors.append(index)
        neighbors = list(set(neighbors))
        mapping = []

        for index in indices + neighbors:
            for dim in range(3):
                mapping.append(index * 3 + dim)

        return mapping

    def toggle_scaled_coordinates(self, config_dict, normalize=False):
        """Toggle between scaled and unscaled coordinates.

        Arguments
        ---------
        config_dict (dict): The configuration data.

        Returns
        -------
        dict: The configuration data.

        """
        box = np.array(config_dict['box'], dtype=np.float64)
        box = box.reshape((-1, 3, 3))
        num_frames = len(box)

        if 'scaled_coordinates' in config_dict.keys():

            scaled_coordinates = np.array(config_dict['scaled_coordinates'])
            coordinates_original_shape = config_dict[
                'scaled_coordinates'
            ].shape
            scaled_coordinates = scaled_coordinates.reshape(num_frames, -1, 3)
            coordinates = np.einsum('ijk,ilk->ijl', box, scaled_coordinates)
            coordinates = np.moveaxis(coordinates, -1, -2)
            config_dict['coordinates'] = coordinates.reshape(
                coordinates_original_shape
            )
            config_dict.pop('scaled_coordinates')
        else:
            box_inv = np.empty_like(box)
            for i in range(num_frames):
                box_inv[i] = np.linalg.inv(box[i])

            coordinates = np.array(config_dict['coordinates'])
            coordinates_original_shape = coordinates.shape
            coordinates = coordinates.reshape((num_frames, -1, 3))
            scaled_coordinates = np.einsum(
                'ijk,ilk->ijl', box_inv, coordinates
            )
            # reorder the two last dimensions
            scaled_coordinates = np.moveaxis(scaled_coordinates, -1, -2)
            config_dict['scaled_coordinates'] = scaled_coordinates.reshape(
                coordinates_original_shape
            )
            config_dict.pop('coordinates')

        return config_dict

    def rescale_to_x(self, value, value_min, value_max, a=0.0, b=1.0):
        """Min max normalization."""
        return (value - value_min) * (b - a) / (value_max - value_min) + a

    def rescale_from_x(self, x, x_min, x_max, a=0.0, b=1.0):
        """Inverse min max normalization."""
        return (x - a) / (b - a) * (x_max - x_min) + x_min

    def rescale_to_dx(self, value, value_min, value_max, a=0.0, b=1.0):
        """Min max normalization."""
        return value / ((b - a) / (value_max - value_min))

    def rescale_from_dx(self, xi, x_min, x_max, a=0.0, b=1.0):
        """Inverse min max normalization."""
        return xi * (b - a) / (x_max - x_min)


def main():
    """Run configurational refinement."""
    import argparse

    parser = argparse.ArgumentParser(
        description='Simple configurational refinement'
    )
    parser.add_argument('--models', type=str, nargs='+', help='List of models')
    parser.add_argument(
        '--input_geometries', type=str, help='Input geometries'
    )
    parser.add_argument('--options', type=str, help='Options', default={})
    parser.add_argument(
        '--output', type=str, help='Output file', default='output.json'
    )
    parser.add_argument(
        '--ml_engines',
        nargs='+',
        help='ML engines, using string representation, to be exceuted.',
        default=[],
    )
    args = parser.parse_args()

    with open(args.input_geometries, 'r') as fp:
        input_geometries = json.load(fp)
    # Create ML engines
    ml_engines = args.ml_engines
    if ml_engines is []:
        for model in args.models:
            if str(model).endswith('.pb'):
                myenv_deepmd = create_compute_settings()
                mydeepmd = hyif.DeepMD(
                    {'check_version': False}, compute_settings=myenv_deepmd
                )
                ml_engines.append(mydeepmd)
            else:
                raise ValueError(
                    'ML model are incorrectly specified, must be .pb file.'
                )
    elif isinstance(ml_engines, str) and ml_engines.endswith('.pkl'):
        # load as pickle
        with open(ml_engines, 'rb') as fp:
            ml_engines = pickle.load(fp)

    # Generate configurations
    if len(args.models) == 1:
        args.models = args.models[0].split()

    my_conref = ConRef(args.models)
    xyz_name = '.'.join(args.input_geometries.split('.')[:-1]) + '.xyz'

    configs = my_conref.gen_configs(
        input_geometries, args.options, xyz_file=xyz_name
    )

    configs.data.to_json(args.output, indent=4, orient='records', index=False)
    with open(args.output, 'r') as fp:
        configs = json.load(fp)

    # TODO: Do it without a hack
    configs_final = {}
    for key in configs[0].keys():
        configs_final[key] = []
        for config in configs:
            configs_final[key].append(config[key])
    with open(args.output, 'w') as fp:
        json.dump(configs_final, fp, indent=4)


if __name__ == '__main__':
    main()
