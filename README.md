# HyAL - Active learning with the Hylleraas software platform

![Active learning loop with calibrated adversarial geometry optimization (CAGO). Uncertainty calibration, the determination of calibration parameters, is performed using training data and a committee of MLIPs. The CAGO algorithm is applied to the MD-sampled structures from the current iteration of the MLIPs to obtain new structures. These structures are subsequently used in reference ab initio calculations and added to the training set.](docs/img/AL_loop.png)


If you use our active learning software, please cite the following paper:
* Learning atomic forces from uncertainty-calibrated adversarial attacks H.M. Cezar, T. Bodenstein, H.A. Sveinsson, M. Ledum, S. Reine, SLB (2025) [arXiv](https://arxiv.org/abs/2502.18314)
