import os
from datetime import timedelta

import hyif
import hyset
import pytest
from hyobj import PeriodicSystem

import hyal

betzy_scratch_dir = '/cluster/work/users/sigbjobo/test_hyal'
betzy_username = 'sigbjobo'
betzy_account = 'nn4654k'
env_cp2k_slurm_betzy = hyset.SlurmArch(
    target='betzy',
    progress_bar=False,
    data_dir_local=os.getcwd(),
    work_dir_local=os.getcwd(),
    submit_dir_remote=betzy_scratch_dir,
    data_dir_remote=betzy_scratch_dir,
    work_dir_remote=betzy_scratch_dir,
    force_recompute=False,
    overwrite_files=True,
    host='betzy',
    job_time=timedelta(hours=1),
    slurm_account=betzy_account,
    memory_per_cpu=None,
    ntasks=128,
    debug=True,
    cpus_per_task=1,
    user='sigbjobo',
    env=['module load CP2K/2022.1-foss-2022a'],
    slurm_extra=['--nodes=4', '--partition=normal'],
)


cp2k_input = """&GLOBAL
  PRINT_LEVEL LOW
  PROJECT_NAME NAME
  RUN_TYPE ENERGY_FORCE
&END GLOBAL
&FORCE_EVAL
  METHOD Quickstep
  STRESS_TENSOR ANALYTICAL
  &PRINT
    &FORCES ON
    &END FORCES
  &END PRINT
  &SUBSYS
    &CELL
      TO_REPLACE_WITH_CELL_COORDINATES
    &END CELL
    &COORD
      TO_REPLACE_WITH_MOLECULE_COORDINATES
    &END COORD
    &KIND H
      POTENTIAL GTH-PBE
      ELEMENT H
      BASIS_SET DZVP-GTH-PBE
    &END KIND
    &KIND O
      POTENTIAL GTH-PBE
      ELEMENT O
      BASIS_SET DZVP-GTH-PBE
    &END KIND
  &END SUBSYS
  &DFT
    POTENTIAL_FILE_NAME GTH_POTENTIALS
    MULTIPLICITY 0
    CHARGE 0
    BASIS_SET_FILE_NAME BASIS_SET
    BASIS_SET_FILE_NAME BASIS_MOLOPT
    BASIS_SET_FILE_NAME BASIS_MOLOPT_UCL
    &MGRID
      REL_CUTOFF 60
      CUTOFF 800
    &END MGRID
    &QS
      EPS_DEFAULT 1e-12
    &END QS
    &XC
      &XC_FUNCTIONAL PBE
      &END XC_FUNCTIONAL
      &XC_GRID
        XC_DERIV PW
      &END XC_GRID
    &END XC
    &SCF
      EPS_SCF 1e-05
      MAX_SCF 20
      SCF_GUESS ATOMIC
      &OUTER_SCF
        EPS_SCF 1e-05
        MAX_SCF 100
      &END OUTER_SCF
      &OT
        PRECONDITIONER FULL_SINGLE_INVERSE
        MINIMIZER DIIS
      &END OT
    &END SCF
  &END DFT
&END FORCE_EVAL
"""
cp2k_single_point = hyif.CP2K(
    {'check_version': False, 'template': cp2k_input},
    compute_settings=env_cp2k_slurm_betzy,
)


@pytest.mark.integtest
def test_calc_parallel():
    """Integration test for calculator_parallel function."""
    al = hyal.ActiveLearning(calculator=cp2k_single_point)
    ice_geo = PeriodicSystem('data/cp2k_calc/water.xyz', units='metal')
    run_opts = [
        {'sub_dir': 'results_tmp/test_calc_parallel/'},
        {'sub_dir': 'results_tmp/test_calc_parallel/'},
    ]
    al.calculator_parallel([ice_geo] * 2, run_opts=run_opts)
