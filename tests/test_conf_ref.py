import copy
import json
from collections import namedtuple

import hyif
import numpy as np
from hyobj import DataSet

from hyal import conref


def test_opt_local():
    """Test local optimization."""
    with open('data/conf_ref_ice/ice.json') as f:
        geometry = json.load(f)
        # single atom with no cutoff
        conf_ref = conref.ConRef(models=['dummy.pb'])
        local_optimization = {'indices': [0], 'cutoff': 0.0}
        mapping = conf_ref.create_local_coord_mapping(
            config_dict0=geometry, **local_optimization
        )
        assert mapping == [0, 1, 2]

        local_optimization = {'indices': [5]}
        mapping = conf_ref.create_local_coord_mapping(
            config_dict0=geometry, **local_optimization
        )
        assert mapping == [15, 16, 17]

        # three atoms with no cutoff
        local_optimization = {'indices': [0, 2, 3], 'cutoff': 0.0}
        mapping = conf_ref.create_local_coord_mapping(
            config_dict0=geometry, **local_optimization
        )
        assert len(mapping) == 9

        # single atom with cutoff (water molecule)
        local_optimization = {'indices': [0], 'cutoff': 1.2}
        mapping = conf_ref.create_local_coord_mapping(
            config_dict0=geometry, **local_optimization
        )
        assert len(mapping) == 9

        # All atoms around first atom
        local_optimization = {'indices': [0], 'cutoff': 100}
        mapping = conf_ref.create_local_coord_mapping(
            config_dict0=geometry, **local_optimization
        )
        assert len(mapping) == len(geometry['coordinates'])


def test_parser():
    """Test input and output parser."""
    conf_ref = conref.ConRef(models=['dummy.pb'])
    # Test input parser
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)

    # Test output parser
    output_file = (
        'data/conf_ref/af27a82dee38dfe41426c4af61ec8c479b065467.output'
    )
    output = conf_ref.OutputParser.parse(output_file)
    assert not output['is_converged']

    # Create Molecule object
    # change from 'box' to 'unit_cell' for geometry dict
    geometry['unit_cell'] = geometry.pop('box')
    # geometry['coordinates'] = geometry.pop('coord')

    # Test reading from file
    n_repeat = 3
    geomtry_list = dict()
    for key, value in geometry.items():
        geomtry_list[key] = [value] * n_repeat
    assert len(DataSet(geomtry_list)) == n_repeat


def test_scaled_coordinates():
    """Test transformation to scaled coordinates."""
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    for key in ['box', 'coordinates']:
        geometry[key] = np.array(geometry[key])
    geometry0 = copy.deepcopy(geometry)
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    assert 'scaled_coordinates' in geometry.keys()
    scaled_coordinates_hyal = (
        geometry['scaled_coordinates'].copy().reshape(-1, 3)
    )
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    assert 'coordinates' in geometry.keys()

    # Check that reversion is correct
    for key in ['box', 'coordinates']:
        assert np.allclose(geometry0[key], geometry[key])

    try:
        import MDAnalysis  # noqa: F401
        import MDAnalysis.analysis.distances  # noqa: F401

        box = geometry['box']
        dimensions = MDAnalysis.lib.mdamath.triclinic_box(
            box[:3], box[3:6], box[6:9]
        )
        scaled_coordinates = MDAnalysis.lib.distances.transform_RtoS(
            geometry['coordinates'].reshape(-1, 3), dimensions
        )
        assert np.allclose(
            scaled_coordinates, scaled_coordinates_hyal, rtol=1e-7
        )
    except ImportError:
        pass


def test_rescale_functions():
    """Test the map functions."""
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    a = 0.0
    b = 100.0
    values = np.array([5, 5, 10.0])
    values_min = values.min()
    values_max = values.max()
    x = []
    for i, value in enumerate(values):
        x.append(conf_ref.rescale_to_x(value, values_min, values_max, a, b))
    values_new = []
    for i, value in enumerate(x):
        values_new.append(
            conf_ref.rescale_from_x(value, values_min, values_max, a, b)
        )
    assert np.allclose(x, [0.0, 0.0, 100.0])
    assert np.allclose(values, values_new)

    # Special test for case where values_min and values_max are kept same, but
    # where values are outside the range.
    vals2 = [-5, 5, 5, 10.0]
    x2 = []
    for i, value in enumerate(vals2):
        x2.append(conf_ref.rescale_to_x(value, values_min, values_max, a, b))
    vals2_back = []
    for i, value in enumerate(x2):
        vals2_back.append(
            conf_ref.rescale_from_x(value, values_min, values_max, a, b)
        )
    assert np.allclose(vals2, vals2_back)

    def fn(value):
        return value**2 + value

    # compute the derivative of f
    def dfn_exact(value):
        return 2 * value + 1

    eps = 1e-6
    x5 = conf_ref.rescale_to_x(5, values_min, values_max, a, b)
    x5_plus_eps = x5 + eps
    x5_minus_eps = x5 - eps
    value_plus_eps = conf_ref.rescale_from_x(
        x5_plus_eps, values_min, values_max, a, b
    )
    value_minus_eps = conf_ref.rescale_from_x(
        x5_minus_eps, values_min, values_max, a, b
    )
    dfndx_num = (fn(value_plus_eps) - fn(value_minus_eps)) / (2 * eps)
    assert np.allclose(
        dfndx_num,
        conf_ref.rescale_to_dx(dfn_exact(5), values_min, values_max, a, b),
        rtol=1e-6,
    )

    assert np.allclose(
        conf_ref.rescale_from_dx(dfndx_num, values_min, values_max, a, b),
        dfn_exact(5),
        rtol=1e-6,
    )

    # Transform a molecular geometry back and forth.
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    # Create a mapping
    opt_vars = {
        'box': {
            'shape': [-1, 9],
            'epsilon': 0.015,
            'mapping': [0, 4, 8],
            'scale': 10.0,
        },
        'scaled_coordinates': {'shape': [-1, 3], 'epsilon': 5e-3},
    }

    mapping_x_to_dict = conf_ref.create_mapping(geometry, opt_vars)

    # turn dict into x
    x0 = np.zeros(len(mapping_x_to_dict))
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    for x_index, mapping in enumerate(mapping_x_to_dict):
        x0[x_index] = geometry[mapping[0]][mapping[1]]
        x0[x_index] = conf_ref.rescale_to_x(
            x0[x_index], mapping[-2], mapping[-1]
        )
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    geometry2 = copy.deepcopy(geometry)
    geometry2 = conf_ref.toggle_scaled_coordinates(geometry2)
    for x_index, mapping in enumerate(mapping_x_to_dict):
        geometry2[mapping[0]][mapping[1]] = conf_ref.rescale_from_x(
            x0[x_index], mapping[-2], mapping[-1]
        )
    geometry2 = conf_ref.toggle_scaled_coordinates(geometry2)
    assert np.allclose(geometry['coordinates'], geometry2['coordinates'])


def test_force_local_opt_derivative():
    """Test local optimization with derivative."""
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    input_file = 'data/conf_ref_ice/ice.json'

    eps_central = 5e-3
    geometry = conf_ref.InputParser.parse(input_file)
    options_local = {
        'opt_vars': {
            'coordinates': {'shape': [-1, 3], 'epsilon': eps_central},
            'local_optimization': {'indices': [0], 'cutoff': 0.0},
        },
        'derivative': 'central',
    }

    parsed_local = conf_ref.set_options(options_local)
    options_local = namedtuple('options', parsed_local.keys())(
        *parsed_local.values()
    )
    mapping_x_to_dict_local = conf_ref.create_mapping(
        geometry, parsed_local['opt_vars']
    )

    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    x0 = np.zeros(len(mapping_x_to_dict_local))
    for x_index, mapping in enumerate(mapping_x_to_dict_local):
        x0[x_index] = conf_ref.rescale_to_x(
            geometry[mapping[0]][mapping[1]], mapping[-2], mapping[-1]
        )

    results = conf_ref.prep_grad(
        x0, geometry, mapping_x_to_dict_local, options_local
    )

    assert len(x0) == 3

    # Verify that x positions of atom 1 has been moved by eps
    dx = -results['coordinates'][2, 0] + results['coordinates'][1, 0]
    assert np.allclose(dx, 2 * eps_central, rtol=1e-8)

    # Verify that the rest of the coordinates have not changed
    assert np.allclose(
        results['coordinates'][0, 1:], results['coordinates'][1, 1:], rtol=1e-8
    )
    force1 = results['forces'][0, 0, 0] * 1000
    de = results['energy'][0, 1] - results['energy'][0, 2]
    force2 = -de / dx * 1000
    assert np.allclose(force1, force2, atol=1)  # 1meV/Angstrom


def test_cell_gradient():
    """Test gradient calculation."""
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    geometry = conf_ref.InputParser.parse('data/conf_ref_ice/ice.json')
    # Move to testing virial with numerical derivative
    eps = 0.0015
    options_box_central = {
        'opt_vars': {
            'box': {
                'shape': [-1, 9],
                'epsilon': eps,
                'mapping': [0, 4, 8, 3, 6, 7],
            }
        },
        'derivative': 'central',
    }
    parsed_box_central = conf_ref.set_options(options_box_central)

    mapping_box_to_dict = conf_ref.create_mapping(
        geometry, parsed_box_central['opt_vars']
    )
    options_box_central = namedtuple('options', parsed_box_central.keys())(
        *parsed_box_central.values()
    )
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    x0 = np.zeros(len(mapping_box_to_dict))
    for x_index, mapping in enumerate(mapping_box_to_dict):
        x0[x_index] = conf_ref.rescale_to_x(
            geometry[mapping[0]][mapping[1]], mapping[-2], mapping[-1]
        )

    results = conf_ref.prep_grad(
        x0, geometry, mapping_box_to_dict, options_box_central
    )
    dx = results['box'][1, 0] - results['box'][2, 0]
    # Check that the box has not changed
    assert np.allclose(geometry['box'][0], results['box'][0, 0], rtol=1e-8)

    # Check that the dx is 2*eps x box length
    assert np.allclose(dx, 2 * eps * geometry['box'][0], rtol=1e-8)

    # Verify that virial is the same as numerical derivative
    virial1 = results['virial'][0, 0, 0]
    de = results['energy'][0, 1] - results['energy'][0, 2]
    virial2 = -de / dx * results['box'][0, 0]

    # Check that the virial has 5 meV accuracy
    assert np.allclose(virial1 * 1000, virial2 * 1000, atol=5)

    # Verify forward derivative
    eps = 0.00015
    options_box_forward = {
        'opt_vars': {
            'box': {
                'shape': [-1, 9],
                'epsilon': eps,
                'mapping': [0, 4, 8, 3, 6, 7],
            }
        },
        'derivative': 'forward',
    }
    parsed_box_forward = conf_ref.set_options(options_box_forward)
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    mapping_box_to_dict = conf_ref.create_mapping(
        geometry, parsed_box_forward['opt_vars']
    )
    options_box_forward = namedtuple('options', parsed_box_forward.keys())(
        *parsed_box_forward.values()
    )
    x0 = np.zeros(len(mapping_box_to_dict))
    for x_index, mapping in enumerate(mapping_box_to_dict):
        x0[x_index] = conf_ref.rescale_to_x(
            geometry[mapping[0]][mapping[1]], mapping[-2], mapping[-1]
        )
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    results = conf_ref.prep_grad(
        x0, geometry, mapping_box_to_dict, options_box_forward
    )
    dx = results['box'][1, 0] - results['box'][0, 0]
    assert np.allclose(dx, eps * results['box'][0, 0], rtol=1e-8)

    # Verify that virial is the same as numerical derivative
    virial1 = results['virial'][0, 0, 0]
    de = results['energy'][0, 1] - results['energy'][0, 0]
    virial2 = -de / dx * results['box'][0, 0]

    # Check that the virial has 50 meV accuracy
    assert np.allclose(virial1 * 1000, virial2 * 1000, atol=100)


def test_coordinates_gradient():
    """Test gradient calculation."""
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)

    # for eps in np.logspace(-4, -2, 10):
    # eps = 0.01
    eps_forward = 5e-5
    eps_central = 5e-5
    options_forward = {
        'opt_vars': {
            'coordinates': {'shape': [-1, 3], 'epsilon': eps_forward}
        },
        'derivative': 'forward',
    }
    options_central = {
        'opt_vars': {
            'coordinates': {'shape': [-1, 3], 'epsilon': eps_central}
        },
        'derivative': 'central',
    }

    parsed_forward = conf_ref.set_options(options_forward)
    parsed_central = conf_ref.set_options(options_central)

    options_forward = namedtuple('options', parsed_forward.keys())(
        *parsed_forward.values()
    )
    options_central = namedtuple('options', parsed_central.keys())(
        *parsed_central.values()
    )

    # Create a mapping, same for central derivative
    mapping_x_to_dict = conf_ref.create_mapping(
        geometry, parsed_forward['opt_vars']
    )

    geometry = conf_ref.InputParser.parse(input_file)
    conf_ref.toggle_scaled_coordinates(geometry)
    x0 = np.zeros(len(mapping_x_to_dict))
    for x_index, mapping in enumerate(mapping_x_to_dict):
        x0[x_index] = conf_ref.rescale_to_x(
            geometry[mapping[0]][mapping[1]], mapping[-2], mapping[-1]
        )

    results = conf_ref.prep_grad(
        x0, geometry, mapping_x_to_dict, options_forward
    )

    # Verify that x positions of atom 1 has been moved by eps
    dx = -results['coordinates'][0, 0] + results['coordinates'][1, 0]
    assert np.allclose(dx, eps_forward, rtol=1e-8)

    # Verify that the rest of the coordinates have not changed
    assert np.allclose(
        results['coordinates'][0, 1:], results['coordinates'][1, 1:], rtol=1e-8
    )
    force1 = results['forces'][0, 0, 0] * 1000
    de = results['energy'][0, 1] - results['energy'][0, 0]
    force2 = -de / dx * 1000
    assert np.allclose(force1, force2, atol=50)  # 50meV/Angstrom

    # move to central derivative
    results = conf_ref.prep_grad(
        x0, geometry, mapping_x_to_dict, options_central
    )
    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    # Verify that x positions of atom 1 has been moved by eps
    assert len(results['coordinates']) == len(geometry['coordinates']) * 2 + 1
    assert np.allclose(
        results['coordinates'][1, 0] - results['coordinates'][2, 0],
        2 * eps_central,
        rtol=1e-8,
    )

    # Verify that the rest of the coordinates have not changed
    assert np.allclose(
        results['coordinates'][0, 2:], results['coordinates'][1, 2:], rtol=1e-8
    )

    force1 = results['forces'][0, 0, 0] * 1000
    de = results['energy'][0, 1] - results['energy'][0, 2]
    force2_central = -de / (2 * eps_central) * 1000
    assert np.allclose(force1, force2_central, atol=1)


def test_force_directly():
    """Test direct calculation of forces."""
    conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    dataset = DataSet(
        'data/conf_ref_ice/ice.json', parser=conf_ref.InputParser
    )
    options_in = {
        'derivative': 'forward',
        'opt_vars': {'coordinates': {'shape': [-1, 3], 'epsilon': 1e-6}},
        'fitness': {
            'energy': {
                'key': 'energy',
                'target': 0.0,
                'shape': [1],
                'type': 'np.mean(replace_string,axis=0)',
                'prefactor': 1,
                'norm_function': 'replace_string',
            }
        },
    }
    # Create a mapping
    parsed = conf_ref.set_options(options_in)
    options = namedtuple('options', parsed.keys())(*parsed.values())
    mapping_x_to_dict = conf_ref.create_mapping(geometry, parsed['opt_vars'])

    geometry = conf_ref.toggle_scaled_coordinates(geometry)
    x0 = np.zeros(len(mapping_x_to_dict))
    for x_index, mapping in enumerate(mapping_x_to_dict):
        x0[x_index] = conf_ref.rescale_to_x(
            geometry[mapping[0]][mapping[1]], mapping[-2], mapping[-1]
        )
    # geometry = conf_ref.toggle_scaled_coordinates(geometry)

    # use objective and gradient to compute fitness (energy) and
    # gradient -forces
    def prep_grad(
        x,
        config_const=geometry,
        mapping_x_to_dict=mapping_x_to_dict,
        options=options,
        **kwargs,
    ):
        return conf_ref.prep_grad(
            x, config_const, mapping_x_to_dict, options, **kwargs
        )

    with open('dummy.xyz', 'w') as f:
        energy, grad, all_data = conf_ref.objective_and_gradient(
            x0, prep_grad, mapping_x_to_dict, f, options
        )
    # rescale back derivative
    for x_index, mapping in enumerate(mapping_x_to_dict):
        grad[x_index] = conf_ref.rescale_from_dx(
            grad[x_index], mapping[-2], mapping[-1]
        )
    # this gives gradient for scaled coordinates
    grad = np.array(grad)
    # now we need to devide by box length
    box_length = np.array(geometry['box']).reshape(3, 3)
    box_length = np.linalg.norm(box_length, axis=1)
    for i in range(len(grad)):
        grad[i] = grad[i] / box_length[i % 3]

    # Compute reference forces
    mydeepmd = hyif.DeepMD({'check_version': False})

    infer_res = mydeepmd.infer('data/conf_ref_ice/dummy.pb', dataset)
    forces = infer_res['forces']

    forces_mev_ref = np.array(forces).flatten() * 1000
    forces_mev = -grad.flatten() * 1000

    assert np.allclose(forces_mev_ref, forces_mev, rtol=1e-2)


def test_conf_ref_single_h2o():
    """Test a single iteration of optimization."""
    conf_ref = conref.ConRef(
        models=['data/conf_ref_ice/dummy.pb', 'data/conf_ref_ice/dummy2.pb']
    )
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    options_in = {
        'opt_vars': {
            'local_optimization': {'indices': [0, 1, 2], 'cutoff': 0.0}
        },
        'default_fitness_params': {'forces_dev': {'target': 0.1}},
    }
    conf_ref.gen_configs(geometry, options_in)


def test_conf_ref_gas():
    """Test a single iteration of optimization."""
    conf_ref = conref.ConRef(
        models=['data/conf_ref_ice/dummy.pb', 'data/conf_ref_ice/dummy2.pb']
    )
    input_file = 'data/conf_ref_dimer/dimer.input'
    geometry = conf_ref.InputParser.parse(input_file)
    options_in = {
        'derivative': 'forward',
        'num_iter': 1000,
        'default_fitness_params': {
            'forces_dev': {'target': 0.1},
            'forces_mag': {'target': 10.0},
        },
        'default_opt_vars_params': {'coordinates': {}},
    }
    conf_ref.gen_configs(geometry, options_in)


def test_conf_ref_box():
    """Test a single iteration of optimization."""
    conf_ref = conref.ConRef(
        models=['data/conf_ref_ice/dummy.pb', 'data/conf_ref_ice/dummy2.pb']
    )
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    options_in = {
        'derivative': 'forward',
        'default_fitness_params': {
            'forces_dev': {'target': 0.1},
            'forces_mag': {'target': 5.0},
        },
        'default_opt_vars_params': {'box': {}},
    }
    conf_ref.gen_configs(geometry, options_in)


def test_conf_ref_coordinates():
    """Test a single iteration of optimization."""
    conf_ref = conref.ConRef(
        models=['data/conf_ref_ice/dummy.pb', 'data/conf_ref_ice/dummy2.pb']
    )
    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    options_in = {
        'derivative': 'forward',
        'default_fitness_params': {'forces_dev': {'target': 0.1}},
        'default_opt_vars_params': {'coordinates': {}},
    }
    conf_ref.gen_configs(geometry, options_in)


def test_conf_ref_full():
    """Test a single iteration of optimization."""
    conf_ref = conref.ConRef(
        models=['data/conf_ref_ice/dummy.pb', 'data/conf_ref_ice/dummy2.pb']
    )

    input_file = 'data/conf_ref_ice/ice.json'
    geometry = conf_ref.InputParser.parse(input_file)
    options_in = {
        'num_iter': 3,
        'derivative': 'forward',
        'default_fitness_params': {
            'forces_dev': {'target': 0.1},
            'forces_mag': {'target': 5.0},
        },
    }

    configs = conf_ref.gen_configs(geometry, options_in)
    configs.data.to_json('relaxed_ice.json')
