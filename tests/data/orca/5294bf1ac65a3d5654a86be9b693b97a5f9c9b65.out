
                                 *****************
                                 * O   R   C   A *
                                 *****************

                                            #,
                                            ###
                                            ####
                                            #####
                                            ######
                                           ########,
                                     ,,################,,,,,
                               ,,#################################,,
                          ,,##########################################,,
                       ,#########################################, ''#####,
                    ,#############################################,,   '####,
                  ,##################################################,,,,####,
                ,###########''''           ''''###############################
              ,#####''   ,,,,##########,,,,          '''####'''          '####
            ,##' ,,,,###########################,,,                        '##
           ' ,,###''''                  '''############,,,
         ,,##''                                '''############,,,,        ,,,,,,###''
      ,#''                                            '''#######################'''
     '                                                          ''''####''''
             ,#######,   #######,   ,#######,      ##
            ,#'     '#,  ##    ##  ,#'     '#,    #''#        ######   ,####,
            ##       ##  ##   ,#'  ##            #'  '#       #        #'  '#
            ##       ##  #######   ##           ,######,      #####,   #    #
            '#,     ,#'  ##    ##  '#,     ,#' ,#      #,         ##   #,  ,#
             '#######'   ##     ##  '#######'  #'      '#     #####' # '####'



                  #######################################################
                  #                        -***-                        #
                  #          Department of theory and spectroscopy      #
                  #    Directorship and core code : Frank Neese         #
                  #        Max Planck Institute fuer Kohlenforschung    #
                  #                Kaiser Wilhelm Platz 1               #
                  #                 D-45470 Muelheim/Ruhr               #
                  #                      Germany                        #
                  #                                                     #
                  #                  All rights reserved                #
                  #                        -***-                        #
                  #######################################################


                         Program Version 5.0.4 -  RELEASE  -


 With contributions from (in alphabetic order):
   Daniel Aravena         : Magnetic Suceptibility
   Michael Atanasov       : Ab Initio Ligand Field Theory (pilot matlab implementation)
   Alexander A. Auer      : GIAO ZORA, VPT2 properties, NMR spectrum
   Ute Becker             : Parallelization
   Giovanni Bistoni       : ED, misc. LED, open-shell LED, HFLD
   Martin Brehm           : Molecular dynamics
   Dmytro Bykov           : SCF Hessian
   Vijay G. Chilkuri      : MRCI spin determinant printing, contributions to CSF-ICE
   Dipayan Datta          : RHF DLPNO-CCSD density
   Achintya Kumar Dutta   : EOM-CC, STEOM-CC
   Dmitry Ganyushin       : Spin-Orbit,Spin-Spin,Magnetic field MRCI
   Miquel Garcia          : C-PCM and meta-GGA Hessian, CC/C-PCM, Gaussian charge scheme
   Yang Guo               : DLPNO-NEVPT2, F12-NEVPT2, CIM, IAO-localization
   Andreas Hansen         : Spin unrestricted coupled pair/coupled cluster methods
   Benjamin Helmich-Paris : MC-RPA, TRAH-SCF, COSX integrals
   Lee Huntington         : MR-EOM, pCC
   Robert Izsak           : Overlap fitted RIJCOSX, COSX-SCS-MP3, EOM
   Marcus Kettner         : VPT2
   Christian Kollmar      : KDIIS, OOCD, Brueckner-CCSD(T), CCSD density, CASPT2, CASPT2-K
   Simone Kossmann        : Meta GGA functionals, TD-DFT gradient, OOMP2, MP2 Hessian
   Martin Krupicka        : Initial AUTO-CI
   Lucas Lang             : DCDCAS
   Marvin Lechner         : AUTO-CI (C++ implementation), FIC-MRCC
   Dagmar Lenk            : GEPOL surface, SMD
   Dimitrios Liakos       : Extrapolation schemes; Compound Job, initial MDCI parallelization
   Dimitrios Manganas     : Further ROCIS development; embedding schemes
   Dimitrios Pantazis     : SARC Basis sets
   Anastasios Papadopoulos: AUTO-CI, single reference methods and gradients
   Taras Petrenko         : DFT Hessian,TD-DFT gradient, ASA, ECA, R-Raman, ABS, FL, XAS/XES, NRVS
   Peter Pinski           : DLPNO-MP2, DLPNO-MP2 Gradient
   Christoph Reimann      : Effective Core Potentials
   Marius Retegan         : Local ZFS, SOC
   Christoph Riplinger    : Optimizer, TS searches, QM/MM, DLPNO-CCSD(T), (RO)-DLPNO pert. Triples
   Tobias Risthaus        : Range-separated hybrids, TD-DFT gradient, RPA, STAB
   Michael Roemelt        : Original ROCIS implementation
   Masaaki Saitow         : Open-shell DLPNO-CCSD energy and density
   Barbara Sandhoefer     : DKH picture change effects
   Avijit Sen             : IP-ROCIS
   Kantharuban Sivalingam : CASSCF convergence, NEVPT2, FIC-MRCI
   Bernardo de Souza      : ESD, SOC TD-DFT
   Georgi Stoychev        : AutoAux, RI-MP2 NMR, DLPNO-MP2 response
   Willem Van den Heuvel  : Paramagnetic NMR
   Boris Wezisla          : Elementary symmetry handling
   Frank Wennmohs         : Technical directorship


 We gratefully acknowledge several colleagues who have allowed us to
 interface, adapt or use parts of their codes:
   Stefan Grimme, W. Hujo, H. Kruse, P. Pracht,  : VdW corrections, initial TS optimization,
                  C. Bannwarth, S. Ehlert          DFT functionals, gCP, sTDA/sTD-DF
   Ed Valeev, F. Pavosevic, A. Kumar             : LibInt (2-el integral package), F12 methods
   Garnet Chan, S. Sharma, J. Yang, R. Olivares  : DMRG
   Ulf Ekstrom                                   : XCFun DFT Library
   Mihaly Kallay                                 : mrcc  (arbitrary order and MRCC methods)
   Jiri Pittner, Ondrej Demel                    : Mk-CCSD
   Frank Weinhold                                : gennbo (NPA and NBO analysis)
   Christopher J. Cramer and Donald G. Truhlar   : smd solvation model
   Lars Goerigk                                  : TD-DFT with DH, B97 family of functionals
   V. Asgeirsson, H. Jonsson                     : NEB implementation
   FAccTs GmbH                                   : IRC, NEB, NEB-TS, DLPNO-Multilevel, CI-OPT
                                                   MM, QMMM, 2- and 3-layer-ONIOM, Crystal-QMMM,
                                                   LR-CPCM, SF, NACMEs, symmetry and pop. for TD-DFT,
                                                   nearIR, NL-DFT gradient (VV10), updates on ESD,
                                                   ML-optimized integration grids
   S Lehtola, MJT Oliveira, MAL Marques          : LibXC Library
   Liviu Ungur et al                             : ANISO software


 Your calculation uses the libint2 library for the computation of 2-el integrals
 For citations please refer to: http://libint.valeyev.net

 Your ORCA version has been built with support for libXC version: 5.1.0
 For citations please refer to: https://tddft.org/programs/libxc/

 This ORCA versions uses:
   CBLAS   interface :  Fast vector & matrix operations
   LAPACKE interface :  Fast linear algebra routines
   SCALAPACK package :  Parallel linear algebra routines
   Shared memory     :  Shared parallel matrices


================================================================================

----- Orbital basis set information -----
Your calculation utilizes the basis: def2-TZVP
   F. Weigend and R. Ahlrichs, Phys. Chem. Chem. Phys. 7, 3297 (2005).

----- AuxJ basis set information -----
Your calculation utilizes the auxiliary basis: def2/J
   F. Weigend, Phys. Chem. Chem. Phys. 8, 1057 (2006).

================================================================================
                                        WARNINGS
                       Please study these warnings very carefully!
================================================================================


INFO   : the flag for use of the SHARK integral package has been found!

================================================================================
                                       INPUT FILE
================================================================================
NAME = 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.inp
|  1> !PBE def2-TZVP def2/J
|  2> !tightscf
|  3> !angs
|  4> * xyz 0 1
|  5>    O    0.00000000000000    0.00000000000000    0.00000000000000
|  6>    H   -0.24600868930000    0.83613433750000    0.45084222570000
|  7>    H    0.83613403950000   -0.24600893900000    0.45084267750000
|  8> *
|  9>
| 10>                          ****END OF INPUT****
================================================================================

                       ****************************
                       * Single Point Calculation *
                       ****************************

---------------------------------
CARTESIAN COORDINATES (ANGSTROEM)
---------------------------------
  O      0.000000    0.000000    0.000000
  H     -0.246009    0.836134    0.450842
  H      0.836134   -0.246009    0.450843

----------------------------
CARTESIAN COORDINATES (A.U.)
----------------------------
  NO LB      ZA    FRAG     MASS         X           Y           Z
   0 O     8.0000    0    15.999    0.000000    0.000000    0.000000
   1 H     1.0000    0     1.008   -0.464889    1.580065    0.851968
   2 H     1.0000    0     1.008    1.580064   -0.464890    0.851969

--------------------------------
INTERNAL COORDINATES (ANGSTROEM)
--------------------------------
 O      0   0   0     0.000000000000     0.00000000     0.00000000
 H      1   0   0     0.981274486590     0.00000000     0.00000000
 H      1   2   0     0.981274502845   102.48319223     0.00000000

---------------------------
INTERNAL COORDINATES (A.U.)
---------------------------
 O      0   0   0     0.000000000000     0.00000000     0.00000000
 H      1   0   0     1.854340041859     0.00000000     0.00000000
 H      1   2   0     1.854340072577   102.48319223     0.00000000

---------------------
BASIS SET INFORMATION
---------------------
There are 2 groups of distinct atoms

 Group   1 Type O   : 11s6p2d1f contracted to 5s3p2d1f pattern {62111/411/11/1}
 Group   2 Type H   : 5s1p contracted to 3s1p pattern {311/1}

Atom   0O    basis set group =>   1
Atom   1H    basis set group =>   2
Atom   2H    basis set group =>   2
---------------------------------
AUXILIARY/J BASIS SET INFORMATION
---------------------------------
There are 2 groups of distinct atoms

 Group   1 Type O   : 12s5p4d2f1g contracted to 6s4p3d1f1g pattern {711111/2111/211/2/1}
 Group   2 Type H   : 5s2p1d contracted to 3s1p1d pattern {311/2/1}

Atom   0O    basis set group =>   1
Atom   1H    basis set group =>   2
Atom   2H    basis set group =>   2
------------------------------------------------------------------------------
                           ORCA GTO INTEGRAL CALCULATION
                           -- RI-GTO INTEGRALS CHOSEN --
------------------------------------------------------------------------------
------------------------------------------------------------------------------
                   ___
                  /   \      - P O W E R E D   B Y -
                 /     \
                 |  |  |   _    _      __       _____    __    __
                 |  |  |  | |  | |    /  \     |  _  \  |  |  /  |
                  \  \/   | |  | |   /    \    | | | |  |  | /  /
                 / \  \   | |__| |  /  /\  \   | |_| |  |  |/  /
                |  |  |   |  __  | /  /__\  \  |    /   |      \
                |  |  |   | |  | | |   __   |  |    \   |  |\   \
                \     /   | |  | | |  |  |  |  | |\  \  |  | \   \
                 \___/    |_|  |_| |__|  |__|  |_| \__\ |__|  \__/

                      - O R C A' S   B I G   F R I E N D -
                                      &
                       - I N T E G R A L  F E E D E R -

 v1 FN, 2020, v2 2021
------------------------------------------------------------------------------


Reading SHARK input file 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.SHARKINP.tmp ... ok
----------------------
SHARK INTEGRAL PACKAGE
----------------------

Number of atoms                             ...      3
Number of basis functions                   ...     43
Number of shells                            ...     19
Maximum angular momentum                    ...      3
Integral batch strategy                     ... SHARK/LIBINT Hybrid
RI-J (if used) integral strategy            ... SPLIT-RIJ (Revised 2003 algorithm where possible)
Printlevel                                  ...      1
Contraction scheme used                     ... SEGMENTED contraction
Coulomb Range Separation                    ... NOT USED
Exchange Range Separation                   ... NOT USED
Finite Nucleus Model                        ... NOT USED
Auxiliary Coulomb fitting basis             ... AVAILABLE
   # of basis functions in Aux-J            ...     71
   # of shells in Aux-J                     ...     25
   Maximum angular momentum in Aux-J        ...      4
Auxiliary J/K fitting basis                 ... NOT available
Auxiliary Correlation fitting basis         ... NOT available
Auxiliary 'external' fitting basis          ... NOT available
Integral threshold                          ...     2.500000e-11
Primitive cut-off                           ...     2.500000e-12
Primitive pair pre-selection threshold      ...     2.500000e-12

Calculating pre-screening integrals         ... done (  0.0 sec) Dimension = 19
Organizing shell pair data                  ... done (  0.0 sec)
Shell pair information
Total number of shell pairs                 ...       190
Shell pairs after pre-screening             ...       190
Total number of primitive shell pairs       ...       556
Primitive shell pairs kept                  ...       535
          la=0 lb=0:     66 shell pairs
          la=1 lb=0:     55 shell pairs
          la=1 lb=1:     15 shell pairs
          la=2 lb=0:     22 shell pairs
          la=2 lb=1:     10 shell pairs
          la=2 lb=2:      3 shell pairs
          la=3 lb=0:     11 shell pairs
          la=3 lb=1:      5 shell pairs
          la=3 lb=2:      2 shell pairs
          la=3 lb=3:      1 shell pairs

Calculating one electron integrals          ... done (  0.0 sec)
Calculating RI/J V-Matrix + Cholesky decomp.... done (  0.0 sec)
Calculating Nuclear repulsion               ... done (  0.0 sec) ENN=      8.974187886138 Eh

SHARK setup successfully completed in   0.1 seconds

Maximum memory used throughout the entire GTOINT-calculation: 13.8 MB
-------------------------------------------------------------------------------
                                 ORCA SCF
-------------------------------------------------------------------------------

------------
SCF SETTINGS
------------
Hamiltonian:
 Density Functional     Method          .... DFT(GTOs)
 Exchange Functional    Exchange        .... PBE
   PBE kappa parameter   XKappa         ....  0.804000
   PBE mue parameter    XMuePBE         ....  0.219520
 Correlation Functional Correlation     .... PBE
   PBE beta parameter  CBetaPBE         ....  0.066725
 LDA part of GGA corr.  LDAOpt          .... PW91-LDA
 Gradients option       PostSCFGGA      .... off
   Density functional embedding theory  .... OFF
   NL short-range parameter             ....  6.400000
 RI-approximation to the Coulomb term is turned on
   Number of AuxJ basis functions       .... 71


General Settings:
 Integral files         IntName         .... 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65
 Hartree-Fock type      HFTyp           .... RHF
 Total Charge           Charge          ....    0
 Multiplicity           Mult            ....    1
 Number of Electrons    NEL             ....   10
 Basis Dimension        Dim             ....   43
 Nuclear Repulsion      ENuc            ....      8.9741878861 Eh

Convergence Acceleration:
 DIIS                   CNVDIIS         .... on
   Start iteration      DIISMaxIt       ....    12
   Startup error        DIISStart       ....  0.200000
   # of expansion vecs  DIISMaxEq       ....     5
   Bias factor          DIISBfac        ....   1.050
   Max. coefficient     DIISMaxC        ....  10.000
 Trust-Rad. Augm. Hess. CNVTRAH         .... auto
   Auto Start mean grad. ratio tolernc. ....  1.125000
   Auto Start start iteration           ....    20
   Auto Start num. interpolation iter.  ....    10
   Max. Number of Micro iterations      ....    16
   Max. Number of Macro iterations      .... Maxiter - #DIIS iter
   Number of Davidson start vectors     ....     2
   Converg. threshold I  (grad. norm)   ....   1.000e-05
   Converg. threshold II (energy diff.) ....   1.000e-08
   Grad. Scal. Fac. for Micro threshold ....   0.100
   Minimum threshold for Micro iter.    ....   0.010
   NR start threshold (gradient norm)   ....   0.001
   Initial trust radius                 ....   0.400
   Minimum AH scaling param. (alpha)    ....   1.000
   Maximum AH scaling param. (alpha)    .... 1000.000
   Orbital update algorithm             .... Taylor
   White noise on init. David. guess    .... on
   Maximum white noise                  ....   0.010
   Quad. conv. algorithm                .... NR
 SOSCF                  CNVSOSCF        .... on
   Start iteration      SOSCFMaxIt      ....   150
   Startup grad/error   SOSCFStart      ....  0.003300
 Level Shifting         CNVShift        .... on
   Level shift para.    LevelShift      ....    0.2500
   Turn off err/grad.   ShiftErr        ....    0.0010
 Zerner damping         CNVZerner       .... off
 Static damping         CNVDamp         .... on
   Fraction old density DampFac         ....    0.7000
   Max. Damping (<1)    DampMax         ....    0.9800
   Min. Damping (>=0)   DampMin         ....    0.0000
   Turn off err/grad.   DampErr         ....    0.1000
 Fernandez-Rico         CNVRico         .... off

SCF Procedure:
 Maximum # iterations   MaxIter         ....   125
 SCF integral mode      SCFMode         .... Direct
   Integral package                     .... SHARK and LIBINT hybrid scheme
 Reset frequency        DirectResetFreq ....    20
 Integral Threshold     Thresh          ....  2.500e-11 Eh
 Primitive CutOff       TCut            ....  2.500e-12 Eh

Convergence Tolerance:
 Convergence Check Mode ConvCheckMode   .... Total+1el-Energy
 Convergence forced     ConvForced      .... 0
 Energy Change          TolE            ....  1.000e-08 Eh
 1-El. energy change                    ....  1.000e-05 Eh
 Orbital Gradient       TolG            ....  1.000e-05
 Orbital Rotation angle TolX            ....  1.000e-05
 DIIS Error             TolErr          ....  5.000e-07


Diagonalization of the overlap matrix:
Smallest eigenvalue                        ... 9.191e-03
Time for diagonalization                   ...    0.000 sec
Threshold for overlap eigenvalues          ... 1.000e-08
Number of eigenvalues below threshold      ... 0
Time for construction of square roots      ...    0.000 sec
Total time needed                          ...    0.001 sec

Time for model grid setup =    0.009 sec

------------------------------
INITIAL GUESS: MODEL POTENTIAL
------------------------------
Loading Hartree-Fock densities                     ... done
Calculating cut-offs                               ... done
Initializing the effective Hamiltonian             ... done
Setting up the integral package (SHARK)            ... done
Starting the Coulomb interaction                   ... done (   0.0 sec)
Reading the grid                                   ... done
Mapping shells                                     ... done
Starting the XC term evaluation                    ... done (   0.0 sec)
  promolecular density results
     # of electrons  =      9.999524302
     EX              =     -8.721163784
     EC              =     -0.315379264
     EX+EC           =     -9.036543049
Transforming the Hamiltonian                       ... done (   0.0 sec)
Diagonalizing the Hamiltonian                      ... done (   0.0 sec)
Back transforming the eigenvectors                 ... done (   0.0 sec)
Now organizing SCF variables                       ... done
                      ------------------
                      INITIAL GUESS DONE (   0.0 sec)
                      ------------------
-------------------
DFT GRID GENERATION
-------------------

General Integration Accuracy     IntAcc      ... 4.388
Radial Grid Type                 RadialGrid  ... OptM3 with GC (2021)
Angular Grid (max. ang.)         AngularGrid ... 4 (Lebedev-302)
Angular grid pruning method      GridPruning ... 4 (adaptive)
Weight generation scheme         WeightScheme... Becke
Basis function cutoff            BFCut       ... 1.0000e-11
Integration weight cutoff        WCut        ... 1.0000e-14
Angular grids for H and He will be reduced by one unit
Partially contracted basis set               ... off
Rotationally invariant grid construction     ... off

Total number of grid points                  ...    12768
Total number of batches                      ...      201
Average number of points per batch           ...       63
Average number of grid points per atom       ...     4256
Time for grid setup =    0.060 sec

--------------
SCF ITERATIONS
--------------
ITER       Energy         Delta-E        Max-DP      RMS-DP      [F,P]     Damp
               ***  Starting incremental Fock matrix formation  ***
  0    -76.2818669047   0.000000000000 0.05629355  0.00441798  0.3187040 0.7000
  1    -76.3320464255  -0.050179520775 0.03089054  0.00244744  0.0991142 0.7000
                               ***Turning on DIIS***
  2    -76.3442090102  -0.012162584712 0.02181512  0.00221123  0.0099279 0.0000
  3    -76.3735008373  -0.029291827063 0.01771907  0.00127994  0.0569860 0.0000
  4    -76.3765871714  -0.003086334179 0.00446947  0.00034005  0.0046754 0.0000
                      *** Initiating the SOSCF procedure ***
                           *** Shutting down DIIS ***
                      *** Re-Reading the Fockian ***
                      *** Removing any level shift ***
ITER      Energy       Delta-E        Grad      Rot      Max-DP    RMS-DP
  5    -76.37668329  -0.0000961159  0.000939  0.000939  0.002578  0.000179
               *** Restarting incremental Fock matrix formation ***
  6    -76.37668974  -0.0000064480  0.000159  0.000351  0.000313  0.000027
  7    -76.37668935   0.0000003863  0.000348  0.000181  0.000218  0.000017
  8    -76.37668993  -0.0000005788  0.000017  0.000022  0.000016  0.000001
                 **** Energy Check signals convergence ****
              ***Rediagonalizing the Fockian in SOSCF/NRSCF***

               *****************************************************
               *                     SUCCESS                       *
               *           SCF CONVERGED AFTER   9 CYCLES          *
               *****************************************************


----------------
TOTAL SCF ENERGY
----------------

Total Energy       :          -76.37668993 Eh           -2078.31539 eV

Components:
Nuclear Repulsion  :            8.97418789 Eh             244.20007 eV
Electronic Energy  :          -85.35087782 Eh           -2322.51546 eV
One Electron Energy:         -122.66492840 Eh           -3337.88240 eV
Two Electron Energy:           37.31405059 Eh            1015.36694 eV

Virial components:
Potential Energy   :         -152.46654419 Eh           -4148.82559 eV
Kinetic Energy     :           76.08985426 Eh            2070.51020 eV
Virial Ratio       :            2.00376970


DFT components:
N(Alpha)           :        5.000004129876 electrons
N(Beta)            :        5.000004129876 electrons
N(Total)           :       10.000008259753 electrons
E(X)               :       -8.906750758092 Eh
E(C)               :       -0.324868209135 Eh
E(XC)              :       -9.231618967227 Eh
DFET-embed. en.    :        0.000000000000 Eh

---------------
SCF CONVERGENCE
---------------

  Last Energy change         ...   -1.3556e-09  Tolerance :   1.0000e-08
  Last MAX-Density change    ...    3.7145e-06  Tolerance :   1.0000e-07
  Last RMS-Density change    ...    3.2799e-07  Tolerance :   5.0000e-09
  Last Orbital Gradient      ...    5.0997e-06  Tolerance :   1.0000e-05
  Last Orbital Rotation      ...    4.9450e-06  Tolerance :   1.0000e-05

             **** THE GBW FILE WAS UPDATED (5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.gbw) ****
             **** DENSITY 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.scfp WAS UPDATED ****
             **** ENERGY FILE WAS UPDATED (5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.en.tmp) ****
             **** THE GBW FILE WAS UPDATED (5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.gbw) ****
             **** DENSITY 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.scfp WAS UPDATED ****
----------------
ORBITAL ENERGIES
----------------

  NO   OCC          E(Eh)            E(eV)
   0   2.0000     -18.748055      -510.1605
   1   2.0000      -0.917126       -24.9563
   2   2.0000      -0.467913       -12.7326
   3   2.0000      -0.335999        -9.1430
   4   2.0000      -0.255092        -6.9414
   5   0.0000      -0.005936        -0.1615
   6   0.0000       0.068439         1.8623
   7   0.0000       0.318984         8.6800
   8   0.0000       0.377434        10.2705
   9   0.0000       0.397459        10.8154
  10   0.0000       0.398384        10.8406
  11   0.0000       0.530078        14.4242
  12   0.0000       0.566580        15.4174
  13   0.0000       1.087143        29.5827
  14   0.0000       1.130125        30.7523
  15   0.0000       1.225293        33.3419
  16   0.0000       1.482674        40.3456
  17   0.0000       1.583188        43.0807
  18   0.0000       1.680802        45.7369
  19   0.0000       1.933835        52.6223
  20   0.0000       1.986150        54.0459
  21   0.0000       2.008629        54.6576
  22   0.0000       2.229579        60.6699
  23   0.0000       2.270228        61.7761
  24   0.0000       2.459543        66.9276
  25   0.0000       2.482699        67.5577
  26   0.0000       2.517366        68.5010
  27   0.0000       2.676920        72.8427
  28   0.0000       3.551086        96.6300
  29   0.0000       3.782425       102.9250
  30   0.0000       4.984916       135.6465
  31   0.0000       5.090686       138.5246
  32   0.0000       5.243469       142.6820
  33   0.0000       5.275017       143.5405
  34   0.0000       5.300792       144.2419
  35   0.0000       5.847921       159.1300
  36   0.0000       6.033048       164.1676
  37   0.0000       6.179750       168.1596
  38   0.0000       6.230537       169.5415
  39   0.0000       6.304534       171.5551
  40   0.0000       6.699656       182.3069
  41   0.0000       6.878157       187.1642
  42   0.0000      42.864615      1166.4055

                    ********************************
                    * MULLIKEN POPULATION ANALYSIS *
                    ********************************

-----------------------
MULLIKEN ATOMIC CHARGES
-----------------------
   0 O :   -0.620521
   1 H :    0.310260
   2 H :    0.310260
Sum of atomic charges:   -0.0000000

--------------------------------
MULLIKEN REDUCED ORBITAL CHARGES
--------------------------------
  0 O s       :     3.825265  s :     3.825265
      pz      :     1.741277  p :     4.773008
      px      :     1.515866
      py      :     1.515866
      dz2     :     0.001932  d :     0.021076
      dxz     :     0.006216
      dyz     :     0.006216
      dx2y2   :     0.006124
      dxy     :     0.000588
      f0      :     0.000240  f :     0.001172
      f+1     :     0.000033
      f-1     :     0.000033
      f+2     :     0.000381
      f-2     :     0.000137
      f+3     :     0.000174
      f-3     :     0.000174
  1 H s       :     0.643400  s :     0.643400
      pz      :     0.015435  p :     0.046340
      px      :     0.013107
      py      :     0.017797
  2 H s       :     0.643400  s :     0.643400
      pz      :     0.015435  p :     0.046340
      px      :     0.017797
      py      :     0.013107


                     *******************************
                     * LOEWDIN POPULATION ANALYSIS *
                     *******************************

----------------------
LOEWDIN ATOMIC CHARGES
----------------------
   0 O :   -0.325264
   1 H :    0.162632
   2 H :    0.162632

-------------------------------
LOEWDIN REDUCED ORBITAL CHARGES
-------------------------------
  0 O s       :     3.491867  s :     3.491867
      pz      :     1.739717  p :     4.798827
      px      :     1.529555
      py      :     1.529555
      dz2     :     0.001005  d :     0.032221
      dxz     :     0.009150
      dyz     :     0.009150
      dx2y2   :     0.011264
      dxy     :     0.001653
      f0      :     0.000431  f :     0.002349
      f+1     :     0.000002
      f-1     :     0.000002
      f+2     :     0.001011
      f-2     :     0.000267
      f+3     :     0.000318
      f-3     :     0.000318
  1 H s       :     0.684674  s :     0.684674
      pz      :     0.050074  p :     0.152694
      px      :     0.033241
      py      :     0.069379
  2 H s       :     0.684674  s :     0.684674
      pz      :     0.050074  p :     0.152694
      px      :     0.069379
      py      :     0.033241


                      *****************************
                      * MAYER POPULATION ANALYSIS *
                      *****************************

  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence

  ATOM       NA         ZA         QA         VA         BVA        FA
  0 O      8.6205     8.0000    -0.6205     1.8277     1.8277    -0.0000
  1 H      0.6897     1.0000     0.3103     0.9183     0.9183     0.0000
  2 H      0.6897     1.0000     0.3103     0.9183     0.9183    -0.0000

  Mayer bond orders larger than 0.100000
B(  0-O ,  1-H ) :   0.9138 B(  0-O ,  2-H ) :   0.9138

-------
TIMINGS
-------

Total SCF time: 0 days 0 hours 0 min 0 sec

Total time                  ....       0.503 sec
Sum of individual times     ....       0.471 sec  ( 93.6%)

Fock matrix formation       ....       0.388 sec  ( 77.2%)
  Split-RI-J                ....       0.033 sec  (  8.4% of F)
  XC integration            ....       0.346 sec  ( 89.1% of F)
    Basis function eval.    ....       0.129 sec  ( 37.3% of XC)
    Density eval.           ....       0.066 sec  ( 19.0% of XC)
    XC-Functional eval.     ....       0.054 sec  ( 15.7% of XC)
    XC-Potential eval.      ....       0.073 sec  ( 21.1% of XC)
Diagonalization             ....       0.002 sec  (  0.4%)
Density matrix formation    ....       0.000 sec  (  0.0%)
Population analysis         ....       0.001 sec  (  0.3%)
Initial guess               ....       0.010 sec  (  1.9%)
Orbital Transformation      ....       0.000 sec  (  0.0%)
Orbital Orthonormalization  ....       0.000 sec  (  0.0%)
DIIS solution               ....       0.001 sec  (  0.1%)
SOSCF solution              ....       0.000 sec  (  0.1%)
Grid generation             ....       0.069 sec  ( 13.6%)

Maximum memory used throughout the entire SCF-calculation: 9.3 MB

-------------------------   --------------------
FINAL SINGLE POINT ENERGY       -76.376689929145
-------------------------   --------------------


                            ***************************************
                            *     ORCA property calculations      *
                            ***************************************

                                    ---------------------
                                    Active property flags
                                    ---------------------
   (+) Dipole Moment


------------------------------------------------------------------------------
                       ORCA ELECTRIC PROPERTIES CALCULATION
------------------------------------------------------------------------------

Dipole Moment Calculation                       ... on
Quadrupole Moment Calculation                   ... off
Polarizability Calculation                      ... off
GBWName                                         ... 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.gbw
Electron density                                ... 5294bf1ac65a3d5654a86be9b693b97a5f9c9b65.scfp
The origin for moment calculation is the CENTER OF MASS  = ( 0.062398,  0.062398  0.095341)

-------------
DIPOLE MOMENT
-------------
                                X             Y             Z
Electronic contribution:     -0.10097      -0.10097      -0.15428
Nuclear contribution   :      0.49120       0.49120       0.75053
                        -----------------------------------------
Total Dipole Moment    :      0.39023       0.39023       0.59625
                        -----------------------------------------
Magnitude (a.u.)       :      0.81244
Magnitude (Debye)      :      2.06507



--------------------
Rotational spectrum
--------------------

Rotational constants in cm-1:    24.949715    14.281277     9.082457
Rotational constants in MHz : 747973.639724 428141.928286 272285.211699

 Dipole components along the rotational axes:
x,y,z [a.u.] :     0.000000     0.812444    -0.000000
x,y,z [Debye]:     0.000000     2.065069    -0.000000



Timings for individual modules:

Sum of individual times         ...        0.654 sec (=   0.011 min)
GTO integral calculation        ...        0.104 sec (=   0.002 min)  15.9 %
SCF iterations                  ...        0.550 sec (=   0.009 min)  84.1 %
                             ****ORCA TERMINATED NORMALLY****
TOTAL RUN TIME: 0 days 0 hours 0 minutes 0 seconds 736 msec
