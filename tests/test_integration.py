import json
from copy import deepcopy
from pathlib import Path

import hyif
import numpy as np
from hyobj import Molecule, PeriodicSystem

import hyal


def test_data_set_reader():
    """Test DataSetReader."""
    cp2k = hyif.CP2K({'check_version': False})
    cp2k_dir = Path('data/cp2k')
    al_cp2k = hyal.ActiveLearning(calculator=cp2k)
    ds_cp2k = al_cp2k.generate_dataset(cp2k_dir)
    assert len(ds_cp2k) == 10
    assert 'walltime' in ds_cp2k.data.columns
    orca = hyif.Orca({'check_version': False})
    orca_dir = Path('data/orca')
    al_orca = hyal.ActiveLearning(calculator=orca)
    ds_orca = al_orca.generate_dataset(orca_dir)
    assert len(ds_orca) == 1
    # vasp_dir = str(Path('data/vasp'))
    # vasp = hyif.VASP({'check_version': False})
    # al_vasp = hyal.ActiveLearning(calculator=vasp)
    # ds = al_vasp.generate_dataset(vasp_dir)
    # assert len(ds) == 2

    # test filters
    ds_cp2k.change_units('metal')
    max_forces = []
    for index, row in ds_cp2k.data.iterrows():
        forces = np.array(row['force']).reshape(-1, 3)
        force_mag = np.linalg.norm(forces, axis=1)
        max_force = np.max(force_mag)
        max_forces.append(max_force)
    max_max_force = np.max(max_forces)
    min_max_force = np.min(max_forces)

    calc_filters = {
        'max_force': {
            'key': 'force',
            'upper': True,
            'operation': 'max',
            'limit': max_max_force - 0.0001,
            'shape': [-1, 3],
        },
        'min_force': {
            'key': 'force',
            'upper': False,
            'operation': 'max',
            'limit': min_max_force + 0.0001,
            'shape': [-1, 3],
        },
    }
    filter_fns = {}
    for filter_name, filter_prop in calc_filters.items():
        filter_fn = al_cp2k.set_filter(filter_name, filter_prop)
        filter_fns[filter_name] = filter_fn
    ds_filtered = al_cp2k.apply_filters(ds_cp2k, filter_fns)
    assert len(ds_filtered) == len(ds_cp2k) - 2

    # read from folder cp2k_warn
    cp2k_warn_dir = Path('data/cp2k_warn')
    ds_cp2k_warn = al_cp2k.generate_dataset(cp2k_warn_dir)
    cp2k_warn_ds = al_cp2k.remove_nonconverged(ds_cp2k_warn)
    assert len(cp2k_warn_ds.data) == 1

    # read from conf_ref
    conf_ref_dir = Path('data/conf_ref_multiple')
    al = hyal.ActiveLearning()
    conf_ref = hyal.conref.ConRef()
    ds = al.generate_dataset(
        conf_ref_dir, calculator=conf_ref, parse_input=False
    )
    assert len(ds) == 10
    assert sum(ds.data['is_converged']) == 10

    # Parse a LAMMPS trajectory
    lammps = hyif.Lammps({'check_version': False})
    lammps_dir = Path('data/lammps')
    al_lammps = hyal.ActiveLearning(calculator=lammps)
    ds_lammps = al_lammps.generate_dataset(lammps_dir, parse_input=False)
    assert len(ds_lammps) == 6

    # test a filter based on
    min_distance = np.array(
        [syst.get_atomic_distances() for syst in ds_cp2k.systems]
    ).min()
    # create a filter for distances
    calc_filters = {
        'min_distances': {
            'key': 'distances',
            'upper': False,
            'operation': 'min',
            'limit': min_distance + 0.0001,
            'shape': [-1],
        }
    }
    filter_fns = {}
    for filter_name, filter_prop in calc_filters.items():
        filter_fn = al_cp2k.set_filter(filter_name, filter_prop)
        filter_fns[filter_name] = filter_fn
    ds_dist_filtered = al_cp2k.apply_filters(ds_cp2k, filter_fns)
    assert len(ds_dist_filtered) == len(ds_cp2k) - 1
    assert 'distances' not in ds_dist_filtered.data.columns


def test_parser_missing():
    """Test parser for missing data."""
    mycp2k = hyif.CP2K({'check_version': False})
    al = hyal.ActiveLearning(calculator=mycp2k)

    data = al.generate_dataset('data/cp2k_missing_data/')
    assert len(data) == 3

    hash_output = data.data['output_file']
    hash_input = data.data['input_file']

    hash_output = [
        Path(hash).stem.replace(
            mycp2k.__dict__.get('output_file_suffix', '.out'), ''
        )
        for hash in hash_output
    ]
    hash_input = [
        Path(hash).stem.replace(
            mycp2k.__dict__.get('input_file_suffix', '.inp'), ''
        )
        for hash in hash_input
    ]
    assert hash_output == hash_input


def test_inhomogenous_dataset_filter():
    """Test inhomogenous filter."""
    cp2k = hyif.CP2K({'check_version': False})
    cp2k_dir = Path('data/cp2k')
    al_cp2k = hyal.ActiveLearning(calculator=cp2k)
    ds_cp2k = al_cp2k.generate_dataset(cp2k_dir)
    ds_cp2k.standardize_columns()
    ds_cp2k_twice = deepcopy(ds_cp2k)
    for i, row in ds_cp2k_twice.data.iterrows():
        for col in ['forces', 'atoms', 'coordinates']:
            ds_cp2k_twice.data.at[i, col] = row[col] + row[col]

    # inhomogenous dataset
    ds_new = ds_cp2k + ds_cp2k_twice

    assert 2 * len(ds_new.data.coordinates[0]) == len(
        ds_cp2k_twice.data.coordinates[0]
    )

    # test filters
    ds_cp2k.change_units('metal')
    max_forces = []
    for index, row in ds_cp2k.data.iterrows():
        forces = np.array(row['forces']).reshape(-1, 3)
        force_mag = np.linalg.norm(forces, axis=1)
        max_force = np.max(force_mag)
        max_forces.append(max_force)
    max_max_force = np.max(max_forces)
    min_max_force = np.min(max_forces)

    calc_filters = {
        'max_force': {
            'key': 'forces',
            'upper': True,
            'operation': 'max',
            'limit': max_max_force - 0.0001,
            'shape': [-1, 3],
        },
        'min_force': {
            'key': 'forces',
            'upper': False,
            'operation': 'max',
            'limit': min_max_force + 0.0001,
            'shape': [-1, 3],
        },
    }
    filter_fns = {}
    for filter_name, filter_prop in calc_filters.items():
        filter_fn = al_cp2k.set_filter(filter_name, filter_prop)
        filter_fns[filter_name] = filter_fn
    ds_filtered = al_cp2k.apply_filters(ds_new, filter_fns)
    assert len(ds_filtered) == len(ds_new) - 4


def test_reference_correction():
    """Test reference correction."""
    cp2k = hyif.CP2K({'check_version': False})
    cp2k_dir = Path('data/cp2k')
    al_cp2k = hyal.ActiveLearning(calculator=cp2k)
    ds_cp2k = al_cp2k.generate_dataset(cp2k_dir)
    ds_cp2k.standardize_columns()

    reference_correction = {'energy': -1.0}
    ds_cp2k_corrected = al_cp2k.correct_dataset(ds_cp2k, reference_correction)
    print(np.array(ds_cp2k_corrected.data['atoms'].values))
    assert np.allclose(
        np.array(ds_cp2k_corrected.data['energy'].values),
        (
            np.array(ds_cp2k.data['energy'].values)
            + reference_correction['energy']
        ),
    )

    reference_correction = {'energy': {'H': +13.6, 'O': +427.80545187305376}}
    ds_cp2k_corrected = al_cp2k.correct_dataset(ds_cp2k, reference_correction)
    num_h = [
        np.sum([1 for atom in atoms if atom == 'H'])
        for atoms in ds_cp2k_corrected.data['atoms'].values
    ][0]

    num_o = [
        np.sum([1 for atom in atoms if atom == 'O'])
        for atoms in ds_cp2k_corrected.data['atoms'].values
    ][0]

    print(num_h, num_o)
    assert np.allclose(
        np.array(ds_cp2k_corrected.data['energy'].values),
        (np.array(ds_cp2k.data['energy'].values))
        + num_h * reference_correction['energy']['H']
        + num_o * reference_correction['energy']['O'],
    )
    print(np.array(ds_cp2k_corrected.data['energy'].values))
    print(ds_cp2k.data['energy'].values)


def test_splitting():
    """Test splitting of datasets."""
    cp2k = hyif.CP2K({'check_version': False})
    cp2k_dir = Path('data/cp2k')
    al_cp2k = hyal.ActiveLearning(calculator=cp2k)
    ds_cp2k = al_cp2k.generate_dataset(cp2k_dir)
    rng = np.random.default_rng(seed=0)
    ds_train, ds_test = al_cp2k.split_datasets(
        {'water': ds_cp2k}, 0.2, rng=rng, safe=True
    )
    # check that ds_train contains 80%
    assert len(ds_train['water']) == np.round(len(ds_cp2k) * 0.8)
    # check that ds_test contains 20%
    assert len(ds_test['water']) == np.round(len(ds_cp2k) * 0.2)

    # test splitting without safe mode with ratio 1
    ds_train, ds_test = al_cp2k.split_datasets(
        {'water': ds_cp2k}, 1, rng=rng, safe=False
    )
    # check that ds_test contains 100%
    assert len(ds_test['water']) == len(ds_cp2k)

    # test splitting with safe mode with ratio 1
    ds_train, ds_test = al_cp2k.split_datasets(
        {'water': ds_cp2k}, 1, rng=rng, safe=True
    )
    # check that ds_test contains all except one
    assert len(ds_test['water']) == len(ds_cp2k) - 1
    # check that ds_train contains one
    assert len(ds_train['water']) == 1

    # test splitting with safe mode with ratio 0
    ds_train, ds_test = al_cp2k.split_datasets(
        {'water': ds_cp2k}, 0, rng=rng, safe=True
    )
    # check that ds_train contains all except one
    assert len(ds_train['water']) == len(ds_cp2k)
    # check that ds_test is empty
    assert ds_test == {}


def test_hyal_settings_reader():
    """Test hyal settings reader."""
    al = hyal.ActiveLearning(
        ml_engines=[hyif.DeepMD({'check_version': False})] * 2
    )
    with open('data/templates/hyal_settings.json', 'r') as f:
        hyal_settings = json.load(f)
    hyal_options = al.parse_options(hyal_settings, maxiter=1)[0][0]

    assert not hyal_options['recalibrate_uncertainty']


def test_sampler_parallel():
    """Test parallel sampler."""
    lammps = hyif.Lammps({'check_version': False})
    al_lammps = hyal.ActiveLearning(sampler=lammps)
    h2o = PeriodicSystem(
        Molecule(
            """O         0.0000000000        0.0000000000        0.0000000000
        H        -0.2460086893        0.8361343375        0.4508422257
        H         0.8361340395       -0.2460089390        0.4508426775""",
            units='metal',
        ),
        pbc=[10, 0, 0, 0, 10, 0, 0, 0, 10],
    )
    res = al_lammps.sampler_parallel(
        [h2o], [dict(nsteps=1)], [dict(sub_dir='tmp')]
    )
    assert 'trajectory' in res[0].keys()
    assert 'files' in res[0].keys()


def test_check_minimum_distance():
    """Test check_minimum_distance."""
    cp2k = hyif.CP2K({'check_version': False})
    al_cp2k = hyal.ActiveLearning(calculator=cp2k)
    cp2k_dir = Path('data/cp2k')
    ds_cp2k = al_cp2k.generate_dataset(cp2k_dir)
    ds_cp2k.standardize_columns()
    al_cp2k.compute_distances(ds_cp2k, 3)


def main():
    """Run main function."""
    test_data_set_reader()


if __name__ == '__main__':
    main()
