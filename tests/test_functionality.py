import json
import os

import hyif
import hyset
import numpy as np
from hyobj import Molecule

import hyal


def test_multiple_systems():
    """Test multiple systems."""
    myenv_calculator = hyset.create_compute_settings(
        work_dir=os.path.realpath('./'),
        cpus_per_task=8,
        force_recompute=False,
        debug=False,
        path='/Users/sigbjobo/software/orca_5_0_4_macosx_arm64_openmpi411',
    )
    my_calculator = hyif.Orca(
        {
            'qcmethod': 'PBE def2-SVP def2/J EnGRAD  TIGHTSCF'
        },  # Extrapolate(3/4,def2) def2-TZVPP/C
        compute_settings=myenv_calculator,
    )

    myenv_deepmd_lammps = hyset.create_compute_settings(
        'local',
        work_dir=os.path.realpath('./'),
        debug=False,
        launcher='conda run -n deepmd',
    )

    myenv_local_debug = hyset.create_compute_settings(
        'local',
        debug=True,
    )

    # Sampler for gathering geometries
    my_sampler = hyif.Lammps(
        {'check_version': False, 'options': {}},
        compute_settings=myenv_deepmd_lammps,
    )

    myenv_deepmd_lammps = hyset.create_compute_settings(
        'local',
        work_dir=os.path.realpath('./'),
        debug=False,
        launcher='conda run -n deepmd',
    )

    # Machine learning potentials
    input_dict = json.load(open('data/templates/deepmd_input.json', 'r'))
    ml_engine = hyif.DeepMD(
        {
            'input': input_dict,
            'debug': False,
            'check_version': False,
            'options': {},
        },
        compute_settings=myenv_deepmd_lammps,
    )

    h2o_monomer = Molecule(
        """O         0.0000000000        0.0000000000        0.0000000000
H        -0.2460086893        0.8361343375        0.4508422257
H         0.8361340395       -0.2460089390        0.4508426775""",
        units='metal',
    )

    co2_monomer = Molecule(
        """C         0.0000000000        0.0000000000        0.0000000000
O         0.0000000000        0.0000000000        1.1600000000
O         0.0000000000        0.0000000000       -1.1600000000""",
        units='metal',
    )

    h2o_co2_dimer = Molecule(
        """O         0.0000000000        0.0000000000        0.0000000000
H        -0.2460086893        0.8361343375        0.4508422257
H         0.8361340395       -0.2460089390        0.4508426775
C         0.0000000000        0.0000000000        2.3200000000
O         0.0000000000        0.0000000000        3.4800000000
O         0.0000000000        0.0000000000        1.1600000000""",
        units='metal',
    )

    co2_h2o_dimer = Molecule(
        """C         0.0000000000        0.0000000000        0.0000000000
O         0.0000000000        0.0000000000        1.1600000000
O         0.0000000000        0.0000000000       -1.1600000000
O         0.0000000000        0.0000000000        2.3200000000
H        -0.2460086893        0.8361343375        2.7708422257
H         0.8361340395       -0.2460089390        2.7708426775""",
        units='metal',
    )

    systems = {
        'h2o': h2o_monomer,
        'co2': co2_monomer,
        'h2o_co2': h2o_co2_dimer,
        'co2_h2o': co2_h2o_dimer,
    }
    al = hyal.ActiveLearning(
        ml_engines=[ml_engine] * 2,
        calculator=my_calculator,
        sampler=my_sampler,
        conf_ref_compute_settings=myenv_local_debug,
    )

    hyal_settings = json.load(open('data/templates/hyal_settings.json', 'r'))
    rng = np.random.default_rng(12345)
    initial_set = al.make_initial_set(
        systems, 1, 0.1, generator=rng, sub_dir='test_functionality_results'
    )

    al.run(
        systems=systems,
        options=[hyal_settings] * 2,
        initial_set=initial_set,
        sub_dir='test_functionality_results',
    )


def main():
    """Call main function."""
    test_multiple_systems()


if __name__ == '__main__':
    main()
