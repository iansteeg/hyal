import copy
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from deepmd.infer import DeepPot

from hyal import conref

conf_ref = conref.ConRef(models=['data/conf_ref_ice/dummy.pb'])

input_file = 'data/conf_ref_ice/ice.json'
geometry = conf_ref.InputParser.parse(input_file)
for key, value in geometry.items():
    # make double precision numerical values for the coordinates
    if key == 'coordinates' or key == 'box':
        geometry[key] = np.array(value, dtype=np.float64)

model = DeepPot(
    'iter14/graph-7569a0e9dd01844fd788bd50e6cb229c95e8e234-compressed.pb'
)
type_map = model.get_type_map()
atom_types = [type_map.index(atom) for atom in geometry['atoms']]
ref_e, ref_f, ref_v = model.eval(
    geometry['coordinates'], geometry['box'], atom_types
)


def numerical_derivative(geometry, index, eps):
    """Forward difference scheme for numerical derivative."""
    geometry_tmp = copy.deepcopy(geometry)
    e0, f0, v0 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    geometry_tmp['coordinates'][index] += eps
    e1, f1, v1 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    return -(e1 - e0) / eps, -(f1 - f0) / eps, -(v1 - v0) / eps


def numerical_derivative2(geometry, index, eps):
    """Central difference scheme for numerical derivative."""
    geometry_tmp = copy.deepcopy(geometry)
    geometry_tmp['coordinates'][index] -= eps
    e0, f0, v0 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    geometry_tmp['coordinates'][index] += 2 * eps
    e1, f1, v1 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    return (
        -(e1 - e0) / (2 * eps),
        -(f1 - f0) / (2 * eps),
        -(v1 - v0) / (2 * eps),
    )


N = 3
fn_bench_forces = 'bench_forces.csv'
if os.path.exists(fn_bench_forces):
    df = pd.read_csv(fn_bench_forces)
else:

    num = np.min((N * 3, len(ref_f.reshape(-1))))

    num_forces = []
    eps_log = np.logspace(-5, -2, 20)
    for eps in eps_log:
        num_forces_step = []
        for index in range(num):
            e, df, dv = numerical_derivative(geometry, index, eps)
            num_forces_step.append(e[0][0])
        num_forces.append(num_forces_step)

    # exchange the axes
    num_forces = np.array(num_forces)
    num_forces -= ref_f.reshape(-1)[:num]
    num_forces = np.array(num_forces).reshape(len(num_forces), -1, 3)
    num_forces = np.sum(num_forces**2, axis=-1) ** 0.5 * 1000  # type: ignore
    num_forces = np.mean(num_forces, axis=1)

    num = np.min((N * 3, len(ref_f.reshape(-1))))
    num_forces2 = []
    eps_log2 = np.logspace(-3, -1, 20)
    for eps in eps_log2:
        num_forces_step = []
        for index in range(num):
            e, df, dv = numerical_derivative2(geometry, index, eps)
            num_forces_step.append(e[0][0])
        num_forces2.append(num_forces_step)

    # exchange the axes
    num_forces2 = np.array(num_forces2)
    num_forces2 -= ref_f.reshape(-1)[:num]
    num_forces2 = np.array(num_forces2).reshape(len(num_forces2), -1, 3)
    num_forces2 = np.sum(num_forces2**2, axis=-1) ** 0.5 * 1000  # type: ignore
    num_forces2 = np.mean(num_forces2, axis=1)

    df = pd.DataFrame(
        {
            'eps_forward': eps_log,
            'force_error_forward': num_forces,
            'eps_central': eps_log2,
            'force_error_central': num_forces2,
        }
    )
    df.to_csv(fn_bench_forces, index=False)


plt.figure(figsize=(3.3, 2.5))
plt.plot(df['eps_forward'], df['force_error_forward'], label='forward')
plt.plot(df['eps_central'], df['force_error_central'], label='central')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Epsilon coordinates (Å)')
plt.ylabel('Force error (meV/Å)')
plt.tight_layout()
plt.legend(frameon=False)
plt.savefig('force_error.png', dpi=600)
# plt.show()

# for box geometry


def numerical_derivative_box(geometry, index, eps):
    """Forward difference scheme for numerical derivative."""
    geometry_tmp = copy.deepcopy(geometry)
    e0, f0, v0 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    ep = eps * geometry_tmp['box'][index]
    geometry_tmp = conf_ref.toggle_scaled_coordinates(geometry_tmp)
    geometry_tmp['box'][index] += ep
    geometry_tmp = conf_ref.toggle_scaled_coordinates(geometry_tmp)
    e1, f1, v1 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )

    return -(e1 - e0) / ep, -(f1 - f0) / ep, -(v1 - v0) / ep


def central_numerical_derivative_box(geometry, index, eps):
    """Central difference scheme for numerical derivative."""
    geometry_tmp = copy.deepcopy(geometry)
    box_length = geometry_tmp['box'][index]
    ep = eps * box_length
    geometry_tmp = conf_ref.toggle_scaled_coordinates(geometry_tmp)
    geometry_tmp['box'][index] -= ep
    geometry_tmp = conf_ref.toggle_scaled_coordinates(geometry_tmp)
    e0, f0, v0 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    geometry_tmp = conf_ref.toggle_scaled_coordinates(geometry_tmp)
    geometry_tmp['box'][index] += 2 * ep
    geometry_tmp = conf_ref.toggle_scaled_coordinates(geometry_tmp)
    e1, f1, v1 = model.eval(
        geometry_tmp['coordinates'], geometry_tmp['box'], atom_types
    )
    return (
        -(e1 - e0) / (2 * ep),
        -(f1 - f0) / (2 * ep),
        -(v1 - v0) / (2 * ep),
    )


# compute the derivative for Lxx, for different eps
eps = np.logspace(-6, -2, 100)
num_forces_box = []
for ep in eps:
    tmp_num_forces_box = []
    for index in [0, 4, 8]:
        e, df, dv = numerical_derivative_box(geometry, index, ep)
        tmp_num_forces_box.append(e[0][0] * geometry['box'][index])
    num_forces_box.append(tmp_num_forces_box)

num_forces_box = np.array(num_forces_box)
ref = np.array([[ref_v[0, 0], ref_v[0, 4], ref_v[0, 8]]] * len(eps))
error_forward = np.sum((num_forces_box - ref) ** 2, axis=-1) ** 0.5

num_forces_box2 = []
eps_central = np.logspace(-4, -1, 100)
for ep in eps_central:
    tmp_num_forces_box = []
    for index in [0, 4, 8]:
        e, df, dv = central_numerical_derivative_box(geometry, index, ep)
        tmp_num_forces_box.append(e[0][0] * geometry['box'][index])
    num_forces_box2.append(tmp_num_forces_box)

num_forces_box2 = np.array(num_forces_box2)
error_central = np.sum((num_forces_box2 - ref) ** 2, axis=-1) ** 0.5

df = pd.DataFrame(
    {
        'eps_forward': eps,
        'error_forward': error_forward,
        'eps_central': eps_central,
        'error_central': error_central,
    }
)
df.to_csv('box_error.csv', index=False)
print(ref_v)
print(num_forces_box)
plt.figure(figsize=(3.3, 2.5))
plt.plot(eps, error_forward * 1000, label='forward')
plt.plot(eps_central, error_central * 1000, label='central')

# plt.plot(eps)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Epsilon (box length)')
plt.ylabel('Virial (meV)')
plt.tight_layout()
plt.legend(frameon=False)
plt.savefig('box_error.png', dpi=600)


# plt.show()
